## Refactor Code FE1 - Phase 2
### Yêu cầu
* Cấu trúc code JS theo hướng module hóa
* Dễ dàng mở rộng
* Hiệu quả mà tốn ít resource
* Learning curve thấp nhất có thể

### Vấn đề chính:
* Code hiện đang hệ thống theo hàm gọi trực tiếp
* Giữa các thành phần (chia theo khu vực UI) gọi lẫn nhau khá nhiều nên dễ gây lỗi/mất nhiều thời gian khi một hàm thay đổi hoặc thay đổi logic.

### Hướng giải quyết:

* Triển khai module hóa theo hướng Mediator Pattern kết hợp với Facade Pattern.
    ![Mediator Pattern](https://raw.github.com/flosse/scaleApp/master/architecture.png)
    Bản chất của pattern này là:
    
    * Các module không giao tiếp trực tiếp với nhau mà chỉ giao tiếp với sandbox có interface đơn giản (Facade Pattern).
    * Sanbox nhận tín hiệu (theo kênh) & sẽ bắn tín hiệu lên Core (có Mediator đóng vai trò điều phối).
    * Mediator sẽ truyền dữ liệu được gửi lên đến tất cả các sandbox có đăng ký kênh.
    * Các module cùng kênh sẽ thực hiện callback được khai báo trong chính module đó
    * Các module có thể bao gồm nhiều module nhỏ nữa (và sẽ lại có sandbox riêng)

-> Giống với socket.io

* Ví dụ:
	* Ở trang details, mỗi khi nhấn chọn biến thể, phải thực hiện các thay đổi tại các module:
        * Phần hiển thị ảnh/video (gallery)
		* Thông tin mô tả ngắn
		* Chọn biến thể
		* ...
Theo cách viết hiện tại, là viết một hàm xử lý lần lượt tất cả các thay đổi này. Khi một chỗ nào đó lỗi, sẽ không load được biến thể. Theo cách viết mới, sẽ là:
* Nhấn nút chọn biến thể, truyền tín hiệu lên Mediator qua kênh "changeVariant", kèm dữ liệu biến thể
* Tất cả các module có đăng ký với kênh "changeVariant" sẽ thực hiện callback tương ứng
* Nếu một module lỗi, các module khác sẽ không bị ảnh hưởng

-> Module hóa tối đa, code đơn giản hơn nhiều mà vẫn đảm bảo sự giao tiếp giữa các module.

*Nhược điểm của mô hình này:*

* Các module không giao tiếp trực tiếp với nhau mà qua trung gian nên về lý thuyết sẽ chậm hơn 1 lệnh
* Nhưng code sẽ đơn giản hơn nhiều + cấu trúc rõ ràng nên thực tế thì hiệu năng sẽ tăng rất cao.

### Công cụ sử dụng để implement: [`scaleApp`](http://scaleapp.org)
* Ưu điểm: 
	* Đơn giản, nhỏ gọn (tầm 300 dòng code, 8.7kB minify)
	* Không phụ thuộc vào thư viện khác
	* Chỉ là wrapper, không conflict với các thư viện đang dùng
	* Dễ dàng add/bỏ module
	* Learning curve thấp

* Lộ trình (tính theo khung story point của mình):

| STT | Task | Story Point | Tiến độ |
| --- | --- | --- | --- |
| 1 | Lên danh sách các trang đang có | 10 | Done |
| 2 | Lên danh sách các module trên từng trang & luồng logic giữa các module | 10 | |
| 3 | Dựng khung JS theo scaleApp | 10 | Done |
| 4 | Hệ thống hóa lại code hiện tại | 100 | |
| 5 | Đổ dần code hiện tại vào khung code mới (cuốn chiếu) | 100 | Done (xong header) |
| Dự kiến ||| 130 Point/13 ngày | 