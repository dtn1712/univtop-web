var ajax = function (sandbox) {
    var _this = this;

    _this.init = function (data) {
        _this.data = data;
    };

    _this.destroy = function () { };

    /**
     * @module handleAjaxCall
     * @description handle all ajax calls
     */
    _this.handleAjaxCall = function (options) {

        var defaultOptions = {
            ajaxOptions: {
                type: 'GET',
                //headers: {
                //    'RequestVerificationToken': globalData.tokenHeaderValue
                //},
            },
            channels: {
                request: 'ajax/request',
                response: 'ajax/response'
            }
        };

        options = $.extend(true, {}, defaultOptions, options);

        $
            .ajax(options.ajaxOptions)
            .always(function(response, status, jqXHR){
                var data = {
                    response: response,
                    status: status,
                    ajaxObject: jqXHR
                };

                
                //run callback
                if('function' === typeof options.callback){
                    return options.callback(data);
                }

                //response channel
                if(options.channels && 'string' === typeof options.channels.response){
                    return sandbox.emit(options.channels.response, data);
                }
            })
        ;
    };

    /**
     * register channels
     */
    sandbox.on('ajax/request', function (options) {

        _this.handleAjaxCall(options);
    });

    return {
        init: _this.init,
        destroy: _this.destroy
    }
};

module.exports = ajax;