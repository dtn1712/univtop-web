'use strict';

var moduleWindow = function(sandbox){
    var _this = this;

    _this.init = function(data){
       
        if ($("#page_type").val() == "account_password_reset_from_key") {
            setTimeout(function() {
                sandbox.emit('modal/resetPassword/show');
            },400);
        }
        
        var $body = $('body');

        $body
            .on('click', '[data-target=modal-login]', function(){
                sandbox.emit('modal/login/show');
            })
            // .on('click', '[data-target=modal-register]', function(){
            //     sandbox.emit('modal/register/show');
            // })
            .on('click', '[data-target=modal-forgot-password]', function(){
                sandbox.emit('modal/forgotPassword/show');
            })
            .on('click', '[data-target=modal-sign-up]', function(){
                sandbox.emit('modal/signUp/show');
            })
            .on('click', '.question [data-action="expand"]', function(event){
                event.preventDefault();

                var $this = $(this),
                    $currentQuestion = $this.closest('.question');

                if ($currentQuestion.attr("data-is-expanded") == 0) {
                    if($currentQuestion.length){
                        $currentQuestion.attr('data-is-expanded', 1);
                    }
                } else {
                    $currentQuestion.attr('data-is-expanded', 0);
                }
            })
            .on("click",'.question [data-action="full-content"]',function(event) {
                event.preventDefault();

                var $this = $(this),
                    $currentText = $this.closest('.text');

                $currentText.find(".full-content").removeClass("hide");
                $currentText.find(".brief-content").addClass("hide");

                $this.addClass("hide");
            });
        ;

    }
    _this.destroy = function(){}

    return {
        init: _this.init,
        destroy: _this.destroy
    }
}

module.exports = moduleWindow;