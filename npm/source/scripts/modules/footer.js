/**
 *  scripts for footer
 * @todo initFormNewsletter
 * @todo handleIconScrollToTop
 */
var footer = function (sandbox) {
    var _this = this;

    /**
     * @module footer/init
     * @param options
     */
    _this.init = function(options){
    };

    /**
     * @module footer/destroy
     */
    _this.destroy = function(){

    };

    return {
        init: _this.init,
        destroy: _this.destroy
    };
};

module.exports = footer;