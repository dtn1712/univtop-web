/**
 * @section header
 */
/**
 *  scripts for header
 * @todo initHeaderSegmentChooseLocation
 * @todo initPopupOrderTracking
 * @todo initPopupRegisterLogin
 * @todo initPopupCart
 * @todo initHeaderSegmentUserInfo (for already logged user)
 * @done initStickyHeader
 */
var header = function (sandbox) {
    var _this = this;

    _this.initToggle = function(){
        _this.objects.$headerToggle.on('click', function(){
            _this.objects.$body.toggleClass('is-menu-expanded');
        });
    }

    /**
     * @module header
     * @function init
     * @param {Object} options
     */
    _this.init = function(data){

        _this.data = data || {};

        _this.DOMSelectors = {};
        _this.DOMSelectors.headerToggle = '#header__toggle';
        _this.DOMSelectors.body = 'body';

        _this.objects = {};
        _this.objects.$headerToggle = $(_this.DOMSelectors.headerToggle);
        _this.objects.$body = $(_this.DOMSelectors.body);

        _this.initToggle();
    };

    _this.destroy = function(){};


    return {
        init: _this.init,
        destroy: _this.destroy
    };
};

module.exports = header;