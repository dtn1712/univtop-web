'use strict';

var moduleModal = function(sandbox){
    var _this = this;

    _this.hideAllModals = function(){
        bootbox.hideAll();
    }

    _this.showModalLogin = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'Log in',
                subtitle: 'Welcome back!',
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_login__form" action='/account/login/' method='POST'>
                <input type="hidden" name="csrfmiddlewaretoken" value="">
                <div class='message' style='display:none'></div>
                <div class="fields">
                    <div class="field">
                        <label for="modal_login__form__input_email">Email</label>
                        <input type="text" id="modal_login__form__input_email" name="email"/>
                    </div>
                    <div class="field">
                        <label for="modal_login__form__input_password">Password</label>
                        <input type="password" id="modal_login__form__input_password" name="password"/>
                        <div class="text-right"><a data-target="modal-forgot-password">Forgot your password?</a></div>
                    </div>
                </div>
                <div class="fields">
                    <div class="error field">
                        <div class="list">
                            <div id='modal_login__form__input_submit-error' class="item" style='display:none'></div>
                        </div>
                    </div>
                </div>
                <div class="inline fields">
                    <div class="field">
                        <button class="univtop button" type="submit">Log In!</button>
                    </div>
                    <div class="field">
                        <p>
                            Don't have an account?<br/>
                            <a data-target="modal-sign-up">Sign Up</a>
                        </p>
                    </div>
                </div>
            </form>
            <div id='login_loading_icon'></div>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-login',
            title: title,
            message: message,
            onEscape: true,
            backdrop: 'static'
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_login__form');
            var csrftoken = Cookies.get('csrftoken');
            $("#modal_login__form input[name='csrfmiddlewaretoken']").val(csrftoken);

            $form.on('submit', function(event){
                event.preventDefault();
            });

            $form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    }
                },
                messages: {
                    email: {
                        required: "Please input your email",
                        email: "Please enter correct email"
                    },
                    password: {
                        required: "Please input your password",
                        minlength: "Password has to be at least 6 characters"
                    }
                },
                errorContainer: '#modal_login__form .fields .field.error',
                errorLabelContainer: '#modal_login__form .fields .field.error .list',
                errorElement: 'div',
                errorClass: 'item',

                submitHandler: function(form) { 
                    var spinner = new Spinner().spin(document.getElementById('login_loading_icon'));
                    $("#modal_login__form__input_submit-error").html("");
                    sandbox.emit('ajax/request', {
                        ajaxOptions: {
                            url: $("#api_url").val() + "/api/v1/auth/login/",
                            type: 'POST',
                            data: {
                                "email": $("#modal_login__form input[name='email']").val(),
                                "password": $("#modal_login__form input[name='password']").val()
                            }
                        },
                        callback: function(data){
                            if (data.status == 'success') {
                                form.submit();
                            } else {
                                spinner.stop();
                                $("#modal_login__form .fields .error").show();
                                $("#modal_login__form .fields .error .list").show();
                                $("#modal_login__form .fields .error .list .item").hide();
                                if (data.response.status == 401) {
                                    console.log("inside");
                                    $("#modal_login__form__input_submit-error").html("Password is incorrect. Please try again");
                                    $("#modal_login__form__input_submit-error").show();
                                } else {
                                    $("#modal_login__form__input_submit-error").html("Unable to process your request. Please try again");
                                    $("#modal_login__form__input_submit-error").show();
                                }
                            }
                        }
                    });
                }
            });
        });
    };

    _this.showModalForgotPassword = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'Forgot your password?'
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_forgot_password__form">
                <div class='message' style='display:none'>
                    We have sent reset password link to your email. Please follow the instruction to reset your password
                </div>
                <div class="fields">
                    <div class="field">
                        <label for="modal_forgot_password__form__input_email">Type your email here</label>
                        <input type="text" id="modal_forgot_password__form__input_email" name="email"/>
                    </div>
                </div>
                <div class="fields">
                    <div class="error field">
                        <div class="list">
                            <div id='modal_forgot_password__form__input_submit-error' class="item" style='display:none'></div>
                        </div>
                    </div>
                </div>
                <div class="fields">
                    <div class="field text-center">
                        <button class="univtop button" type="submit">Reset Password</button>
                    </div>
                </div>
            </form>
            <div id='forgot_password_loading_icon'></div>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-forgot-password',
            title: title,
            message: message,
            onEscape: true,
            backdrop: 'static'
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_forgot_password__form');

            $form.on('submit', function(event){
                event.preventDefault();
            });

            $form.validate({
                onclick: false,
                onfocusout: false,
                onkeyup: false,
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    email: {
                        required: "Please input your email",
                        email: "Please enter correct email"
                    },
                },
                errorContainer: '#modal_forgot_password__form .fields .field.error',
                errorLabelContainer: '#modal_forgot_password__form .fields .field.error .list',
                errorElement: 'div',
                errorClass: 'item',

                submitHandler: function(form) { 
                    var spinner = new Spinner().spin(document.getElementById('forgot_password_loading_icon'));
                    $("#modal_forgot_password__form__input_submit-error").html("");
                    sandbox.emit('ajax/request', {
                        ajaxOptions: {
                            url: $("#api_url").val() + "/api/v1/auth/password_reset",
                            type: 'POST',
                            data: {
                                "email": $("#modal_forgot_password__form input[name='email']").val()
                            }
                        },
                        callback: function(data){
                            spinner.stop();
                            if (data.status == 'success') {
                                $("#modal_forgot_password__form .fields").hide();
                                $("#modal_forgot_password__form .message").show();
                            } else {
                                $("#modal_forgot_password__form .fields .error").show();
                                $("#modal_forgot_password__form .fields .error .list").show();
                                $("#modal_forgot_password__form .fields .error .list .item").hide();
                                if (data.response.status == 404) {
                                    $("#modal_forgot_password__form__input_submit-error").html(data.response.responseJSON.error.message);
                                    $("#modal_forgot_password__form__input_submit-error").show();
                                } else {
                                    $("#modal_forgot_password__form__input_submit-error").html("Unable to process your request. Please try again");
                                    $("#modal_forgot_password__form__input_submit-error").show();
                                }
                            }
                        }
                    });
                }
            });

        });


        $dialog.on('hidden.bs.modal',function() {
            $("#modal_forgot_password__form .fields").show();
            $("#modal_forgot_password__form .message").hide();
        });
    };

    _this.showModalSignUp = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'SIGN UP NOW'
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_sign_up__form" action='/account/login/' method='POST'>
                <input type="hidden" name="csrfmiddlewaretoken" value="">
                <div class="inline fields">
                    <div class="six wide field">
                        <label for="modal_sign_up__form__input_first_name">First Name</label>
                        <input type="text" id="modal_sign_up__form__input_first_name" name="firstName"/>
                    </div>
                    <div class="six wide field">
                        <label for="modal_sign_up__form__input_last_name">Last Name</label>
                        <input type="text" id="modal_sign_up__form__input_last_name" name="lastName"/>
                    </div>
                </div>
                <div class="fields">
                    <div class="field">
                        <label for="modal_sign_up__form__input_email">Email</label>
                        <input type="text" id="modal_sign_up__form__input_email" name="email"/>
                    </div>
                    <div class="field">
                        <label for="modal_sign_up__form__input_password">Password</label>
                        <input type="password" id="modal_sign_up__form__input_password" name="password"/>
                    </div>
                    <div class="field">
                        <label for="modal_sign_up__form__input_password_confirm">Confirm Password</label>
                        <input type="password" id="modal_sign_up__form__input_password_confirm" name="password_confirm"/>
                        <div class="text-right">Already on Univtop? <a data-target="modal-login">Sign In</a></div>
                    </div>
                </div>

                <div class="fields">
                    <div class="error field">
                        <div class="list">
                            <div id='modal_sign_up__form__input_submit-error' class="item" style='display:none'></div>
                        </div>
                    </div>
                </div>
                <div class="fields">
                    <div class="field">
                        <button class="univtop button" type="submit">Let's go!</button>
                    </div>
                </div>
            </form>
            <div id='signup_loading_icon'></div>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-signup',
            title: title,
            message: message,
            onEscape: true,
            backdrop: 'static'
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_sign_up__form');
            var csrftoken = Cookies.get('csrftoken');
            $("#modal_sign_up__form input[name='csrfmiddlewaretoken']").val(csrftoken);

            $form.on('submit', function(event){
                event.preventDefault();
            });

            jQuery.validator.addMethod("matchPassword", function(value, element) {
                var password1 = $("#modal_sign_up__form input[name='password']").val();
                var password2 = $("#modal_sign_up__form input[name='password_confirm']").val();
                return password1 == password2;
            });

            $form.validate({
                onclick: false,
                onfocusout: false,
                onkeyup: false,
                rules: {
                    firstName: "required",
                    lastName: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    password_confirm: {
                        required: true,
                        matchPassword: true,
                    }
                },
                messages: {
                    email: {
                        required: "Please input your email",
                        email: "Please enter correct email"
                    },
                    firstName: {
                        required: "Please input your first name",
                    },
                    lastName: {
                        required: "Please input your last name",
                    },
                    password: {
                        required: "Please input your password",
                        minlength: "Password has to be at least 6 characters"
                    },
                    password_confirm: {
                        required: "Please confirm your password",
                        matchPassword: "Confirm password does not match. Please try again"
                    }
                },
                errorContainer: '#modal_sign_up__form .fields .field.error',
                errorLabelContainer: '#modal_sign_up__form .fields .field.error .list',
                errorElement: 'div',
                errorClass: 'item',

                submitHandler: function(form) { 
                    $("#modal_sign_up__form__input_submit-error").html("");
                    var spinner = new Spinner().spin(document.getElementById('signup_loading_icon'));
                    var formData = {
                        "email": $("#modal_sign_up__form input[name='email']").val(),
                        "first_name": $("#modal_sign_up__form input[name='firstName']").val(),
                        "last_name": $("#modal_sign_up__form input[name='lastName']").val(),
                        "password": $("#modal_sign_up__form input[name='password']").val(),
                        "confirm_type": "activate_link"
                    }
                    sandbox.emit('ajax/request', {
                        ajaxOptions: {
                            url: $("#api_url").val() + "/api/v1/auth/signup/",
                            type: 'POST',
                            data: formData
                        },
                        callback: function(data){
                            if (data.status == 'success') {
                                form.submit();
                            } else {
                                spinner.stop();
                                $("#modal_sign_up__form .fields .error").show();
                                $("#modal_sign_up__form .fields .error .list").show();
                                $("#modal_sign_up__form .fields .error .list .item").hide();
                                if (data.response.status == 400) {
                                    $("#modal_sign_up__form__input_submit-error").html(data.response.responseJSON.error.message);
                                    $("#modal_sign_up__form__input_submit-error").show();
                                } else {
                                    $("#modal_sign_up__form__input_submit-error").html("Unable to process your request. Please try again");
                                    $("#modal_sign_up__form__input_submit-error").show();
                                }
                            }
                        }
                    });
                }
            });
        });
    };


    _this.showModalResetPassword = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'Reset your password'
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_reset_password__form" method='POST'>
                <input type="hidden" name="csrfmiddlewaretoken" value="">
                <div class="fields">
                    <div class="field">
                        <label for="modal_reset_password__form__input_password1">New Password</label>
                        <input type="password" id="modal_reset_password__form__input_password1" name="password1"/>
                    </div>
                    <div class="field">
                        <label for="modal_reset_password__form__input_password2">Confirm Password</label>
                        <input type="password" id="modal_reset_password__form__input_password2" name="password2"/>
                    </div>
                </div>
                <div class="fields">
                    <div class="error field">
                        <div class="list">
                            <div id='modal_reset_password__form__input_submit-error' class="item" style='display:none'></div>
                        </div>
                    </div>
                </div>
                <div class="fields">
                    <div class="field text-center">
                        <button class="univtop button" type="submit">Reset Password</button>
                    </div>
                </div>
            </form>
            <div id='reset_password_loading_icon'></div>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-reset-password',
            title: title,
            message: message,
            onEscape: true,
            backdrop: 'static'
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_reset_password__form');
            var csrftoken = Cookies.get('csrftoken');
            $("#modal_reset_password__form input[name='csrfmiddlewaretoken']").val(csrftoken);

            $form.on('submit', function(event){
                event.preventDefault();
            });

            jQuery.validator.addMethod("matchPassword", function(value, element) {
                var password1 = $("#modal_reset_password__form input[name='password1']").val();
                var password2 = $("#modal_reset_password__form input[name='password2']").val();
                return password1 == password2;
            });

            $form.validate({
                onclick: false,
                onfocusout: false,
                onkeyup: false,
                rules: {
                    password1: {
                        required: true,
                        minlength: 6
                    },
                    password2: {
                        required: true,
                        minlength: 6,
                        matchPassword: true
                    }
                },
                messages: {
                    password1: {
                        required: "Please input your email",
                        minlength: "Password has to be at least 6 characters"
                    },
                    password2: {
                        required: "Please confirm your password",
                        matchPassword: "Confirm password does not match. Please try again"
                    }
                },
                errorContainer: '#modal_reset_password__form .fields .field.error',
                errorLabelContainer: '#modal_reset_password__form .fields .field.error .list',
                errorElement: 'div',
                errorClass: 'item',

                submitHandler: function(form) { 
                    form.submit();
                }
            });
        });

        $dialog.on('hide.bs.modal',function() {
            window.location.href = "/"
        });
    };


    _this.showInitModal = function() {
        var init_modal = getUrlParameter("modal");
        if (init_modal == 'login') {
            _this.hideAllModals();
            setTimeout(function() {
                _this.showModalLogin();
            },400)
        } else if (init_modal == 'signup') {
            _this.hideAllModals();
            setTimeout(function() {
                _this.showModalSignUp();
            },400);
        } else if (init_modal == 'forgot_password') {
            _this.hideAllModals();
            setTimeout(function() {
                _this.showModalForgotPassword();
            },400);
        } 
    }

    sandbox.on('modal/login/show', function(){
        _this.hideAllModals();
        _this.showModalLogin();
    });

    sandbox.on('modal/forgotPassword/show', function(){
        _this.hideAllModals();
        _this.showModalForgotPassword();
    });

    sandbox.on('modal/resetPassword/show', function(){
        _this.hideAllModals();
        _this.showModalResetPassword();
    });

    sandbox.on('modal/signUp/show', function(){
        _this.hideAllModals();
        _this.showModalSignUp();
    });

    _this.init = function(data){
        _this.data = {};
        _this.data.titleTemplate = multiline(function(){/*!@preserve
             <div class="title">{{title}}</div>
             <div class="subtitle">{{subtitle}}</div>
         */console.log});
        _this.showInitModal();
    } 
    _this.destroy = function(){}

    return {
        init: _this.init,
        destroy: _this.destroy
    }
}

module.exports = moduleModal;