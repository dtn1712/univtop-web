swig.setFilter('calendarTime', function (input) {
    return moment(input).calendar();
});

swig.setFilter('elapseTime', function (input) {
	return moment(input).fromNow();
});