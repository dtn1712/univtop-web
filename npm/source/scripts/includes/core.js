var ADRSandbox = require('./../core/sandbox'),
    ADRCore = new scaleApp.Core(ADRSandbox)
;

ADRCore.use(scaleApp.plugins.ls);
ADRCore.use(scaleApp.plugins.util);
ADRCore.use(scaleApp.plugins.submodule, {
    inherit: true,             // use all plugins from the parent's Core
    use: ['ls','submodule', 'util'],        // use some additional plugins
    useGlobalMediator: true,   // emit and receive all events from the parent's Core
});

module.exports = ADRCore;