'use strict';

var pageContact = function(sandbox){
    var _this = this;

    _this.handleForm = function(){
        var $form = $('#segment_2__form'),
            $listErrors = $form.find('.group.error')
        ;

        $form.on('submit', function(event){
            event.preventDefault();
        });

        $form.validate({
            debug: true,
            rules: {
                "name": "required",
                "email": {
                    required: true,
                    email: true
                },
                "subject": "required",
                "message": {
                    required: true
                }
            },
            messages: {
                name: "Please specify your name",
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                },
                "subject": "Please specify your subject",
                "message": "Please specify your message"
            },
            errorContainer: '#segment_2__form .group.error',
            errorLabelContainer: '#segment_2__form .group.error .list',
            errorElement: 'div',
            errorClass: 'item',
            //errorPlacement: function(error, element){
            //    $listErrors.html('');
            //    $('<div class="item"></div>').html($(error).text()).appendTo($listErrors);
            //    element.addClass('error');
            //},
            submitHandler: function(form) { 
                form.submit();
            }
        });
    }

    _this.init = function(data){
        _this.handleForm();
    }
    _this.destroy = function(){}
}

module.exports = pageContact;