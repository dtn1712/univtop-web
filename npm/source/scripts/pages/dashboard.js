'use strict';

var pageDashboard = function(sandbox){
    var _this = this;

    _this.handleTabs = function(){
    	var $menu = $('#segment_2__menu'),
            $tabs = $('#segment_3__segment_questions__tabs')
        ;

        $menu.on('click', '>.item', function(){
            var $this = $(this),
                $target = $($this.data('target'))
            ;

            $this
                .addClass('active')
                .siblings('.active')
                .removeClass('active')
            ;

            if($target.length){
                $target
                    .addClass('active')
                    .siblings('.active')
                    .removeClass('active')
                ;

                var spinner = new Spinner().spin(document.getElementById('loading_icon'));
                var data = {
                    "username": $("#user_login_username").val(),
                    "question_type": $target.data("question-type"),
                    "start_index": 0
                }

                ajaxGet("/ajax/user/load_user_questions",data,function(response) {
                    $target.find(".list.questions").html(response['content']);
                    $target.find(".start-index").val(response['start_index']);
                    $target.find(".all-item-loaded").val(response['is_all_items_loaded']);
                    _this.handleAnswer();
                    spinner.stop();
                })
            }
        });

        $menu.children().first().trigger('click');

        var win = $(window);

        win.scroll(function() {
            if ($(document).height() - win.height() == win.scrollTop()) {

                var $target = $tabs.find(".active");
        
                if ($target.find(".all-item-loaded").val() != "true") {
                    var spinner = new Spinner().spin(document.getElementById('loading_icon'));

                    var data = {
                        "username": $("#user_login_username").val(),
                        "question_type": $target.data("question-type"),
                        "start_index": $target.find(".start-index").val()
                    }

                    ajaxGet("/ajax/user/load_user_questions",data,function(response){
                        spinner.stop();
                        if (parseInt(response['num_item']) > 0) {
                            $target.find(".list.questions").append(response['content']);
                            $target.find(".start-index").val(response['start_index']);
                            $target.find(".all-item-loaded").val(response['is_all_items_loaded']);
                            _this.handleAnswer();
                        }
                    })
                }
            }
        });
    }

    _this.handleAnswer = function(){

        _this.templates.answerItem = multiline(function(){/*
        <div class="univtop card">
            <div class="thumbnail">
                <img src='{{answer.user_post.avatar}}' width='56' height='56'/>
                <a href="#" class="meta name">{{answer.user_post.fullname}}</a>
            </div>
            <div class="content">
                <div class="segment">
                    <div class="header">
                        <div class="meta time">
                            {% if answer.edit_date %}
                                Edited {{answer.edit_date|elapseTime}}
                            {% else %}
                                {{answer.create_date|elapseTime}}
                            {% endif %}
                        </div>
                    </div>
                    <div class="body">
                        <div class="summary">{{answer.content}}</div>
                    </div>
                    <div class="footer">
                        {% if answer.is_user_login_vote %}
                        <a class="univtop label vote" data-action='unvote-answer'>
                        {% else %}
                        <a class="univtop label vote" data-action='vote-answer'>
                        {% endif %}
                            <i class="univtop icon heart" ></i><span class='value'>{{answer.total_vote}}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        */console.log});

        var $tabs = $('#segment_3__segment_questions__tabs');

        $tabs.find(".univtop.form.answer").on("submit",function(event){
            event.preventDefault();
            
            var $this = $(this),
                $content = $this.find("textarea[name='content']").val();

            var user_answer_id = $this.data("answer-id");

            if ($content.length) {
                var spinner = new Spinner().spin(document.getElementById('loading_icon'));
                var question_id = $this.attr("data-question-id");
                var data = {
                    "content": $content
                }

                if (isUndefined(user_answer_id)) {
                    var ajax_data = { 
                        "type": "post",
                        "url": $("#api_url").val() + '/api/v1/question/' + question_id + "/add_answer/",
                        "data": JSON.stringify(data)
                    }
                    ajaxPost("/ajax/api/call",ajax_data,function(data){
                        spinner.stop();
                        if (data['status'] == 'success') {
                            var item_content = swig.render(_this.templates.answerItem, {locals: {
                                    answer: data['response']['answer']
                                }
                            });
                            var html = "<div class='item answer' data-answer-id='" + data['response']['answer_id'] + "'>" + item_content + "</div>"
                            var $answersList = $this.parent().find(".list.answers");
                            $answersList.append(html);
                            $this.attr("data-answer-id",data['response']['answer_id']);
                            
                        }
                    })
                } else {
                    var ajax_data = { 
                        "type": "patch",
                        "url": $("#api_url").val() + '/api/v1/answer/' + user_answer_id + "/",
                        "data": JSON.stringify(data)
                    }
                    ajaxPost("/ajax/api/call",ajax_data,function(data){
                        spinner.stop();
                        if (data['status'] == 'success') {
                            var html = swig.render(_this.templates.answerItem, {locals: {
                                    answer: data['response']
                                }
                            });
                            var $answerItem = $(".answer.item[data-answer-id='" + user_answer_id + "']");
                            $answerItem.html(html);
                            if (!$answerItem.is(":in-viewport")) {
                                $("html, body").animate({
                                    scrollTop: $answerItem.offset().top
                                }, 300);
                            }  
                        }
                    })
                }
            }
        });
    }

    _this.handleVote = function() {
        
        var $tabs = $('#segment_3__segment_questions__tabs');

        $tabs.on("click",".item.question [data-action='vote-question']",function() {
            var $this = $(this);
            
            var $questionItem = $this.closest(".item.question");
            var question_id = $questionItem.data("id");

            var ajax_data = { 
                "type": "post",
                "url": $("#api_url").val() + "/api/v1/question/" + question_id + "/vote/",
                "data": JSON.stringify({})
            }

            var spinner = new Spinner().spin(document.getElementById('loading_icon'));

            ajaxPost("/ajax/api/call",ajax_data,function(data){
                spinner.stop();
                if (data['status'] == 'success') {
                    $this.attr("data-action","unvote-question");
                    var current_vote = parseInt($this.find("span.value").text());
                    $this.find("span.value").text(current_vote+1);
                }
            })
            
        })

        $tabs.on("click",".item.answer [data-action='vote-answer']",function() {
            var $this = $(this);
            
            var $answerItem = $this.closest(".item.answer");
            var answer_id = $answerItem.data("id");

            var ajax_data = { 
                "type": "post",
                "url": $("#api_url").val() + "/api/v1/answer/" + answer_id + "/vote/",
                "data": JSON.stringify({})
            }

            var spinner = new Spinner().spin(document.getElementById('loading_icon'));

            ajaxPost("/ajax/api/call",ajax_data,function(data){
                spinner.stop();
                if (data['status'] == 'success') {
                    $this.attr("data-action","unvote-answer");
                    var current_vote = parseInt($this.find("span.value").text());
                    $this.find("span.value").text(current_vote+1);
                }
            })
            
        })
    }

    _this.handleUnvote = function() {

        var $tabs = $('#segment_3__segment_questions__tabs');

        $tabs.on("click",".item.question [data-action='unvote-question']",function() {
            var $this = $(this);
            
            console.log("unvote");

            var $questionItem = $this.closest(".item.question");
            var question_id = $questionItem.data("id");

            var ajax_data = { 
                "type": "post",
                "url": $("#api_url").val() + "/api/v1/question/" + question_id + "/unvote/",
                "data": JSON.stringify({})
            }

            var spinner = new Spinner().spin(document.getElementById('loading_icon'));

            ajaxPost("/ajax/api/call",ajax_data,function(data){
                spinner.stop();
                if (data['status'] == 'success') {
                    $this.attr("data-action","vote-question");
                    var current_vote = parseInt($this.find("span.value").text());
                    $this.find("span.value").text(current_vote-1);
                }
            })
        });


        $tabs.on("click",".item.answer [data-action='unvote-answer']",function() {
            var $this = $(this);
            
            var $answerItem = $this.closest(".item.answer");
            var answer_id = $answerItem.data("id");

            var ajax_data = { 
                "type": "post",
                "url": $("#api_url").val() + "/api/v1/answer/" + answer_id + "/unvote/",
                "data": JSON.stringify({})
            }

            var spinner = new Spinner().spin(document.getElementById('loading_icon'));

            ajaxPost("/ajax/api/call",ajax_data,function(data){
                spinner.stop();
                if (data['status'] == 'success') {
                    $this.attr("data-action","vote-answer");
                    var current_vote = parseInt($this.find("span.value").text());
                    $this.find("span.value").text(current_vote-1);
                }
            })
            
        })
    }

    _this.handleEditProfile = function(){

    }

    _this.handleFollow = function() {
        var $tabs = $('#segment_3__segment_questions__tabs');

        $tabs.on("click",".item.question [data-action='follow']",function(event) {
            event.preventDefault();
            var $this = $(this);
            
            var $questionItem = $this.closest(".item.question");
            var question_id = $questionItem.data("id");

            var ajax_data = { 
                "type": "post",
                "url": $("#api_url").val() + "/api/v1/user/follow/question/" + question_id + "/",
                "data": JSON.stringify({})
            }

            var spinner = new Spinner().spin(document.getElementById('loading_icon'));

            ajaxPost("/ajax/api/call",ajax_data,function(data){
                spinner.stop();
                if (data['status'] == 'success') {
                    $this.attr("data-action","unfollow");
                    $this.text("FOLLOWED");
                }
                
            })
            
        })
    }

    _this.handleUnfollow = function() {
        var $tabs = $('#segment_3__segment_questions__tabs');

        $tabs.on("click",".item.question [data-action='unfollow']",function(event) {
            event.preventDefault();
            var $this = $(this);
            
            var $questionItem = $this.closest(".item.question");
            var question_id = $questionItem.data("id");

            var ajax_data = { 
                "type": "post",
                "url": $("#api_url").val() + "/api/v1/user/unfollow/question/" + question_id + "/",
                "data": JSON.stringify({})
            }

            var spinner = new Spinner().spin(document.getElementById('loading_icon'));

            ajaxPost("/ajax/api/call",ajax_data,function(data){
                spinner.stop();
                if (data['status'] == 'success') {
                    $this.attr("data-action","follow");
                    $this.text("FOLLOW");
                }
                
            })
            
        })
    }

    _this.init = function(data){
    	_this.data = {};
        _this.templates = {};
        _this.handleTabs();
        _this.handleEditProfile();
        _this.handleAnswer();
        _this.handleVote();
        _this.handleUnvote();
        _this.handleFollow();
        _this.handleUnfollow();
    }

    _this.destroy = function(){}
}

module.exports = pageDashboard;