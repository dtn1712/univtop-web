'use strict';

var pageQuestions = function(sandbox){
    var _this = this;

    _this.handleFormSearch = function(){
        var $input = $('#form_ask_question__input');

        var inputMagicSuggest = $input.magicSuggest({
            data: [
                {
                    name: 'Question 1',
                    href: '/pages/contact-us.html'
                },
                {
                    name: 'Question 2',
                    href: '/pages/question.html'
                }
            ],
            hideTrigger: true,
            selectFirst: true,
            placeholder: '',
            renderer: function(data){
                return '<a href="'+data.href+'">'+data.name+'</a>';
            }
        });

        $(inputMagicSuggest).on('selectionchange', function(){
            var selectedItems = this.getSelection();
            if(selectedItems.length){
                var selectedItem = selectedItems[0];
                if(selectedItem.href){
                    window.location.assign(selectedItem.href);
                }
            }
        });
    }

    _this.handleTabs = function(){
        _this.templates.tab = multiline(function(){/*
         <div class="list questions">
         {% for question in questions %}
            <div class="item question" data-id="{{question.id}}" data-is-expanded="0">
                <div class="univtop card">
                    <div class="thumbnail">
                        <img src="{{question.user_post.avatar}}" alt="" width="72" height="72">
                        <a href="/user/{{question.user_post.username}}" class="meta name">{{question.user_post.first_name}} {{question.user_post.last_name}}</a>
                    </div>
                    <div class="content">
                        <div class="segment">
                            <div class="header">
                                <div class="actions">
                                    <a href="#" data-action="follow" data-question-id="{{question.id}}" data-is-followed="0">Follow</a>
                                </div>
                                <div class="meta time">{{question.create_date}}</div>
                                <div class="tags">
                                {% for tag in question.tags %}
                                    <a class="tag" data-id="{{tag.id}}">{{tag.value}}</a>
                                {% endfor %}
                                </div>
                            </div>

                            <div class="body">
                                <div class="summary">{{question.title}}</div>
                                <div class="extra text">
                                    {{question.content}}
                                    {% if question.latest_answer %}<a href="#" data-action="expand">more</a>{% endif %}
                                </div>
                            </div><!-- end .body -->

                            <div class="footer">
                                <a class="univtop label">
                                    <i class="univtop icon heart"></i>70
                                </a>

                                <a class="univtop label">
                                    <i class="univtop icon chat-bubble"></i>70
                                </a>
                            </div><!-- end .footer -->
                        </div>
                    </div>
                    {% if question.latest_answer %}
                    <div class="answers">
                        <div class="item">
                            <div class="univtop card">
                                <div class="thumbnail">
                                    <img src="{{question.user_post.avatar}}" alt="" width="56" height="56">
                                    <a href="#" class="meta name">{{question.latest_answer.user_post_fullname}}</a>
                                </div>
                                <div class="content">
                                    <div class="segment">
                                        <div class="body">
                                            <div class="summary">{{question.latest_answer.content}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {% endif %}

                    <form class="univtop form" data-form="reply" data-question-id="{{question.id}}">
                        <div class="inline fields">
                            <div class="field avatar">
                                <img src="../assets/images/demo/pages/question/segment-2.avatar-1.png" alt="" width="72" height="72"/>
                                <div class="meta name">YOU</div>
                            </div>
                            <div class="field input">
                                <div class="fields">
                                    <div class="field">
                                        <textarea name="content" placeholder="Add your answer"></textarea>
                                    </div>
                                    <div class="field text-right">
                                        <button type="submit" class="univtop button">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        {% endfor %}
        </div>
        */console.log});

        var $menu = $('#segment_2__menu'),
            $tabs = $('#segment_2__tabs')
        ;

        $menu.on('click', '>.item', function(){
            var $this = $(this),
                $target = $($this.data('target'))
            ;

            $this
                .addClass('active')
                .siblings('.active')
                .removeClass('active')
            ;

            if($target.length){
                $target
                    .addClass('active')
                    .siblings('.active')
                    .removeClass('active')
                ;

                var spinner = new Spinner().spin(document.getElementById('loading_icon'));

                //load tab content
                //get data
                switch($target.data('loading-status')){
                    case 'success':
                        //do nothing
                        break;
                    case 'not-yet':
                    case 'error':
                    default:
                        sandbox.emit('ajax/request', {
                            ajaxOptions: {
                                url: $("#api_url").val() + '/api/v1/question?format=json',
                                data: {limit: 10}
                            },
                            callback: function(data){
                                spinner.stop();
                                if('success' === data.status){
                                    //render
                                    var html = swig.render(_this.templates.tab, {locals: {
                                            questions: data.response.objects
                                        }
                                    });
                                    $target.html(html);

                                    $target.data('loading-status', 'success');
                                } else {
                                    $target.html("There's an error while fetching the content")
                                }
                            }
                        });
                        break;
                }
            }
        });

        $menu.children().first().trigger('click');
    }

    _this.handleAnswer = function(){

        _this.templates.answerItem = multiline(function(){/*
        <div class="item answer" data-answer-id="{{answer_id}}">
            <div class="univtop card">
                <div class="thumbnail">
                    <img src='{{answer.user_post.avatar}}' width='56' height='56'/>
                    <a href="#" class="meta name">{{answer.user_post.fullname}}</a>
                </div>
                <div class="content">
                    <div class="segment">
                        <div class="body">
                            <div class="summary">{{answer.content}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        */console.log});

        var $tabs = $('#segment_3__segment_questions__tabs');

        $tabs.find(".univtop.form.answer").on("submit",function(event){
            event.preventDefault();
            
            var $this = $(this),
                $content = $this.find("textarea[name='content']").val();

            var user_answer_id = $this.data("answer-id");

            if ($content.length) {
                var spinner = new Spinner().spin(document.getElementById('loading_icon'));
                var question_id = $this.attr("data-question-id");
                var data = {
                    "content": $content
                }

                if (isUndefined(user_answer_id)) {
                    var ajax_data = { 
                        "type": "post",
                        "url": $("#api_url").val() + '/api/v1/question/' + question_id + "/add_answer/",
                        "data": JSON.stringify(data)
                    }
                    ajaxPost("/ajax/api/call",ajax_data,function(data){
                        var html = swig.render(_this.templates.answerItem, {locals: {
                                answer: data['response']['answer'],
                                answer_id: data['response']['answer_id']
                            }
                        });
                        var $answersList = $this.parent().find(".list.answers");
                        $answersList.append(html);
                        $this.attr("data-answer-id",data['response']['answer_id']);
                        spinner.stop();
                    })
                } else {
                    var ajax_data = { 
                        "type": "patch",
                        "url": $("#api_url").val() + '/api/v1/answer/' + user_answer_id + "/",
                        "data": JSON.stringify(data)
                    }
                    ajaxPost("/ajax/api/call",ajax_data,function(data){
                        var $answerItem = $(".answer.item[data-answer-id='" + user_answer_id + "']");
                        $answerItem.find(".summary").html(data['response']['content'])
                        if (!$answerItem.is(":in-viewport")) {
                            $("html, body").animate({
                                scrollTop: $answerItem.offset().top
                            }, 300);
                        }  
                        spinner.stop();
                    })
                }
            }
        });
    }

    _this.handleVote = function() {
        var $tabs = $('#segment_3__segment_questions__tabs');

        $tabs.on("click",".univtop.icon.heart",function() {

        })
    }

    _this.init = function(data){
        _this.data = {};
        _this.templates = {};

        _this.handleFormSearch();
        _this.handleTabs();
        _this.handleAnswer();
        _this.handleVote();
    }
    _this.destroy = function(){}

    return {
        init: _this.init,
        destroy: _this.destroy
    }
}

module.exports = pageQuestions;