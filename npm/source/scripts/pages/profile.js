'use strict';

var pageProfile = function(sandbox){
    var _this = this;

    _this.handleTabs = function(){
    	var $menu = $('#segment_2__menu'),
            $tabs = $('#segment_3__segment_questions__tabs')
        ;

        $menu.on('click', '>.item', function(){
            var $this = $(this),
                $target = $($this.data('target'))
            ;

            $this
                .addClass('active')
                .siblings('.active')
                .removeClass('active')
            ;

            if($target.length){
                $target
                    .addClass('active')
                    .siblings('.active')
                    .removeClass('active')
                ;
            }
        });
    }

    _this.init = function(data){
    	_this.data = {};
        _this.templates = {};
        _this.handleTabs();
    }
    _this.destroy = function(){}
}

module.exports = pageProfile;