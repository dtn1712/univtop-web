/**
 * @module pageHome
 */
var pageHome = function(sandbox){
    var _this = this;

    /**
     * @module pageHome
     * @function init
     * @param options
     */
    _this.init = function(options){
        _this.objects = {};
        _this.objects.$segment1 = $('#segment_1');
        _this.objects.$segment2 = $('#segment_2');
        _this.objects.$segment1ToggleScrollDown = $('#segment_1__toggle_scroll_down');

        _this.handleAskForm();
        _this.handleSegment1();
    };

    /**
     * @module pageHome
     * @function destroy
     */
    _this.destroy = function(){}

    /**
     * @module pageHome
     * @function handleAskForm
     */
    _this.handleAskForm = function(){
        
    }

    _this.handleSegment1 = function(){
        _this.objects.$segment1.find('[data-toggle=scrollDown]').on('click', function(){
            $("html, body").animate({
                scrollTop: _this.objects.$segment2.offset().top
            }, 300);
        });
    }

    return ({
        init: _this.init,
        destroy: _this.destroy
    })
};

module.exports = pageHome;