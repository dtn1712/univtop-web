var ADRSandbox = function(core, instanceId, options, moduleId) {

    // define your API
    this.namespace = "ADR";

    // e.g. provide the Mediator methods 'on', 'emit', etc.
    core._mediator.installTo(this);

    // ... or define your custom communication methods
    this.myEmit = function(channel, data){
        core.emit(channel + '/' + instanceId, data);
    };

    // maybe you'd like to expose the instance ID
    this.id = instanceId;

    return this;
};

module.exports = ADRSandbox;