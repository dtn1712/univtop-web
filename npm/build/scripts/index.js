(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var ADRSandbox = require('./../core/sandbox'),
    ADRApplication = new scaleApp.Core(ADRSandbox)
    ;

ADRApplication.use(scaleApp.plugins.ls);
ADRApplication.use(scaleApp.plugins.util);
ADRApplication.use(scaleApp.plugins.submodule, {
    inherit: true,             // use all plugins from the parent's Core
    use: ['ls','submodule', 'util'],        // use some additional plugins
    useGlobalMediator: true,   // emit and receive all events from the parent's Core
});

module.exports = ADRApplication;
},{"./../core/sandbox":2}],2:[function(require,module,exports){
var ADRSandbox = function(core, instanceId, options, moduleId) {

    // define your API
    this.namespace = "ADR";

    // e.g. provide the Mediator methods 'on', 'emit', etc.
    core._mediator.installTo(this);

    // ... or define your custom communication methods
    this.myEmit = function(channel, data){
        core.emit(channel + '/' + instanceId, data);
    };

    // maybe you'd like to expose the instance ID
    this.id = instanceId;

    return this;
};

module.exports = ADRSandbox;
},{}],3:[function(require,module,exports){
'use strict';

(function ($) {
    /**
     * jQuery.ADRDropdown
     * @param options
     * @returns {$.fn.ADRDropdown}
     * @constructor
     */
    $.fn.ADRDropdown = function (options) {
        var defaultSettings = {
            textReplace: false
        };

        var settings = $.extend({},defaultSettings, options);

        this.each(function(){
            var $this = $(this),
                $window = $(window)
            ;

            $this.on('click',function(event){
                $this.toggleClass('open');

                var $target = $(event.target);
                if($.contains($this[0],event.target)){
                    var $text = $this.children('.text:first');
                    //check if it is a button
                    var $textButton = $text.children('.button:first');

                    if(settings.textReplace){
                        if($textButton.length > 0){
                            $textButton.html($target.text());
                        } else {
                            $text.html($target.text());
                        }
                    }
                } else {
                    $this.removeClass('open');
                }

                if($this.hasClass('open')){
                    setTimeout(function(){
                        $window.one('click',function(){
                            $this.removeClass('open');
                        });
                    });
                }
            });
        });

        return this;
    };

}(jQuery));

},{}],4:[function(require,module,exports){
swig.setFilter('calendarTime', function (input) {
    return moment(input).calendar();
});

swig.setFilter('elapseTime', function (input) {
	return moment(input).fromNow();
});
},{}],5:[function(require,module,exports){
(function (global){
/**
 * Some in-house plugins/libraries
 */
require('./includes/jquery.adr-dropdown');
require('./includes/swig');

global.NAMESPACE = 'univtop';

//temporarily expose Application to global for debugging purpose
var Application = require('./core/application');

/**
 * register modules
 */
Application.register('ajax', require('./modules/ajax'));

if ($('.' + NAMESPACE +'.header').length) {
    Application.register('header', require('./modules/header'));
}

Application.register('footer', require('./modules/footer'));
Application.register('modal', require('./modules/modal'));
Application.register('window', require('./modules/window'));

if ($('.page-home').length) {
    Application.register('pageHome',require('./pages/home'));
}

if($('.page-contact').length){
    Application.register('pageContact', require('./pages/contact'));
}

if($('.page-questions').length){
    Application.register('pageQuestions', require('./pages/questions'));
}

if($('.page-profile').length){
    Application.register('pageProfile', require('./pages/profile'));
}

if($('.page-dashboard').length){
    Application.register('pageDashboard', require('./pages/dashboard'));
}

Application.start();

window.UnivtopApplication = Application;
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./core/application":1,"./includes/jquery.adr-dropdown":3,"./includes/swig":4,"./modules/ajax":6,"./modules/footer":7,"./modules/header":8,"./modules/modal":9,"./modules/window":10,"./pages/contact":11,"./pages/dashboard":12,"./pages/home":13,"./pages/profile":14,"./pages/questions":15}],6:[function(require,module,exports){
var ajax = function (sandbox) {
    var _this = this;

    _this.init = function (data) {
        _this.data = data;
    };

    _this.destroy = function () { };

    /**
     * @module handleAjaxCall
     * @description handle all ajax calls
     */
    _this.handleAjaxCall = function (options) {

        var defaultOptions = {
            ajaxOptions: {
                type: 'GET',
                //headers: {
                //    'RequestVerificationToken': globalData.tokenHeaderValue
                //},
            },
            channels: {
                request: 'ajax/request',
                response: 'ajax/response'
            }
        };

        options = $.extend(true, {}, defaultOptions, options);

        $
            .ajax(options.ajaxOptions)
            .always(function(response, status, jqXHR){
                var data = {
                    response: response,
                    status: status,
                    ajaxObject: jqXHR
                };

                
                //run callback
                if('function' === typeof options.callback){
                    return options.callback(data);
                }

                //response channel
                if(options.channels && 'string' === typeof options.channels.response){
                    return sandbox.emit(options.channels.response, data);
                }
            })
        ;
    };

    /**
     * register channels
     */
    sandbox.on('ajax/request', function (options) {

        _this.handleAjaxCall(options);
    });

    return {
        init: _this.init,
        destroy: _this.destroy
    }
};

module.exports = ajax;
},{}],7:[function(require,module,exports){
/**
 *  scripts for footer
 * @todo initFormNewsletter
 * @todo handleIconScrollToTop
 */
var footer = function (sandbox) {
    var _this = this;

    /**
     * @module footer/init
     * @param options
     */
    _this.init = function(options){
    };

    /**
     * @module footer/destroy
     */
    _this.destroy = function(){

    };

    return {
        init: _this.init,
        destroy: _this.destroy
    };
};

module.exports = footer;
},{}],8:[function(require,module,exports){
/**
 * @section header
 */
/**
 *  scripts for header
 * @todo initHeaderSegmentChooseLocation
 * @todo initPopupOrderTracking
 * @todo initPopupRegisterLogin
 * @todo initPopupCart
 * @todo initHeaderSegmentUserInfo (for already logged user)
 * @done initStickyHeader
 */
var header = function (sandbox) {
    var _this = this;

    _this.initToggle = function(){
        _this.objects.$headerToggle.on('click', function(){
            _this.objects.$body.toggleClass('is-menu-expanded');
        });
    }

    /**
     * @module header
     * @function init
     * @param {Object} options
     */
    _this.init = function(data){

        _this.data = data || {};

        _this.DOMSelectors = {};
        _this.DOMSelectors.headerToggle = '#header__toggle';
        _this.DOMSelectors.body = 'body';

        _this.objects = {};
        _this.objects.$headerToggle = $(_this.DOMSelectors.headerToggle);
        _this.objects.$body = $(_this.DOMSelectors.body);

        _this.initToggle();
    };

    _this.destroy = function(){};


    return {
        init: _this.init,
        destroy: _this.destroy
    };
};

module.exports = header;
},{}],9:[function(require,module,exports){
'use strict';

var moduleModal = function(sandbox){
    var _this = this;

    _this.hideAllModals = function(){
        bootbox.hideAll();
    }

    _this.showModalLogin = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'Log in',
                subtitle: 'Welcome back!',
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_login__form" action='/account/login/' method='POST'>
                <input type="hidden" name="csrfmiddlewaretoken" value="">
                <div class='message' style='display:none'></div>
                <div class="fields">
                    <div class="field">
                        <label for="modal_login__form__input_email">Email</label>
                        <input type="text" id="modal_login__form__input_email" name="email"/>
                    </div>
                    <div class="field">
                        <label for="modal_login__form__input_password">Password</label>
                        <input type="password" id="modal_login__form__input_password" name="password"/>
                        <div class="text-right"><a data-target="modal-forgot-password">Forgot your password?</a></div>
                    </div>
                </div>
                <div class="fields">
                    <div class="error field">
                        <div class="list">
                            <div id='modal_login__form__input_submit-error' class="item" style='display:none'></div>
                        </div>
                    </div>
                </div>
                <div class="inline fields">
                    <div class="field">
                        <button class="univtop button" type="submit">Log In!</button>
                    </div>
                    <div class="field">
                        <p>
                            Don't have an account?<br/>
                            <a data-target="modal-sign-up">Sign Up</a>
                        </p>
                    </div>
                </div>
            </form>
            <div id='login_loading_icon'></div>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-login',
            title: title,
            message: message,
            onEscape: true,
            backdrop: 'static'
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_login__form');
            var csrftoken = Cookies.get('csrftoken');
            $("#modal_login__form input[name='csrfmiddlewaretoken']").val(csrftoken);

            $form.on('submit', function(event){
                event.preventDefault();
            });

            $form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    }
                },
                messages: {
                    email: {
                        required: "Please input your email",
                        email: "Please enter correct email"
                    },
                    password: {
                        required: "Please input your password",
                        minlength: "Password has to be at least 6 characters"
                    }
                },
                errorContainer: '#modal_login__form .fields .field.error',
                errorLabelContainer: '#modal_login__form .fields .field.error .list',
                errorElement: 'div',
                errorClass: 'item',

                submitHandler: function(form) { 
                    var spinner = new Spinner().spin(document.getElementById('login_loading_icon'));
                    $("#modal_login__form__input_submit-error").html("");
                    sandbox.emit('ajax/request', {
                        ajaxOptions: {
                            url: $("#api_url").val() + "/api/v1/auth/login/",
                            type: 'POST',
                            data: {
                                "email": $("#modal_login__form input[name='email']").val(),
                                "password": $("#modal_login__form input[name='password']").val()
                            }
                        },
                        callback: function(data){
                            if (data.status == 'success') {
                                form.submit();
                            } else {
                                spinner.stop();
                                $("#modal_login__form .fields .error").show();
                                $("#modal_login__form .fields .error .list").show();
                                $("#modal_login__form .fields .error .list .item").hide();
                                if (data.response.status == 401) {
                                    console.log("inside");
                                    $("#modal_login__form__input_submit-error").html("Password is incorrect. Please try again");
                                    $("#modal_login__form__input_submit-error").show();
                                } else {
                                    $("#modal_login__form__input_submit-error").html("Unable to process your request. Please try again");
                                    $("#modal_login__form__input_submit-error").show();
                                }
                            }
                        }
                    });
                }
            });
        });
    };

    _this.showModalForgotPassword = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'Forgot your password?'
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_forgot_password__form">
                <div class='message' style='display:none'>
                    We have sent reset password link to your email. Please follow the instruction to reset your password
                </div>
                <div class="fields">
                    <div class="field">
                        <label for="modal_forgot_password__form__input_email">Type your email here</label>
                        <input type="text" id="modal_forgot_password__form__input_email" name="email"/>
                    </div>
                </div>
                <div class="fields">
                    <div class="error field">
                        <div class="list">
                            <div id='modal_forgot_password__form__input_submit-error' class="item" style='display:none'></div>
                        </div>
                    </div>
                </div>
                <div class="fields">
                    <div class="field text-center">
                        <button class="univtop button" type="submit">Reset Password</button>
                    </div>
                </div>
            </form>
            <div id='forgot_password_loading_icon'></div>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-forgot-password',
            title: title,
            message: message,
            onEscape: true,
            backdrop: 'static'
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_forgot_password__form');

            $form.on('submit', function(event){
                event.preventDefault();
            });

            $form.validate({
                onclick: false,
                onfocusout: false,
                onkeyup: false,
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    email: {
                        required: "Please input your email",
                        email: "Please enter correct email"
                    },
                },
                errorContainer: '#modal_forgot_password__form .fields .field.error',
                errorLabelContainer: '#modal_forgot_password__form .fields .field.error .list',
                errorElement: 'div',
                errorClass: 'item',

                submitHandler: function(form) { 
                    var spinner = new Spinner().spin(document.getElementById('forgot_password_loading_icon'));
                    $("#modal_forgot_password__form__input_submit-error").html("");
                    sandbox.emit('ajax/request', {
                        ajaxOptions: {
                            url: $("#api_url").val() + "/api/v1/auth/password_reset",
                            type: 'POST',
                            data: {
                                "email": $("#modal_forgot_password__form input[name='email']").val()
                            }
                        },
                        callback: function(data){
                            spinner.stop();
                            if (data.status == 'success') {
                                $("#modal_forgot_password__form .fields").hide();
                                $("#modal_forgot_password__form .message").show();
                            } else {
                                $("#modal_forgot_password__form .fields .error").show();
                                $("#modal_forgot_password__form .fields .error .list").show();
                                $("#modal_forgot_password__form .fields .error .list .item").hide();
                                if (data.response.status == 404) {
                                    $("#modal_forgot_password__form__input_submit-error").html(data.response.responseJSON.error.message);
                                    $("#modal_forgot_password__form__input_submit-error").show();
                                } else {
                                    $("#modal_forgot_password__form__input_submit-error").html("Unable to process your request. Please try again");
                                    $("#modal_forgot_password__form__input_submit-error").show();
                                }
                            }
                        }
                    });
                }
            });

        });


        $dialog.on('hidden.bs.modal',function() {
            $("#modal_forgot_password__form .fields").show();
            $("#modal_forgot_password__form .message").hide();
        });
    };

    _this.showModalSignUp = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'SIGN UP NOW'
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_sign_up__form" action='/account/login/' method='POST'>
                <input type="hidden" name="csrfmiddlewaretoken" value="">
                <div class="inline fields">
                    <div class="six wide field">
                        <label for="modal_sign_up__form__input_first_name">First Name</label>
                        <input type="text" id="modal_sign_up__form__input_first_name" name="firstName"/>
                    </div>
                    <div class="six wide field">
                        <label for="modal_sign_up__form__input_last_name">Last Name</label>
                        <input type="text" id="modal_sign_up__form__input_last_name" name="lastName"/>
                    </div>
                </div>
                <div class="fields">
                    <div class="field">
                        <label for="modal_sign_up__form__input_email">Email</label>
                        <input type="text" id="modal_sign_up__form__input_email" name="email"/>
                    </div>
                    <div class="field">
                        <label for="modal_sign_up__form__input_password">Password</label>
                        <input type="password" id="modal_sign_up__form__input_password" name="password"/>
                    </div>
                    <div class="field">
                        <label for="modal_sign_up__form__input_password_confirm">Confirm Password</label>
                        <input type="password" id="modal_sign_up__form__input_password_confirm" name="password_confirm"/>
                        <div class="text-right">Already on Univtop? <a data-target="modal-login">Sign In</a></div>
                    </div>
                </div>

                <div class="fields">
                    <div class="error field">
                        <div class="list">
                            <div id='modal_sign_up__form__input_submit-error' class="item" style='display:none'></div>
                        </div>
                    </div>
                </div>
                <div class="fields">
                    <div class="field">
                        <button class="univtop button" type="submit">Let's go!</button>
                    </div>
                </div>
            </form>
            <div id='signup_loading_icon'></div>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-signup',
            title: title,
            message: message,
            onEscape: true,
            backdrop: 'static'
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_sign_up__form');
            var csrftoken = Cookies.get('csrftoken');
            $("#modal_sign_up__form input[name='csrfmiddlewaretoken']").val(csrftoken);

            $form.on('submit', function(event){
                event.preventDefault();
            });

            jQuery.validator.addMethod("matchPassword", function(value, element) {
                var password1 = $("#modal_sign_up__form input[name='password']").val();
                var password2 = $("#modal_sign_up__form input[name='password_confirm']").val();
                return password1 == password2;
            });

            $form.validate({
                onclick: false,
                onfocusout: false,
                onkeyup: false,
                rules: {
                    firstName: "required",
                    lastName: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    password_confirm: {
                        required: true,
                        matchPassword: true,
                    }
                },
                messages: {
                    email: {
                        required: "Please input your email",
                        email: "Please enter correct email"
                    },
                    firstName: {
                        required: "Please input your first name",
                    },
                    lastName: {
                        required: "Please input your last name",
                    },
                    password: {
                        required: "Please input your password",
                        minlength: "Password has to be at least 6 characters"
                    },
                    password_confirm: {
                        required: "Please confirm your password",
                        matchPassword: "Confirm password does not match. Please try again"
                    }
                },
                errorContainer: '#modal_sign_up__form .fields .field.error',
                errorLabelContainer: '#modal_sign_up__form .fields .field.error .list',
                errorElement: 'div',
                errorClass: 'item',

                submitHandler: function(form) { 
                    $("#modal_sign_up__form__input_submit-error").html("");
                    var spinner = new Spinner().spin(document.getElementById('signup_loading_icon'));
                    var formData = {
                        "email": $("#modal_sign_up__form input[name='email']").val(),
                        "first_name": $("#modal_sign_up__form input[name='firstName']").val(),
                        "last_name": $("#modal_sign_up__form input[name='lastName']").val(),
                        "password": $("#modal_sign_up__form input[name='password']").val(),
                        "confirm_type": "activate_link"
                    }
                    sandbox.emit('ajax/request', {
                        ajaxOptions: {
                            url: $("#api_url").val() + "/api/v1/auth/signup/",
                            type: 'POST',
                            data: formData
                        },
                        callback: function(data){
                            if (data.status == 'success') {
                                form.submit();
                            } else {
                                spinner.stop();
                                $("#modal_sign_up__form .fields .error").show();
                                $("#modal_sign_up__form .fields .error .list").show();
                                $("#modal_sign_up__form .fields .error .list .item").hide();
                                if (data.response.status == 400) {
                                    $("#modal_sign_up__form__input_submit-error").html(data.response.responseJSON.error.message);
                                    $("#modal_sign_up__form__input_submit-error").show();
                                } else {
                                    $("#modal_sign_up__form__input_submit-error").html("Unable to process your request. Please try again");
                                    $("#modal_sign_up__form__input_submit-error").show();
                                }
                            }
                        }
                    });
                }
            });
        });
    };


    _this.showModalResetPassword = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'Reset your password'
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_reset_password__form" method='POST'>
                <input type="hidden" name="csrfmiddlewaretoken" value="">
                <div class="fields">
                    <div class="field">
                        <label for="modal_reset_password__form__input_password1">New Password</label>
                        <input type="password" id="modal_reset_password__form__input_password1" name="password1"/>
                    </div>
                    <div class="field">
                        <label for="modal_reset_password__form__input_password2">Confirm Password</label>
                        <input type="password" id="modal_reset_password__form__input_password2" name="password2"/>
                    </div>
                </div>
                <div class="fields">
                    <div class="error field">
                        <div class="list">
                            <div id='modal_reset_password__form__input_submit-error' class="item" style='display:none'></div>
                        </div>
                    </div>
                </div>
                <div class="fields">
                    <div class="field text-center">
                        <button class="univtop button" type="submit">Reset Password</button>
                    </div>
                </div>
            </form>
            <div id='reset_password_loading_icon'></div>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-reset-password',
            title: title,
            message: message,
            onEscape: true,
            backdrop: 'static'
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_reset_password__form');
            var csrftoken = Cookies.get('csrftoken');
            $("#modal_reset_password__form input[name='csrfmiddlewaretoken']").val(csrftoken);

            $form.on('submit', function(event){
                event.preventDefault();
            });

            jQuery.validator.addMethod("matchPassword", function(value, element) {
                var password1 = $("#modal_reset_password__form input[name='password1']").val();
                var password2 = $("#modal_reset_password__form input[name='password2']").val();
                return password1 == password2;
            });

            $form.validate({
                onclick: false,
                onfocusout: false,
                onkeyup: false,
                rules: {
                    password1: {
                        required: true,
                        minlength: 6
                    },
                    password2: {
                        required: true,
                        minlength: 6,
                        matchPassword: true
                    }
                },
                messages: {
                    password1: {
                        required: "Please input your email",
                        minlength: "Password has to be at least 6 characters"
                    },
                    password2: {
                        required: "Please confirm your password",
                        matchPassword: "Confirm password does not match. Please try again"
                    }
                },
                errorContainer: '#modal_reset_password__form .fields .field.error',
                errorLabelContainer: '#modal_reset_password__form .fields .field.error .list',
                errorElement: 'div',
                errorClass: 'item',

                submitHandler: function(form) { 
                    form.submit();
                }
            });
        });

        $dialog.on('hide.bs.modal',function() {
            window.location.href = "/"
        });
    };


    _this.showInitModal = function() {
        var init_modal = getUrlParameter("modal");
        if (init_modal == 'login') {
            _this.hideAllModals();
            setTimeout(function() {
                _this.showModalLogin();
            },400)
        } else if (init_modal == 'signup') {
            _this.hideAllModals();
            setTimeout(function() {
                _this.showModalSignUp();
            },400);
        } else if (init_modal == 'forgot_password') {
            _this.hideAllModals();
            setTimeout(function() {
                _this.showModalForgotPassword();
            },400);
        } 
    }

    sandbox.on('modal/login/show', function(){
        _this.hideAllModals();
        _this.showModalLogin();
    });

    sandbox.on('modal/forgotPassword/show', function(){
        _this.hideAllModals();
        _this.showModalForgotPassword();
    });

    sandbox.on('modal/resetPassword/show', function(){
        _this.hideAllModals();
        _this.showModalResetPassword();
    });

    sandbox.on('modal/signUp/show', function(){
        _this.hideAllModals();
        _this.showModalSignUp();
    });

    _this.init = function(data){
        _this.data = {};
        _this.data.titleTemplate = multiline(function(){/*!@preserve
             <div class="title">{{title}}</div>
             <div class="subtitle">{{subtitle}}</div>
         */console.log});
        _this.showInitModal();
    } 
    _this.destroy = function(){}

    return {
        init: _this.init,
        destroy: _this.destroy
    }
}

module.exports = moduleModal;
},{}],10:[function(require,module,exports){
'use strict';

var moduleWindow = function(sandbox){
    var _this = this;

    _this.init = function(data){
       
        if ($("#page_type").val() == "account_password_reset_from_key") {
            setTimeout(function() {
                sandbox.emit('modal/resetPassword/show');
            },400);
        }
        
        var $body = $('body');

        $body
            .on('click', '[data-target=modal-login]', function(){
                sandbox.emit('modal/login/show');
            })
            // .on('click', '[data-target=modal-register]', function(){
            //     sandbox.emit('modal/register/show');
            // })
            .on('click', '[data-target=modal-forgot-password]', function(){
                sandbox.emit('modal/forgotPassword/show');
            })
            .on('click', '[data-target=modal-sign-up]', function(){
                sandbox.emit('modal/signUp/show');
            })
            .on('click', '.question [data-action="expand"]', function(event){
                event.preventDefault();

                var $this = $(this),
                    $currentQuestion = $this.closest('.question');

                if ($currentQuestion.attr("data-is-expanded") == 0) {
                    if($currentQuestion.length){
                        $currentQuestion.attr('data-is-expanded', 1);
                    }
                } else {
                    $currentQuestion.attr('data-is-expanded', 0);
                }
            })
            .on("click",'.question [data-action="full-content"]',function(event) {
                event.preventDefault();

                var $this = $(this),
                    $currentText = $this.closest('.text');

                $currentText.find(".full-content").removeClass("hide");
                $currentText.find(".brief-content").addClass("hide");

                $this.addClass("hide");
            });
        ;

    }
    _this.destroy = function(){}

    return {
        init: _this.init,
        destroy: _this.destroy
    }
}

module.exports = moduleWindow;
},{}],11:[function(require,module,exports){
'use strict';

var pageContact = function(sandbox){
    var _this = this;

    _this.handleForm = function(){
        var $form = $('#segment_2__form'),
            $listErrors = $form.find('.group.error')
        ;

        $form.on('submit', function(event){
            event.preventDefault();
        });

        $form.validate({
            debug: true,
            rules: {
                "name": "required",
                "email": {
                    required: true,
                    email: true
                },
                "subject": "required",
                "message": {
                    required: true
                }
            },
            messages: {
                name: "Please specify your name",
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                },
                "subject": "Please specify your subject",
                "message": "Please specify your message"
            },
            errorContainer: '#segment_2__form .group.error',
            errorLabelContainer: '#segment_2__form .group.error .list',
            errorElement: 'div',
            errorClass: 'item',
            //errorPlacement: function(error, element){
            //    $listErrors.html('');
            //    $('<div class="item"></div>').html($(error).text()).appendTo($listErrors);
            //    element.addClass('error');
            //},
            submitHandler: function(form) { 
                form.submit();
            }
        });
    }

    _this.init = function(data){
        _this.handleForm();
    }
    _this.destroy = function(){}
}

module.exports = pageContact;
},{}],12:[function(require,module,exports){
'use strict';

var pageDashboard = function(sandbox){
    var _this = this;

    _this.handleTabs = function(){
    	var $menu = $('#segment_2__menu'),
            $tabs = $('#segment_3__segment_questions__tabs')
        ;

        $menu.on('click', '>.item', function(){
            var $this = $(this),
                $target = $($this.data('target'))
            ;

            $this
                .addClass('active')
                .siblings('.active')
                .removeClass('active')
            ;

            if($target.length){
                $target
                    .addClass('active')
                    .siblings('.active')
                    .removeClass('active')
                ;

                var spinner = new Spinner().spin(document.getElementById('loading_icon'));
                var data = {
                    "username": $("#user_login_username").val(),
                    "question_type": $target.data("question-type"),
                    "start_index": 0
                }

                ajaxGet("/ajax/user/load_user_questions",data,function(response) {
                    $target.find(".list.questions").html(response['content']);
                    $target.find(".start-index").val(response['start_index']);
                    $target.find(".all-item-loaded").val(response['is_all_items_loaded']);
                    _this.handleAnswer();
                    spinner.stop();
                })
            }
        });

        $menu.children().first().trigger('click');

        var win = $(window);

        win.scroll(function() {
            if ($(document).height() - win.height() == win.scrollTop()) {

                var $target = $tabs.find(".active");
        
                if ($target.find(".all-item-loaded").val() != "true") {
                    var spinner = new Spinner().spin(document.getElementById('loading_icon'));

                    var data = {
                        "username": $("#user_login_username").val(),
                        "question_type": $target.data("question-type"),
                        "start_index": $target.find(".start-index").val()
                    }

                    ajaxGet("/ajax/user/load_user_questions",data,function(response){
                        spinner.stop();
                        if (parseInt(response['num_item']) > 0) {
                            $target.find(".list.questions").append(response['content']);
                            $target.find(".start-index").val(response['start_index']);
                            $target.find(".all-item-loaded").val(response['is_all_items_loaded']);
                            _this.handleAnswer();
                        }
                    })
                }
            }
        });
    }

    _this.handleAnswer = function(){

        _this.templates.answerItem = multiline(function(){/*
        <div class="univtop card">
            <div class="thumbnail">
                <img src='{{answer.user_post.avatar}}' width='56' height='56'/>
                <a href="#" class="meta name">{{answer.user_post.fullname}}</a>
            </div>
            <div class="content">
                <div class="segment">
                    <div class="header">
                        <div class="meta time">
                            {% if answer.edit_date %}
                                Edited {{answer.edit_date|elapseTime}}
                            {% else %}
                                {{answer.create_date|elapseTime}}
                            {% endif %}
                        </div>
                    </div>
                    <div class="body">
                        <div class="summary">{{answer.content}}</div>
                    </div>
                    <div class="footer">
                        {% if answer.is_user_login_vote %}
                        <a class="univtop label vote" data-action='unvote-answer'>
                        {% else %}
                        <a class="univtop label vote" data-action='vote-answer'>
                        {% endif %}
                            <i class="univtop icon heart" ></i><span class='value'>{{answer.total_vote}}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        */console.log});

        var $tabs = $('#segment_3__segment_questions__tabs');

        $tabs.find(".univtop.form.answer").on("submit",function(event){
            event.preventDefault();
            
            var $this = $(this),
                $content = $this.find("textarea[name='content']").val();

            var user_answer_id = $this.data("answer-id");

            if ($content.length) {
                var spinner = new Spinner().spin(document.getElementById('loading_icon'));
                var question_id = $this.attr("data-question-id");
                var data = {
                    "content": $content
                }

                if (isUndefined(user_answer_id)) {
                    var ajax_data = { 
                        "type": "post",
                        "url": $("#api_url").val() + '/api/v1/question/' + question_id + "/add_answer/",
                        "data": JSON.stringify(data)
                    }
                    ajaxPost("/ajax/api/call",ajax_data,function(data){
                        spinner.stop();
                        if (data['status'] == 'success') {
                            var item_content = swig.render(_this.templates.answerItem, {locals: {
                                    answer: data['response']['answer']
                                }
                            });
                            var html = "<div class='item answer' data-answer-id='" + data['response']['answer_id'] + "'>" + item_content + "</div>"
                            var $answersList = $this.parent().find(".list.answers");
                            $answersList.append(html);
                            $this.attr("data-answer-id",data['response']['answer_id']);
                            
                        }
                    })
                } else {
                    var ajax_data = { 
                        "type": "patch",
                        "url": $("#api_url").val() + '/api/v1/answer/' + user_answer_id + "/",
                        "data": JSON.stringify(data)
                    }
                    ajaxPost("/ajax/api/call",ajax_data,function(data){
                        spinner.stop();
                        if (data['status'] == 'success') {
                            var html = swig.render(_this.templates.answerItem, {locals: {
                                    answer: data['response']
                                }
                            });
                            var $answerItem = $(".answer.item[data-answer-id='" + user_answer_id + "']");
                            $answerItem.html(html);
                            if (!$answerItem.is(":in-viewport")) {
                                $("html, body").animate({
                                    scrollTop: $answerItem.offset().top
                                }, 300);
                            }  
                        }
                    })
                }
            }
        });
    }

    _this.handleVote = function() {
        
        var $tabs = $('#segment_3__segment_questions__tabs');

        $tabs.on("click",".item.question [data-action='vote-question']",function() {
            var $this = $(this);
            
            var $questionItem = $this.closest(".item.question");
            var question_id = $questionItem.data("id");

            var ajax_data = { 
                "type": "post",
                "url": $("#api_url").val() + "/api/v1/question/" + question_id + "/vote/",
                "data": JSON.stringify({})
            }

            var spinner = new Spinner().spin(document.getElementById('loading_icon'));

            ajaxPost("/ajax/api/call",ajax_data,function(data){
                spinner.stop();
                if (data['status'] == 'success') {
                    $this.attr("data-action","unvote-question");
                    var current_vote = parseInt($this.find("span.value").text());
                    $this.find("span.value").text(current_vote+1);
                }
            })
            
        })

        $tabs.on("click",".item.answer [data-action='vote-answer']",function() {
            var $this = $(this);
            
            var $answerItem = $this.closest(".item.answer");
            var answer_id = $answerItem.data("id");

            var ajax_data = { 
                "type": "post",
                "url": $("#api_url").val() + "/api/v1/answer/" + answer_id + "/vote/",
                "data": JSON.stringify({})
            }

            var spinner = new Spinner().spin(document.getElementById('loading_icon'));

            ajaxPost("/ajax/api/call",ajax_data,function(data){
                spinner.stop();
                if (data['status'] == 'success') {
                    $this.attr("data-action","unvote-answer");
                    var current_vote = parseInt($this.find("span.value").text());
                    $this.find("span.value").text(current_vote+1);
                }
            })
            
        })
    }

    _this.handleUnvote = function() {

        var $tabs = $('#segment_3__segment_questions__tabs');

        $tabs.on("click",".item.question [data-action='unvote-question']",function() {
            var $this = $(this);
            
            console.log("unvote");

            var $questionItem = $this.closest(".item.question");
            var question_id = $questionItem.data("id");

            var ajax_data = { 
                "type": "post",
                "url": $("#api_url").val() + "/api/v1/question/" + question_id + "/unvote/",
                "data": JSON.stringify({})
            }

            var spinner = new Spinner().spin(document.getElementById('loading_icon'));

            ajaxPost("/ajax/api/call",ajax_data,function(data){
                spinner.stop();
                if (data['status'] == 'success') {
                    $this.attr("data-action","vote-question");
                    var current_vote = parseInt($this.find("span.value").text());
                    $this.find("span.value").text(current_vote-1);
                }
            })
        });


        $tabs.on("click",".item.answer [data-action='unvote-answer']",function() {
            var $this = $(this);
            
            var $answerItem = $this.closest(".item.answer");
            var answer_id = $answerItem.data("id");

            var ajax_data = { 
                "type": "post",
                "url": $("#api_url").val() + "/api/v1/answer/" + answer_id + "/unvote/",
                "data": JSON.stringify({})
            }

            var spinner = new Spinner().spin(document.getElementById('loading_icon'));

            ajaxPost("/ajax/api/call",ajax_data,function(data){
                spinner.stop();
                if (data['status'] == 'success') {
                    $this.attr("data-action","vote-answer");
                    var current_vote = parseInt($this.find("span.value").text());
                    $this.find("span.value").text(current_vote-1);
                }
            })
            
        })
    }

    _this.handleEditProfile = function(){

    }

    _this.handleFollow = function() {
        var $tabs = $('#segment_3__segment_questions__tabs');

        $tabs.on("click",".item.question [data-action='follow']",function(event) {
            event.preventDefault();
            var $this = $(this);
            
            var $questionItem = $this.closest(".item.question");
            var question_id = $questionItem.data("id");

            var ajax_data = { 
                "type": "post",
                "url": $("#api_url").val() + "/api/v1/user/follow/question/" + question_id + "/",
                "data": JSON.stringify({})
            }

            var spinner = new Spinner().spin(document.getElementById('loading_icon'));

            ajaxPost("/ajax/api/call",ajax_data,function(data){
                spinner.stop();
                if (data['status'] == 'success') {
                    $this.attr("data-action","unfollow");
                    $this.text("FOLLOWED");
                }
                
            })
            
        })
    }

    _this.handleUnfollow = function() {
        var $tabs = $('#segment_3__segment_questions__tabs');

        $tabs.on("click",".item.question [data-action='unfollow']",function(event) {
            event.preventDefault();
            var $this = $(this);
            
            var $questionItem = $this.closest(".item.question");
            var question_id = $questionItem.data("id");

            var ajax_data = { 
                "type": "post",
                "url": $("#api_url").val() + "/api/v1/user/unfollow/question/" + question_id + "/",
                "data": JSON.stringify({})
            }

            var spinner = new Spinner().spin(document.getElementById('loading_icon'));

            ajaxPost("/ajax/api/call",ajax_data,function(data){
                spinner.stop();
                if (data['status'] == 'success') {
                    $this.attr("data-action","follow");
                    $this.text("FOLLOW");
                }
                
            })
            
        })
    }

    _this.init = function(data){
    	_this.data = {};
        _this.templates = {};
        _this.handleTabs();
        _this.handleEditProfile();
        _this.handleAnswer();
        _this.handleVote();
        _this.handleUnvote();
        _this.handleFollow();
        _this.handleUnfollow();
    }

    _this.destroy = function(){}
}

module.exports = pageDashboard;
},{}],13:[function(require,module,exports){
/**
 * @module pageHome
 */
var pageHome = function(sandbox){
    var _this = this;

    /**
     * @module pageHome
     * @function init
     * @param options
     */
    _this.init = function(options){
        _this.objects = {};
        _this.objects.$segment1 = $('#segment_1');
        _this.objects.$segment2 = $('#segment_2');
        _this.objects.$segment1ToggleScrollDown = $('#segment_1__toggle_scroll_down');

        _this.handleAskForm();
        _this.handleSegment1();
    };

    /**
     * @module pageHome
     * @function destroy
     */
    _this.destroy = function(){}

    /**
     * @module pageHome
     * @function handleAskForm
     */
    _this.handleAskForm = function(){
        
    }

    _this.handleSegment1 = function(){
        _this.objects.$segment1.find('[data-toggle=scrollDown]').on('click', function(){
            $("html, body").animate({
                scrollTop: _this.objects.$segment2.offset().top
            }, 300);
        });
    }

    return ({
        init: _this.init,
        destroy: _this.destroy
    })
};

module.exports = pageHome;
},{}],14:[function(require,module,exports){
'use strict';

var pageProfile = function(sandbox){
    var _this = this;

    _this.handleTabs = function(){
    	var $menu = $('#segment_2__menu'),
            $tabs = $('#segment_3__segment_questions__tabs')
        ;

        $menu.on('click', '>.item', function(){
            var $this = $(this),
                $target = $($this.data('target'))
            ;

            $this
                .addClass('active')
                .siblings('.active')
                .removeClass('active')
            ;

            if($target.length){
                $target
                    .addClass('active')
                    .siblings('.active')
                    .removeClass('active')
                ;
            }
        });
    }

    _this.init = function(data){
    	_this.data = {};
        _this.templates = {};
        _this.handleTabs();
    }
    _this.destroy = function(){}
}

module.exports = pageProfile;
},{}],15:[function(require,module,exports){
'use strict';

var pageQuestions = function(sandbox){
    var _this = this;

    _this.handleFormSearch = function(){
        var $input = $('#form_ask_question__input');

        var inputMagicSuggest = $input.magicSuggest({
            data: [
                {
                    name: 'Question 1',
                    href: '/pages/contact-us.html'
                },
                {
                    name: 'Question 2',
                    href: '/pages/question.html'
                }
            ],
            hideTrigger: true,
            selectFirst: true,
            placeholder: '',
            renderer: function(data){
                return '<a href="'+data.href+'">'+data.name+'</a>';
            }
        });

        $(inputMagicSuggest).on('selectionchange', function(){
            var selectedItems = this.getSelection();
            if(selectedItems.length){
                var selectedItem = selectedItems[0];
                if(selectedItem.href){
                    window.location.assign(selectedItem.href);
                }
            }
        });
    }

    _this.handleTabs = function(){
        _this.templates.tab = multiline(function(){/*
         <div class="list questions">
         {% for question in questions %}
            <div class="item question" data-id="{{question.id}}" data-is-expanded="0">
                <div class="univtop card">
                    <div class="thumbnail">
                        <img src="{{question.user_post.avatar}}" alt="" width="72" height="72">
                        <a href="/user/{{question.user_post.username}}" class="meta name">{{question.user_post.first_name}} {{question.user_post.last_name}}</a>
                    </div>
                    <div class="content">
                        <div class="segment">
                            <div class="header">
                                <div class="actions">
                                    <a href="#" data-action="follow" data-question-id="{{question.id}}" data-is-followed="0">Follow</a>
                                </div>
                                <div class="meta time">{{question.create_date}}</div>
                                <div class="tags">
                                {% for tag in question.tags %}
                                    <a class="tag" data-id="{{tag.id}}">{{tag.value}}</a>
                                {% endfor %}
                                </div>
                            </div>

                            <div class="body">
                                <div class="summary">{{question.title}}</div>
                                <div class="extra text">
                                    {{question.content}}
                                    {% if question.latest_answer %}<a href="#" data-action="expand">more</a>{% endif %}
                                </div>
                            </div><!-- end .body -->

                            <div class="footer">
                                <a class="univtop label">
                                    <i class="univtop icon heart"></i>70
                                </a>

                                <a class="univtop label">
                                    <i class="univtop icon chat-bubble"></i>70
                                </a>
                            </div><!-- end .footer -->
                        </div>
                    </div>
                    {% if question.latest_answer %}
                    <div class="answers">
                        <div class="item">
                            <div class="univtop card">
                                <div class="thumbnail">
                                    <img src="{{question.user_post.avatar}}" alt="" width="56" height="56">
                                    <a href="#" class="meta name">{{question.latest_answer.user_post_fullname}}</a>
                                </div>
                                <div class="content">
                                    <div class="segment">
                                        <div class="body">
                                            <div class="summary">{{question.latest_answer.content}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {% endif %}

                    <form class="univtop form" data-form="reply" data-question-id="{{question.id}}">
                        <div class="inline fields">
                            <div class="field avatar">
                                <img src="../assets/images/demo/pages/question/segment-2.avatar-1.png" alt="" width="72" height="72"/>
                                <div class="meta name">YOU</div>
                            </div>
                            <div class="field input">
                                <div class="fields">
                                    <div class="field">
                                        <textarea name="content" placeholder="Add your answer"></textarea>
                                    </div>
                                    <div class="field text-right">
                                        <button type="submit" class="univtop button">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        {% endfor %}
        </div>
        */console.log});

        var $menu = $('#segment_2__menu'),
            $tabs = $('#segment_2__tabs')
        ;

        $menu.on('click', '>.item', function(){
            var $this = $(this),
                $target = $($this.data('target'))
            ;

            $this
                .addClass('active')
                .siblings('.active')
                .removeClass('active')
            ;

            if($target.length){
                $target
                    .addClass('active')
                    .siblings('.active')
                    .removeClass('active')
                ;

                var spinner = new Spinner().spin(document.getElementById('loading_icon'));

                //load tab content
                //get data
                switch($target.data('loading-status')){
                    case 'success':
                        //do nothing
                        break;
                    case 'not-yet':
                    case 'error':
                    default:
                        sandbox.emit('ajax/request', {
                            ajaxOptions: {
                                url: $("#api_url").val() + '/api/v1/question?format=json',
                                data: {limit: 10}
                            },
                            callback: function(data){
                                spinner.stop();
                                if('success' === data.status){
                                    //render
                                    var html = swig.render(_this.templates.tab, {locals: {
                                            questions: data.response.objects
                                        }
                                    });
                                    $target.html(html);

                                    $target.data('loading-status', 'success');
                                } else {
                                    $target.html("There's an error while fetching the content")
                                }
                            }
                        });
                        break;
                }
            }
        });

        $menu.children().first().trigger('click');
    }

    _this.handleAnswer = function(){

        _this.templates.answerItem = multiline(function(){/*
        <div class="item answer" data-answer-id="{{answer_id}}">
            <div class="univtop card">
                <div class="thumbnail">
                    <img src='{{answer.user_post.avatar}}' width='56' height='56'/>
                    <a href="#" class="meta name">{{answer.user_post.fullname}}</a>
                </div>
                <div class="content">
                    <div class="segment">
                        <div class="body">
                            <div class="summary">{{answer.content}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        */console.log});

        var $tabs = $('#segment_3__segment_questions__tabs');

        $tabs.find(".univtop.form.answer").on("submit",function(event){
            event.preventDefault();
            
            var $this = $(this),
                $content = $this.find("textarea[name='content']").val();

            var user_answer_id = $this.data("answer-id");

            if ($content.length) {
                var spinner = new Spinner().spin(document.getElementById('loading_icon'));
                var question_id = $this.attr("data-question-id");
                var data = {
                    "content": $content
                }

                if (isUndefined(user_answer_id)) {
                    var ajax_data = { 
                        "type": "post",
                        "url": $("#api_url").val() + '/api/v1/question/' + question_id + "/add_answer/",
                        "data": JSON.stringify(data)
                    }
                    ajaxPost("/ajax/api/call",ajax_data,function(data){
                        var html = swig.render(_this.templates.answerItem, {locals: {
                                answer: data['response']['answer'],
                                answer_id: data['response']['answer_id']
                            }
                        });
                        var $answersList = $this.parent().find(".list.answers");
                        $answersList.append(html);
                        $this.attr("data-answer-id",data['response']['answer_id']);
                        spinner.stop();
                    })
                } else {
                    var ajax_data = { 
                        "type": "patch",
                        "url": $("#api_url").val() + '/api/v1/answer/' + user_answer_id + "/",
                        "data": JSON.stringify(data)
                    }
                    ajaxPost("/ajax/api/call",ajax_data,function(data){
                        var $answerItem = $(".answer.item[data-answer-id='" + user_answer_id + "']");
                        $answerItem.find(".summary").html(data['response']['content'])
                        if (!$answerItem.is(":in-viewport")) {
                            $("html, body").animate({
                                scrollTop: $answerItem.offset().top
                            }, 300);
                        }  
                        spinner.stop();
                    })
                }
            }
        });
    }

    _this.handleVote = function() {
        var $tabs = $('#segment_3__segment_questions__tabs');

        $tabs.on("click",".univtop.icon.heart",function() {

        })
    }

    _this.init = function(data){
        _this.data = {};
        _this.templates = {};

        _this.handleFormSearch();
        _this.handleTabs();
        _this.handleAnswer();
        _this.handleVote();
    }
    _this.destroy = function(){}

    return {
        init: _this.init,
        destroy: _this.destroy
    }
}

module.exports = pageQuestions;
},{}]},{},[5])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzb3VyY2Uvc2NyaXB0cy9jb3JlL2FwcGxpY2F0aW9uLmpzIiwic291cmNlL3NjcmlwdHMvY29yZS9zYW5kYm94LmpzIiwic291cmNlL3NjcmlwdHMvaW5jbHVkZXMvanF1ZXJ5LmFkci1kcm9wZG93bi5qcyIsInNvdXJjZS9zY3JpcHRzL2luY2x1ZGVzL3N3aWcuanMiLCJzb3VyY2Uvc2NyaXB0cy9pbmRleC5qcyIsInNvdXJjZS9zY3JpcHRzL21vZHVsZXMvYWpheC5qcyIsInNvdXJjZS9zY3JpcHRzL21vZHVsZXMvZm9vdGVyLmpzIiwic291cmNlL3NjcmlwdHMvbW9kdWxlcy9oZWFkZXIuanMiLCJzb3VyY2Uvc2NyaXB0cy9tb2R1bGVzL21vZGFsLmpzIiwic291cmNlL3NjcmlwdHMvbW9kdWxlcy93aW5kb3cuanMiLCJzb3VyY2Uvc2NyaXB0cy9wYWdlcy9jb250YWN0LmpzIiwic291cmNlL3NjcmlwdHMvcGFnZXMvZGFzaGJvYXJkLmpzIiwic291cmNlL3NjcmlwdHMvcGFnZXMvaG9tZS5qcyIsInNvdXJjZS9zY3JpcHRzL3BhZ2VzL3Byb2ZpbGUuanMiLCJzb3VyY2Uvc2NyaXB0cy9wYWdlcy9xdWVzdGlvbnMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNaQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ25CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3ZEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FDTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQzlDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ25FQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzVCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDekRBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoWEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNqREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdkNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJ2YXIgQURSU2FuZGJveCA9IHJlcXVpcmUoJy4vLi4vY29yZS9zYW5kYm94JyksXG4gICAgQURSQXBwbGljYXRpb24gPSBuZXcgc2NhbGVBcHAuQ29yZShBRFJTYW5kYm94KVxuICAgIDtcblxuQURSQXBwbGljYXRpb24udXNlKHNjYWxlQXBwLnBsdWdpbnMubHMpO1xuQURSQXBwbGljYXRpb24udXNlKHNjYWxlQXBwLnBsdWdpbnMudXRpbCk7XG5BRFJBcHBsaWNhdGlvbi51c2Uoc2NhbGVBcHAucGx1Z2lucy5zdWJtb2R1bGUsIHtcbiAgICBpbmhlcml0OiB0cnVlLCAgICAgICAgICAgICAvLyB1c2UgYWxsIHBsdWdpbnMgZnJvbSB0aGUgcGFyZW50J3MgQ29yZVxuICAgIHVzZTogWydscycsJ3N1Ym1vZHVsZScsICd1dGlsJ10sICAgICAgICAvLyB1c2Ugc29tZSBhZGRpdGlvbmFsIHBsdWdpbnNcbiAgICB1c2VHbG9iYWxNZWRpYXRvcjogdHJ1ZSwgICAvLyBlbWl0IGFuZCByZWNlaXZlIGFsbCBldmVudHMgZnJvbSB0aGUgcGFyZW50J3MgQ29yZVxufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gQURSQXBwbGljYXRpb247IiwidmFyIEFEUlNhbmRib3ggPSBmdW5jdGlvbihjb3JlLCBpbnN0YW5jZUlkLCBvcHRpb25zLCBtb2R1bGVJZCkge1xuXG4gICAgLy8gZGVmaW5lIHlvdXIgQVBJXG4gICAgdGhpcy5uYW1lc3BhY2UgPSBcIkFEUlwiO1xuXG4gICAgLy8gZS5nLiBwcm92aWRlIHRoZSBNZWRpYXRvciBtZXRob2RzICdvbicsICdlbWl0JywgZXRjLlxuICAgIGNvcmUuX21lZGlhdG9yLmluc3RhbGxUbyh0aGlzKTtcblxuICAgIC8vIC4uLiBvciBkZWZpbmUgeW91ciBjdXN0b20gY29tbXVuaWNhdGlvbiBtZXRob2RzXG4gICAgdGhpcy5teUVtaXQgPSBmdW5jdGlvbihjaGFubmVsLCBkYXRhKXtcbiAgICAgICAgY29yZS5lbWl0KGNoYW5uZWwgKyAnLycgKyBpbnN0YW5jZUlkLCBkYXRhKTtcbiAgICB9O1xuXG4gICAgLy8gbWF5YmUgeW91J2QgbGlrZSB0byBleHBvc2UgdGhlIGluc3RhbmNlIElEXG4gICAgdGhpcy5pZCA9IGluc3RhbmNlSWQ7XG5cbiAgICByZXR1cm4gdGhpcztcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gQURSU2FuZGJveDsiLCIndXNlIHN0cmljdCc7XG5cbihmdW5jdGlvbiAoJCkge1xuICAgIC8qKlxuICAgICAqIGpRdWVyeS5BRFJEcm9wZG93blxuICAgICAqIEBwYXJhbSBvcHRpb25zXG4gICAgICogQHJldHVybnMgeyQuZm4uQURSRHJvcGRvd259XG4gICAgICogQGNvbnN0cnVjdG9yXG4gICAgICovXG4gICAgJC5mbi5BRFJEcm9wZG93biA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gICAgICAgIHZhciBkZWZhdWx0U2V0dGluZ3MgPSB7XG4gICAgICAgICAgICB0ZXh0UmVwbGFjZTogZmFsc2VcbiAgICAgICAgfTtcblxuICAgICAgICB2YXIgc2V0dGluZ3MgPSAkLmV4dGVuZCh7fSxkZWZhdWx0U2V0dGluZ3MsIG9wdGlvbnMpO1xuXG4gICAgICAgIHRoaXMuZWFjaChmdW5jdGlvbigpe1xuICAgICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKSxcbiAgICAgICAgICAgICAgICAkd2luZG93ID0gJCh3aW5kb3cpXG4gICAgICAgICAgICA7XG5cbiAgICAgICAgICAgICR0aGlzLm9uKCdjbGljaycsZnVuY3Rpb24oZXZlbnQpe1xuICAgICAgICAgICAgICAgICR0aGlzLnRvZ2dsZUNsYXNzKCdvcGVuJyk7XG5cbiAgICAgICAgICAgICAgICB2YXIgJHRhcmdldCA9ICQoZXZlbnQudGFyZ2V0KTtcbiAgICAgICAgICAgICAgICBpZigkLmNvbnRhaW5zKCR0aGlzWzBdLGV2ZW50LnRhcmdldCkpe1xuICAgICAgICAgICAgICAgICAgICB2YXIgJHRleHQgPSAkdGhpcy5jaGlsZHJlbignLnRleHQ6Zmlyc3QnKTtcbiAgICAgICAgICAgICAgICAgICAgLy9jaGVjayBpZiBpdCBpcyBhIGJ1dHRvblxuICAgICAgICAgICAgICAgICAgICB2YXIgJHRleHRCdXR0b24gPSAkdGV4dC5jaGlsZHJlbignLmJ1dHRvbjpmaXJzdCcpO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmKHNldHRpbmdzLnRleHRSZXBsYWNlKXtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKCR0ZXh0QnV0dG9uLmxlbmd0aCA+IDApe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0ZXh0QnV0dG9uLmh0bWwoJHRhcmdldC50ZXh0KCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkdGV4dC5odG1sKCR0YXJnZXQudGV4dCgpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICR0aGlzLnJlbW92ZUNsYXNzKCdvcGVuJyk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYoJHRoaXMuaGFzQ2xhc3MoJ29wZW4nKSl7XG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICR3aW5kb3cub25lKCdjbGljaycsZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkdGhpcy5yZW1vdmVDbGFzcygnb3BlbicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG59KGpRdWVyeSkpO1xuIiwic3dpZy5zZXRGaWx0ZXIoJ2NhbGVuZGFyVGltZScsIGZ1bmN0aW9uIChpbnB1dCkge1xuICAgIHJldHVybiBtb21lbnQoaW5wdXQpLmNhbGVuZGFyKCk7XG59KTtcblxuc3dpZy5zZXRGaWx0ZXIoJ2VsYXBzZVRpbWUnLCBmdW5jdGlvbiAoaW5wdXQpIHtcblx0cmV0dXJuIG1vbWVudChpbnB1dCkuZnJvbU5vdygpO1xufSk7IiwiLyoqXG4gKiBTb21lIGluLWhvdXNlIHBsdWdpbnMvbGlicmFyaWVzXG4gKi9cbnJlcXVpcmUoJy4vaW5jbHVkZXMvanF1ZXJ5LmFkci1kcm9wZG93bicpO1xucmVxdWlyZSgnLi9pbmNsdWRlcy9zd2lnJyk7XG5cbmdsb2JhbC5OQU1FU1BBQ0UgPSAndW5pdnRvcCc7XG5cbi8vdGVtcG9yYXJpbHkgZXhwb3NlIEFwcGxpY2F0aW9uIHRvIGdsb2JhbCBmb3IgZGVidWdnaW5nIHB1cnBvc2VcbnZhciBBcHBsaWNhdGlvbiA9IHJlcXVpcmUoJy4vY29yZS9hcHBsaWNhdGlvbicpO1xuXG4vKipcbiAqIHJlZ2lzdGVyIG1vZHVsZXNcbiAqL1xuQXBwbGljYXRpb24ucmVnaXN0ZXIoJ2FqYXgnLCByZXF1aXJlKCcuL21vZHVsZXMvYWpheCcpKTtcblxuaWYgKCQoJy4nICsgTkFNRVNQQUNFICsnLmhlYWRlcicpLmxlbmd0aCkge1xuICAgIEFwcGxpY2F0aW9uLnJlZ2lzdGVyKCdoZWFkZXInLCByZXF1aXJlKCcuL21vZHVsZXMvaGVhZGVyJykpO1xufVxuXG5BcHBsaWNhdGlvbi5yZWdpc3RlcignZm9vdGVyJywgcmVxdWlyZSgnLi9tb2R1bGVzL2Zvb3RlcicpKTtcbkFwcGxpY2F0aW9uLnJlZ2lzdGVyKCdtb2RhbCcsIHJlcXVpcmUoJy4vbW9kdWxlcy9tb2RhbCcpKTtcbkFwcGxpY2F0aW9uLnJlZ2lzdGVyKCd3aW5kb3cnLCByZXF1aXJlKCcuL21vZHVsZXMvd2luZG93JykpO1xuXG5pZiAoJCgnLnBhZ2UtaG9tZScpLmxlbmd0aCkge1xuICAgIEFwcGxpY2F0aW9uLnJlZ2lzdGVyKCdwYWdlSG9tZScscmVxdWlyZSgnLi9wYWdlcy9ob21lJykpO1xufVxuXG5pZigkKCcucGFnZS1jb250YWN0JykubGVuZ3RoKXtcbiAgICBBcHBsaWNhdGlvbi5yZWdpc3RlcigncGFnZUNvbnRhY3QnLCByZXF1aXJlKCcuL3BhZ2VzL2NvbnRhY3QnKSk7XG59XG5cbmlmKCQoJy5wYWdlLXF1ZXN0aW9ucycpLmxlbmd0aCl7XG4gICAgQXBwbGljYXRpb24ucmVnaXN0ZXIoJ3BhZ2VRdWVzdGlvbnMnLCByZXF1aXJlKCcuL3BhZ2VzL3F1ZXN0aW9ucycpKTtcbn1cblxuaWYoJCgnLnBhZ2UtcHJvZmlsZScpLmxlbmd0aCl7XG4gICAgQXBwbGljYXRpb24ucmVnaXN0ZXIoJ3BhZ2VQcm9maWxlJywgcmVxdWlyZSgnLi9wYWdlcy9wcm9maWxlJykpO1xufVxuXG5pZigkKCcucGFnZS1kYXNoYm9hcmQnKS5sZW5ndGgpe1xuICAgIEFwcGxpY2F0aW9uLnJlZ2lzdGVyKCdwYWdlRGFzaGJvYXJkJywgcmVxdWlyZSgnLi9wYWdlcy9kYXNoYm9hcmQnKSk7XG59XG5cbkFwcGxpY2F0aW9uLnN0YXJ0KCk7XG5cbndpbmRvdy5Vbml2dG9wQXBwbGljYXRpb24gPSBBcHBsaWNhdGlvbjsiLCJ2YXIgYWpheCA9IGZ1bmN0aW9uIChzYW5kYm94KSB7XG4gICAgdmFyIF90aGlzID0gdGhpcztcblxuICAgIF90aGlzLmluaXQgPSBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICBfdGhpcy5kYXRhID0gZGF0YTtcbiAgICB9O1xuXG4gICAgX3RoaXMuZGVzdHJveSA9IGZ1bmN0aW9uICgpIHsgfTtcblxuICAgIC8qKlxuICAgICAqIEBtb2R1bGUgaGFuZGxlQWpheENhbGxcbiAgICAgKiBAZGVzY3JpcHRpb24gaGFuZGxlIGFsbCBhamF4IGNhbGxzXG4gICAgICovXG4gICAgX3RoaXMuaGFuZGxlQWpheENhbGwgPSBmdW5jdGlvbiAob3B0aW9ucykge1xuXG4gICAgICAgIHZhciBkZWZhdWx0T3B0aW9ucyA9IHtcbiAgICAgICAgICAgIGFqYXhPcHRpb25zOiB7XG4gICAgICAgICAgICAgICAgdHlwZTogJ0dFVCcsXG4gICAgICAgICAgICAgICAgLy9oZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgLy8gICAgJ1JlcXVlc3RWZXJpZmljYXRpb25Ub2tlbic6IGdsb2JhbERhdGEudG9rZW5IZWFkZXJWYWx1ZVxuICAgICAgICAgICAgICAgIC8vfSxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBjaGFubmVsczoge1xuICAgICAgICAgICAgICAgIHJlcXVlc3Q6ICdhamF4L3JlcXVlc3QnLFxuICAgICAgICAgICAgICAgIHJlc3BvbnNlOiAnYWpheC9yZXNwb25zZSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICBvcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRPcHRpb25zLCBvcHRpb25zKTtcblxuICAgICAgICAkXG4gICAgICAgICAgICAuYWpheChvcHRpb25zLmFqYXhPcHRpb25zKVxuICAgICAgICAgICAgLmFsd2F5cyhmdW5jdGlvbihyZXNwb25zZSwgc3RhdHVzLCBqcVhIUil7XG4gICAgICAgICAgICAgICAgdmFyIGRhdGEgPSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlOiByZXNwb25zZSxcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiBzdGF0dXMsXG4gICAgICAgICAgICAgICAgICAgIGFqYXhPYmplY3Q6IGpxWEhSXG4gICAgICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIC8vcnVuIGNhbGxiYWNrXG4gICAgICAgICAgICAgICAgaWYoJ2Z1bmN0aW9uJyA9PT0gdHlwZW9mIG9wdGlvbnMuY2FsbGJhY2spe1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gb3B0aW9ucy5jYWxsYmFjayhkYXRhKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAvL3Jlc3BvbnNlIGNoYW5uZWxcbiAgICAgICAgICAgICAgICBpZihvcHRpb25zLmNoYW5uZWxzICYmICdzdHJpbmcnID09PSB0eXBlb2Ygb3B0aW9ucy5jaGFubmVscy5yZXNwb25zZSl7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzYW5kYm94LmVtaXQob3B0aW9ucy5jaGFubmVscy5yZXNwb25zZSwgZGF0YSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiByZWdpc3RlciBjaGFubmVsc1xuICAgICAqL1xuICAgIHNhbmRib3gub24oJ2FqYXgvcmVxdWVzdCcsIGZ1bmN0aW9uIChvcHRpb25zKSB7XG5cbiAgICAgICAgX3RoaXMuaGFuZGxlQWpheENhbGwob3B0aW9ucyk7XG4gICAgfSk7XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBpbml0OiBfdGhpcy5pbml0LFxuICAgICAgICBkZXN0cm95OiBfdGhpcy5kZXN0cm95XG4gICAgfVxufTtcblxubW9kdWxlLmV4cG9ydHMgPSBhamF4OyIsIi8qKlxuICogIHNjcmlwdHMgZm9yIGZvb3RlclxuICogQHRvZG8gaW5pdEZvcm1OZXdzbGV0dGVyXG4gKiBAdG9kbyBoYW5kbGVJY29uU2Nyb2xsVG9Ub3BcbiAqL1xudmFyIGZvb3RlciA9IGZ1bmN0aW9uIChzYW5kYm94KSB7XG4gICAgdmFyIF90aGlzID0gdGhpcztcblxuICAgIC8qKlxuICAgICAqIEBtb2R1bGUgZm9vdGVyL2luaXRcbiAgICAgKiBAcGFyYW0gb3B0aW9uc1xuICAgICAqL1xuICAgIF90aGlzLmluaXQgPSBmdW5jdGlvbihvcHRpb25zKXtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQG1vZHVsZSBmb290ZXIvZGVzdHJveVxuICAgICAqL1xuICAgIF90aGlzLmRlc3Ryb3kgPSBmdW5jdGlvbigpe1xuXG4gICAgfTtcblxuICAgIHJldHVybiB7XG4gICAgICAgIGluaXQ6IF90aGlzLmluaXQsXG4gICAgICAgIGRlc3Ryb3k6IF90aGlzLmRlc3Ryb3lcbiAgICB9O1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBmb290ZXI7IiwiLyoqXG4gKiBAc2VjdGlvbiBoZWFkZXJcbiAqL1xuLyoqXG4gKiAgc2NyaXB0cyBmb3IgaGVhZGVyXG4gKiBAdG9kbyBpbml0SGVhZGVyU2VnbWVudENob29zZUxvY2F0aW9uXG4gKiBAdG9kbyBpbml0UG9wdXBPcmRlclRyYWNraW5nXG4gKiBAdG9kbyBpbml0UG9wdXBSZWdpc3RlckxvZ2luXG4gKiBAdG9kbyBpbml0UG9wdXBDYXJ0XG4gKiBAdG9kbyBpbml0SGVhZGVyU2VnbWVudFVzZXJJbmZvIChmb3IgYWxyZWFkeSBsb2dnZWQgdXNlcilcbiAqIEBkb25lIGluaXRTdGlja3lIZWFkZXJcbiAqL1xudmFyIGhlYWRlciA9IGZ1bmN0aW9uIChzYW5kYm94KSB7XG4gICAgdmFyIF90aGlzID0gdGhpcztcblxuICAgIF90aGlzLmluaXRUb2dnbGUgPSBmdW5jdGlvbigpe1xuICAgICAgICBfdGhpcy5vYmplY3RzLiRoZWFkZXJUb2dnbGUub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIF90aGlzLm9iamVjdHMuJGJvZHkudG9nZ2xlQ2xhc3MoJ2lzLW1lbnUtZXhwYW5kZWQnKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQG1vZHVsZSBoZWFkZXJcbiAgICAgKiBAZnVuY3Rpb24gaW5pdFxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gICAgICovXG4gICAgX3RoaXMuaW5pdCA9IGZ1bmN0aW9uKGRhdGEpe1xuXG4gICAgICAgIF90aGlzLmRhdGEgPSBkYXRhIHx8IHt9O1xuXG4gICAgICAgIF90aGlzLkRPTVNlbGVjdG9ycyA9IHt9O1xuICAgICAgICBfdGhpcy5ET01TZWxlY3RvcnMuaGVhZGVyVG9nZ2xlID0gJyNoZWFkZXJfX3RvZ2dsZSc7XG4gICAgICAgIF90aGlzLkRPTVNlbGVjdG9ycy5ib2R5ID0gJ2JvZHknO1xuXG4gICAgICAgIF90aGlzLm9iamVjdHMgPSB7fTtcbiAgICAgICAgX3RoaXMub2JqZWN0cy4kaGVhZGVyVG9nZ2xlID0gJChfdGhpcy5ET01TZWxlY3RvcnMuaGVhZGVyVG9nZ2xlKTtcbiAgICAgICAgX3RoaXMub2JqZWN0cy4kYm9keSA9ICQoX3RoaXMuRE9NU2VsZWN0b3JzLmJvZHkpO1xuXG4gICAgICAgIF90aGlzLmluaXRUb2dnbGUoKTtcbiAgICB9O1xuXG4gICAgX3RoaXMuZGVzdHJveSA9IGZ1bmN0aW9uKCl7fTtcblxuXG4gICAgcmV0dXJuIHtcbiAgICAgICAgaW5pdDogX3RoaXMuaW5pdCxcbiAgICAgICAgZGVzdHJveTogX3RoaXMuZGVzdHJveVxuICAgIH07XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGhlYWRlcjsiLCIndXNlIHN0cmljdCc7XG5cbnZhciBtb2R1bGVNb2RhbCA9IGZ1bmN0aW9uKHNhbmRib3gpe1xuICAgIHZhciBfdGhpcyA9IHRoaXM7XG5cbiAgICBfdGhpcy5oaWRlQWxsTW9kYWxzID0gZnVuY3Rpb24oKXtcbiAgICAgICAgYm9vdGJveC5oaWRlQWxsKCk7XG4gICAgfVxuXG4gICAgX3RoaXMuc2hvd01vZGFsTG9naW4gPSBmdW5jdGlvbigpe1xuICAgICAgICB2YXIgdGl0bGUgPSBzd2lnLnJlbmRlcihfdGhpcy5kYXRhLnRpdGxlVGVtcGxhdGUsIHtsb2NhbHM6IHtcbiAgICAgICAgICAgICAgICB0aXRsZTogJ0xvZyBpbicsXG4gICAgICAgICAgICAgICAgc3VidGl0bGU6ICdXZWxjb21lIGJhY2shJyxcbiAgICAgICAgICAgIH19KSxcbiAgICAgICAgICAgIG1lc3NhZ2VUZW1wbGF0ZSA9IG11bHRpbGluZShmdW5jdGlvbigpey8qIUBwcmVzZXJ2ZVxuICAgICAgICAgICAgPGZvcm0gY2xhc3M9XCJ1bml2dG9wIGZvcm1cIiBpZD1cIm1vZGFsX2xvZ2luX19mb3JtXCIgYWN0aW9uPScvYWNjb3VudC9sb2dpbi8nIG1ldGhvZD0nUE9TVCc+XG4gICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJoaWRkZW5cIiBuYW1lPVwiY3NyZm1pZGRsZXdhcmV0b2tlblwiIHZhbHVlPVwiXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz0nbWVzc2FnZScgc3R5bGU9J2Rpc3BsYXk6bm9uZSc+PC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkc1wiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJtb2RhbF9sb2dpbl9fZm9ybV9faW5wdXRfZW1haWxcIj5FbWFpbDwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBpZD1cIm1vZGFsX2xvZ2luX19mb3JtX19pbnB1dF9lbWFpbFwiIG5hbWU9XCJlbWFpbFwiLz5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1vZGFsX2xvZ2luX19mb3JtX19pbnB1dF9wYXNzd29yZFwiPlBhc3N3b3JkPC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicGFzc3dvcmRcIiBpZD1cIm1vZGFsX2xvZ2luX19mb3JtX19pbnB1dF9wYXNzd29yZFwiIG5hbWU9XCJwYXNzd29yZFwiLz5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0ZXh0LXJpZ2h0XCI+PGEgZGF0YS10YXJnZXQ9XCJtb2RhbC1mb3Jnb3QtcGFzc3dvcmRcIj5Gb3Jnb3QgeW91ciBwYXNzd29yZD88L2E+PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZHNcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImVycm9yIGZpZWxkXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibGlzdFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgaWQ9J21vZGFsX2xvZ2luX19mb3JtX19pbnB1dF9zdWJtaXQtZXJyb3InIGNsYXNzPVwiaXRlbVwiIHN0eWxlPSdkaXNwbGF5Om5vbmUnPjwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbmxpbmUgZmllbGRzXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cInVuaXZ0b3AgYnV0dG9uXCIgdHlwZT1cInN1Ym1pdFwiPkxvZyBJbiE8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgRG9uJ3QgaGF2ZSBhbiBhY2NvdW50Pzxici8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgZGF0YS10YXJnZXQ9XCJtb2RhbC1zaWduLXVwXCI+U2lnbiBVcDwvYT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Zvcm0+XG4gICAgICAgICAgICA8ZGl2IGlkPSdsb2dpbl9sb2FkaW5nX2ljb24nPjwvZGl2PlxuICAgICAgICAgICAgKi9jb25zb2xlLmxvZ30pLFxuICAgICAgICAgICAgbWVzc2FnZSA9IHN3aWcucmVuZGVyKG1lc3NhZ2VUZW1wbGF0ZSk7XG4gICAgICAgIDtcblxuICAgICAgICB2YXIgJGRpYWxvZyA9IGJvb3Rib3guZGlhbG9nKHtcbiAgICAgICAgICAgIGNsYXNzTmFtZTogJ21vZGFsLWxvZ2luJyxcbiAgICAgICAgICAgIHRpdGxlOiB0aXRsZSxcbiAgICAgICAgICAgIG1lc3NhZ2U6IG1lc3NhZ2UsXG4gICAgICAgICAgICBvbkVzY2FwZTogdHJ1ZSxcbiAgICAgICAgICAgIGJhY2tkcm9wOiAnc3RhdGljJ1xuICAgICAgICB9KTtcblxuICAgICAgICAkZGlhbG9nLm9uKCdzaG93bi5icy5tb2RhbCcsIGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICB2YXIgJGZvcm0gPSAkZGlhbG9nLmZpbmQoJyNtb2RhbF9sb2dpbl9fZm9ybScpO1xuICAgICAgICAgICAgdmFyIGNzcmZ0b2tlbiA9IENvb2tpZXMuZ2V0KCdjc3JmdG9rZW4nKTtcbiAgICAgICAgICAgICQoXCIjbW9kYWxfbG9naW5fX2Zvcm0gaW5wdXRbbmFtZT0nY3NyZm1pZGRsZXdhcmV0b2tlbiddXCIpLnZhbChjc3JmdG9rZW4pO1xuXG4gICAgICAgICAgICAkZm9ybS5vbignc3VibWl0JywgZnVuY3Rpb24oZXZlbnQpe1xuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgJGZvcm0udmFsaWRhdGUoe1xuICAgICAgICAgICAgICAgIHJ1bGVzOiB7XG4gICAgICAgICAgICAgICAgICAgIGVtYWlsOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGVtYWlsOiB0cnVlXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIHBhc3N3b3JkOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbmxlbmd0aDogNlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBtZXNzYWdlczoge1xuICAgICAgICAgICAgICAgICAgICBlbWFpbDoge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IFwiUGxlYXNlIGlucHV0IHlvdXIgZW1haWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIGVtYWlsOiBcIlBsZWFzZSBlbnRlciBjb3JyZWN0IGVtYWlsXCJcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgcGFzc3dvcmQ6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBcIlBsZWFzZSBpbnB1dCB5b3VyIHBhc3N3b3JkXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5sZW5ndGg6IFwiUGFzc3dvcmQgaGFzIHRvIGJlIGF0IGxlYXN0IDYgY2hhcmFjdGVyc1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGVycm9yQ29udGFpbmVyOiAnI21vZGFsX2xvZ2luX19mb3JtIC5maWVsZHMgLmZpZWxkLmVycm9yJyxcbiAgICAgICAgICAgICAgICBlcnJvckxhYmVsQ29udGFpbmVyOiAnI21vZGFsX2xvZ2luX19mb3JtIC5maWVsZHMgLmZpZWxkLmVycm9yIC5saXN0JyxcbiAgICAgICAgICAgICAgICBlcnJvckVsZW1lbnQ6ICdkaXYnLFxuICAgICAgICAgICAgICAgIGVycm9yQ2xhc3M6ICdpdGVtJyxcblxuICAgICAgICAgICAgICAgIHN1Ym1pdEhhbmRsZXI6IGZ1bmN0aW9uKGZvcm0pIHsgXG4gICAgICAgICAgICAgICAgICAgIHZhciBzcGlubmVyID0gbmV3IFNwaW5uZXIoKS5zcGluKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdsb2dpbl9sb2FkaW5nX2ljb24nKSk7XG4gICAgICAgICAgICAgICAgICAgICQoXCIjbW9kYWxfbG9naW5fX2Zvcm1fX2lucHV0X3N1Ym1pdC1lcnJvclwiKS5odG1sKFwiXCIpO1xuICAgICAgICAgICAgICAgICAgICBzYW5kYm94LmVtaXQoJ2FqYXgvcmVxdWVzdCcsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGFqYXhPcHRpb25zOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdXJsOiAkKFwiI2FwaV91cmxcIikudmFsKCkgKyBcIi9hcGkvdjEvYXV0aC9sb2dpbi9cIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnUE9TVCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImVtYWlsXCI6ICQoXCIjbW9kYWxfbG9naW5fX2Zvcm0gaW5wdXRbbmFtZT0nZW1haWwnXVwiKS52YWwoKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJwYXNzd29yZFwiOiAkKFwiI21vZGFsX2xvZ2luX19mb3JtIGlucHV0W25hbWU9J3Bhc3N3b3JkJ11cIikudmFsKClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uKGRhdGEpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLnN0YXR1cyA9PSAnc3VjY2VzcycpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybS5zdWJtaXQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzcGlubmVyLnN0b3AoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNtb2RhbF9sb2dpbl9fZm9ybSAuZmllbGRzIC5lcnJvclwiKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjbW9kYWxfbG9naW5fX2Zvcm0gLmZpZWxkcyAuZXJyb3IgLmxpc3RcIikuc2hvdygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiI21vZGFsX2xvZ2luX19mb3JtIC5maWVsZHMgLmVycm9yIC5saXN0IC5pdGVtXCIpLmhpZGUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEucmVzcG9uc2Uuc3RhdHVzID09IDQwMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJpbnNpZGVcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiI21vZGFsX2xvZ2luX19mb3JtX19pbnB1dF9zdWJtaXQtZXJyb3JcIikuaHRtbChcIlBhc3N3b3JkIGlzIGluY29ycmVjdC4gUGxlYXNlIHRyeSBhZ2FpblwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjbW9kYWxfbG9naW5fX2Zvcm1fX2lucHV0X3N1Ym1pdC1lcnJvclwiKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiI21vZGFsX2xvZ2luX19mb3JtX19pbnB1dF9zdWJtaXQtZXJyb3JcIikuaHRtbChcIlVuYWJsZSB0byBwcm9jZXNzIHlvdXIgcmVxdWVzdC4gUGxlYXNlIHRyeSBhZ2FpblwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjbW9kYWxfbG9naW5fX2Zvcm1fX2lucHV0X3N1Ym1pdC1lcnJvclwiKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgX3RoaXMuc2hvd01vZGFsRm9yZ290UGFzc3dvcmQgPSBmdW5jdGlvbigpe1xuICAgICAgICB2YXIgdGl0bGUgPSBzd2lnLnJlbmRlcihfdGhpcy5kYXRhLnRpdGxlVGVtcGxhdGUsIHtsb2NhbHM6IHtcbiAgICAgICAgICAgICAgICB0aXRsZTogJ0ZvcmdvdCB5b3VyIHBhc3N3b3JkPydcbiAgICAgICAgICAgIH19KSxcbiAgICAgICAgICAgIG1lc3NhZ2VUZW1wbGF0ZSA9IG11bHRpbGluZShmdW5jdGlvbigpey8qIUBwcmVzZXJ2ZVxuICAgICAgICAgICAgPGZvcm0gY2xhc3M9XCJ1bml2dG9wIGZvcm1cIiBpZD1cIm1vZGFsX2ZvcmdvdF9wYXNzd29yZF9fZm9ybVwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9J21lc3NhZ2UnIHN0eWxlPSdkaXNwbGF5Om5vbmUnPlxuICAgICAgICAgICAgICAgICAgICBXZSBoYXZlIHNlbnQgcmVzZXQgcGFzc3dvcmQgbGluayB0byB5b3VyIGVtYWlsLiBQbGVhc2UgZm9sbG93IHRoZSBpbnN0cnVjdGlvbiB0byByZXNldCB5b3VyIHBhc3N3b3JkXG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkc1wiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJtb2RhbF9mb3Jnb3RfcGFzc3dvcmRfX2Zvcm1fX2lucHV0X2VtYWlsXCI+VHlwZSB5b3VyIGVtYWlsIGhlcmU8L2xhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgaWQ9XCJtb2RhbF9mb3Jnb3RfcGFzc3dvcmRfX2Zvcm1fX2lucHV0X2VtYWlsXCIgbmFtZT1cImVtYWlsXCIvPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRzXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJlcnJvciBmaWVsZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxpc3RcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGlkPSdtb2RhbF9mb3Jnb3RfcGFzc3dvcmRfX2Zvcm1fX2lucHV0X3N1Ym1pdC1lcnJvcicgY2xhc3M9XCJpdGVtXCIgc3R5bGU9J2Rpc3BsYXk6bm9uZSc+PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkc1wiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGQgdGV4dC1jZW50ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJ1bml2dG9wIGJ1dHRvblwiIHR5cGU9XCJzdWJtaXRcIj5SZXNldCBQYXNzd29yZDwvYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZm9ybT5cbiAgICAgICAgICAgIDxkaXYgaWQ9J2ZvcmdvdF9wYXNzd29yZF9sb2FkaW5nX2ljb24nPjwvZGl2PlxuICAgICAgICAgICAgKi9jb25zb2xlLmxvZ30pLFxuICAgICAgICAgICAgbWVzc2FnZSA9IHN3aWcucmVuZGVyKG1lc3NhZ2VUZW1wbGF0ZSk7XG4gICAgICAgIDtcblxuICAgICAgICB2YXIgJGRpYWxvZyA9IGJvb3Rib3guZGlhbG9nKHtcbiAgICAgICAgICAgIGNsYXNzTmFtZTogJ21vZGFsLWZvcmdvdC1wYXNzd29yZCcsXG4gICAgICAgICAgICB0aXRsZTogdGl0bGUsXG4gICAgICAgICAgICBtZXNzYWdlOiBtZXNzYWdlLFxuICAgICAgICAgICAgb25Fc2NhcGU6IHRydWUsXG4gICAgICAgICAgICBiYWNrZHJvcDogJ3N0YXRpYydcbiAgICAgICAgfSk7XG5cbiAgICAgICAgJGRpYWxvZy5vbignc2hvd24uYnMubW9kYWwnLCBmdW5jdGlvbigpe1xuICAgICAgICAgICAgdmFyICRmb3JtID0gJGRpYWxvZy5maW5kKCcjbW9kYWxfZm9yZ290X3Bhc3N3b3JkX19mb3JtJyk7XG5cbiAgICAgICAgICAgICRmb3JtLm9uKCdzdWJtaXQnLCBmdW5jdGlvbihldmVudCl7XG4gICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAkZm9ybS52YWxpZGF0ZSh7XG4gICAgICAgICAgICAgICAgb25jbGljazogZmFsc2UsXG4gICAgICAgICAgICAgICAgb25mb2N1c291dDogZmFsc2UsXG4gICAgICAgICAgICAgICAgb25rZXl1cDogZmFsc2UsXG4gICAgICAgICAgICAgICAgcnVsZXM6IHtcbiAgICAgICAgICAgICAgICAgICAgZW1haWw6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgZW1haWw6IHRydWVcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgbWVzc2FnZXM6IHtcbiAgICAgICAgICAgICAgICAgICAgZW1haWw6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBcIlBsZWFzZSBpbnB1dCB5b3VyIGVtYWlsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBlbWFpbDogXCJQbGVhc2UgZW50ZXIgY29ycmVjdCBlbWFpbFwiXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBlcnJvckNvbnRhaW5lcjogJyNtb2RhbF9mb3Jnb3RfcGFzc3dvcmRfX2Zvcm0gLmZpZWxkcyAuZmllbGQuZXJyb3InLFxuICAgICAgICAgICAgICAgIGVycm9yTGFiZWxDb250YWluZXI6ICcjbW9kYWxfZm9yZ290X3Bhc3N3b3JkX19mb3JtIC5maWVsZHMgLmZpZWxkLmVycm9yIC5saXN0JyxcbiAgICAgICAgICAgICAgICBlcnJvckVsZW1lbnQ6ICdkaXYnLFxuICAgICAgICAgICAgICAgIGVycm9yQ2xhc3M6ICdpdGVtJyxcblxuICAgICAgICAgICAgICAgIHN1Ym1pdEhhbmRsZXI6IGZ1bmN0aW9uKGZvcm0pIHsgXG4gICAgICAgICAgICAgICAgICAgIHZhciBzcGlubmVyID0gbmV3IFNwaW5uZXIoKS5zcGluKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdmb3Jnb3RfcGFzc3dvcmRfbG9hZGluZ19pY29uJykpO1xuICAgICAgICAgICAgICAgICAgICAkKFwiI21vZGFsX2ZvcmdvdF9wYXNzd29yZF9fZm9ybV9faW5wdXRfc3VibWl0LWVycm9yXCIpLmh0bWwoXCJcIik7XG4gICAgICAgICAgICAgICAgICAgIHNhbmRib3guZW1pdCgnYWpheC9yZXF1ZXN0Jywge1xuICAgICAgICAgICAgICAgICAgICAgICAgYWpheE9wdGlvbnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmw6ICQoXCIjYXBpX3VybFwiKS52YWwoKSArIFwiL2FwaS92MS9hdXRoL3Bhc3N3b3JkX3Jlc2V0XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ1BPU1QnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJlbWFpbFwiOiAkKFwiI21vZGFsX2ZvcmdvdF9wYXNzd29yZF9fZm9ybSBpbnB1dFtuYW1lPSdlbWFpbCddXCIpLnZhbCgpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbihkYXRhKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzcGlubmVyLnN0b3AoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5zdGF0dXMgPT0gJ3N1Y2Nlc3MnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjbW9kYWxfZm9yZ290X3Bhc3N3b3JkX19mb3JtIC5maWVsZHNcIikuaGlkZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiI21vZGFsX2ZvcmdvdF9wYXNzd29yZF9fZm9ybSAubWVzc2FnZVwiKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNtb2RhbF9mb3Jnb3RfcGFzc3dvcmRfX2Zvcm0gLmZpZWxkcyAuZXJyb3JcIikuc2hvdygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiI21vZGFsX2ZvcmdvdF9wYXNzd29yZF9fZm9ybSAuZmllbGRzIC5lcnJvciAubGlzdFwiKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjbW9kYWxfZm9yZ290X3Bhc3N3b3JkX19mb3JtIC5maWVsZHMgLmVycm9yIC5saXN0IC5pdGVtXCIpLmhpZGUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEucmVzcG9uc2Uuc3RhdHVzID09IDQwNCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNtb2RhbF9mb3Jnb3RfcGFzc3dvcmRfX2Zvcm1fX2lucHV0X3N1Ym1pdC1lcnJvclwiKS5odG1sKGRhdGEucmVzcG9uc2UucmVzcG9uc2VKU09OLmVycm9yLm1lc3NhZ2UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNtb2RhbF9mb3Jnb3RfcGFzc3dvcmRfX2Zvcm1fX2lucHV0X3N1Ym1pdC1lcnJvclwiKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiI21vZGFsX2ZvcmdvdF9wYXNzd29yZF9fZm9ybV9faW5wdXRfc3VibWl0LWVycm9yXCIpLmh0bWwoXCJVbmFibGUgdG8gcHJvY2VzcyB5b3VyIHJlcXVlc3QuIFBsZWFzZSB0cnkgYWdhaW5cIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiI21vZGFsX2ZvcmdvdF9wYXNzd29yZF9fZm9ybV9faW5wdXRfc3VibWl0LWVycm9yXCIpLnNob3coKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgfSk7XG5cblxuICAgICAgICAkZGlhbG9nLm9uKCdoaWRkZW4uYnMubW9kYWwnLGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJChcIiNtb2RhbF9mb3Jnb3RfcGFzc3dvcmRfX2Zvcm0gLmZpZWxkc1wiKS5zaG93KCk7XG4gICAgICAgICAgICAkKFwiI21vZGFsX2ZvcmdvdF9wYXNzd29yZF9fZm9ybSAubWVzc2FnZVwiKS5oaWRlKCk7XG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICBfdGhpcy5zaG93TW9kYWxTaWduVXAgPSBmdW5jdGlvbigpe1xuICAgICAgICB2YXIgdGl0bGUgPSBzd2lnLnJlbmRlcihfdGhpcy5kYXRhLnRpdGxlVGVtcGxhdGUsIHtsb2NhbHM6IHtcbiAgICAgICAgICAgICAgICB0aXRsZTogJ1NJR04gVVAgTk9XJ1xuICAgICAgICAgICAgfX0pLFxuICAgICAgICAgICAgbWVzc2FnZVRlbXBsYXRlID0gbXVsdGlsaW5lKGZ1bmN0aW9uKCl7LyohQHByZXNlcnZlXG4gICAgICAgICAgICA8Zm9ybSBjbGFzcz1cInVuaXZ0b3AgZm9ybVwiIGlkPVwibW9kYWxfc2lnbl91cF9fZm9ybVwiIGFjdGlvbj0nL2FjY291bnQvbG9naW4vJyBtZXRob2Q9J1BPU1QnPlxuICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiaGlkZGVuXCIgbmFtZT1cImNzcmZtaWRkbGV3YXJldG9rZW5cIiB2YWx1ZT1cIlwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbmxpbmUgZmllbGRzXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJzaXggd2lkZSBmaWVsZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X2ZpcnN0X25hbWVcIj5GaXJzdCBOYW1lPC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGlkPVwibW9kYWxfc2lnbl91cF9fZm9ybV9faW5wdXRfZmlyc3RfbmFtZVwiIG5hbWU9XCJmaXJzdE5hbWVcIi8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic2l4IHdpZGUgZmllbGRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJtb2RhbF9zaWduX3VwX19mb3JtX19pbnB1dF9sYXN0X25hbWVcIj5MYXN0IE5hbWU8L2xhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgaWQ9XCJtb2RhbF9zaWduX3VwX19mb3JtX19pbnB1dF9sYXN0X25hbWVcIiBuYW1lPVwibGFzdE5hbWVcIi8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZHNcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwibW9kYWxfc2lnbl91cF9fZm9ybV9faW5wdXRfZW1haWxcIj5FbWFpbDwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBpZD1cIm1vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X2VtYWlsXCIgbmFtZT1cImVtYWlsXCIvPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwibW9kYWxfc2lnbl91cF9fZm9ybV9faW5wdXRfcGFzc3dvcmRcIj5QYXNzd29yZDwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInBhc3N3b3JkXCIgaWQ9XCJtb2RhbF9zaWduX3VwX19mb3JtX19pbnB1dF9wYXNzd29yZFwiIG5hbWU9XCJwYXNzd29yZFwiLz5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X3Bhc3N3b3JkX2NvbmZpcm1cIj5Db25maXJtIFBhc3N3b3JkPC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicGFzc3dvcmRcIiBpZD1cIm1vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X3Bhc3N3b3JkX2NvbmZpcm1cIiBuYW1lPVwicGFzc3dvcmRfY29uZmlybVwiLz5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0ZXh0LXJpZ2h0XCI+QWxyZWFkeSBvbiBVbml2dG9wPyA8YSBkYXRhLXRhcmdldD1cIm1vZGFsLWxvZ2luXCI+U2lnbiBJbjwvYT48L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRzXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJlcnJvciBmaWVsZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxpc3RcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGlkPSdtb2RhbF9zaWduX3VwX19mb3JtX19pbnB1dF9zdWJtaXQtZXJyb3InIGNsYXNzPVwiaXRlbVwiIHN0eWxlPSdkaXNwbGF5Om5vbmUnPjwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZHNcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwidW5pdnRvcCBidXR0b25cIiB0eXBlPVwic3VibWl0XCI+TGV0J3MgZ28hPC9idXR0b24+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9mb3JtPlxuICAgICAgICAgICAgPGRpdiBpZD0nc2lnbnVwX2xvYWRpbmdfaWNvbic+PC9kaXY+XG4gICAgICAgICAgICAqL2NvbnNvbGUubG9nfSksXG4gICAgICAgICAgICBtZXNzYWdlID0gc3dpZy5yZW5kZXIobWVzc2FnZVRlbXBsYXRlKTtcbiAgICAgICAgO1xuXG4gICAgICAgIHZhciAkZGlhbG9nID0gYm9vdGJveC5kaWFsb2coe1xuICAgICAgICAgICAgY2xhc3NOYW1lOiAnbW9kYWwtc2lnbnVwJyxcbiAgICAgICAgICAgIHRpdGxlOiB0aXRsZSxcbiAgICAgICAgICAgIG1lc3NhZ2U6IG1lc3NhZ2UsXG4gICAgICAgICAgICBvbkVzY2FwZTogdHJ1ZSxcbiAgICAgICAgICAgIGJhY2tkcm9wOiAnc3RhdGljJ1xuICAgICAgICB9KTtcblxuICAgICAgICAkZGlhbG9nLm9uKCdzaG93bi5icy5tb2RhbCcsIGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICB2YXIgJGZvcm0gPSAkZGlhbG9nLmZpbmQoJyNtb2RhbF9zaWduX3VwX19mb3JtJyk7XG4gICAgICAgICAgICB2YXIgY3NyZnRva2VuID0gQ29va2llcy5nZXQoJ2NzcmZ0b2tlbicpO1xuICAgICAgICAgICAgJChcIiNtb2RhbF9zaWduX3VwX19mb3JtIGlucHV0W25hbWU9J2NzcmZtaWRkbGV3YXJldG9rZW4nXVwiKS52YWwoY3NyZnRva2VuKTtcblxuICAgICAgICAgICAgJGZvcm0ub24oJ3N1Ym1pdCcsIGZ1bmN0aW9uKGV2ZW50KXtcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIGpRdWVyeS52YWxpZGF0b3IuYWRkTWV0aG9kKFwibWF0Y2hQYXNzd29yZFwiLCBmdW5jdGlvbih2YWx1ZSwgZWxlbWVudCkge1xuICAgICAgICAgICAgICAgIHZhciBwYXNzd29yZDEgPSAkKFwiI21vZGFsX3NpZ25fdXBfX2Zvcm0gaW5wdXRbbmFtZT0ncGFzc3dvcmQnXVwiKS52YWwoKTtcbiAgICAgICAgICAgICAgICB2YXIgcGFzc3dvcmQyID0gJChcIiNtb2RhbF9zaWduX3VwX19mb3JtIGlucHV0W25hbWU9J3Bhc3N3b3JkX2NvbmZpcm0nXVwiKS52YWwoKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gcGFzc3dvcmQxID09IHBhc3N3b3JkMjtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAkZm9ybS52YWxpZGF0ZSh7XG4gICAgICAgICAgICAgICAgb25jbGljazogZmFsc2UsXG4gICAgICAgICAgICAgICAgb25mb2N1c291dDogZmFsc2UsXG4gICAgICAgICAgICAgICAgb25rZXl1cDogZmFsc2UsXG4gICAgICAgICAgICAgICAgcnVsZXM6IHtcbiAgICAgICAgICAgICAgICAgICAgZmlyc3ROYW1lOiBcInJlcXVpcmVkXCIsXG4gICAgICAgICAgICAgICAgICAgIGxhc3ROYW1lOiBcInJlcXVpcmVkXCIsXG4gICAgICAgICAgICAgICAgICAgIGVtYWlsOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGVtYWlsOiB0cnVlXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIHBhc3N3b3JkOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbmxlbmd0aDogNlxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBwYXNzd29yZF9jb25maXJtOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hdGNoUGFzc3dvcmQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG1lc3NhZ2VzOiB7XG4gICAgICAgICAgICAgICAgICAgIGVtYWlsOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogXCJQbGVhc2UgaW5wdXQgeW91ciBlbWFpbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgZW1haWw6IFwiUGxlYXNlIGVudGVyIGNvcnJlY3QgZW1haWxcIlxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBmaXJzdE5hbWU6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBcIlBsZWFzZSBpbnB1dCB5b3VyIGZpcnN0IG5hbWVcIixcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgbGFzdE5hbWU6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBcIlBsZWFzZSBpbnB1dCB5b3VyIGxhc3QgbmFtZVwiLFxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBwYXNzd29yZDoge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IFwiUGxlYXNlIGlucHV0IHlvdXIgcGFzc3dvcmRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbmxlbmd0aDogXCJQYXNzd29yZCBoYXMgdG8gYmUgYXQgbGVhc3QgNiBjaGFyYWN0ZXJzXCJcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgcGFzc3dvcmRfY29uZmlybToge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IFwiUGxlYXNlIGNvbmZpcm0geW91ciBwYXNzd29yZFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgbWF0Y2hQYXNzd29yZDogXCJDb25maXJtIHBhc3N3b3JkIGRvZXMgbm90IG1hdGNoLiBQbGVhc2UgdHJ5IGFnYWluXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZXJyb3JDb250YWluZXI6ICcjbW9kYWxfc2lnbl91cF9fZm9ybSAuZmllbGRzIC5maWVsZC5lcnJvcicsXG4gICAgICAgICAgICAgICAgZXJyb3JMYWJlbENvbnRhaW5lcjogJyNtb2RhbF9zaWduX3VwX19mb3JtIC5maWVsZHMgLmZpZWxkLmVycm9yIC5saXN0JyxcbiAgICAgICAgICAgICAgICBlcnJvckVsZW1lbnQ6ICdkaXYnLFxuICAgICAgICAgICAgICAgIGVycm9yQ2xhc3M6ICdpdGVtJyxcblxuICAgICAgICAgICAgICAgIHN1Ym1pdEhhbmRsZXI6IGZ1bmN0aW9uKGZvcm0pIHsgXG4gICAgICAgICAgICAgICAgICAgICQoXCIjbW9kYWxfc2lnbl91cF9fZm9ybV9faW5wdXRfc3VibWl0LWVycm9yXCIpLmh0bWwoXCJcIik7XG4gICAgICAgICAgICAgICAgICAgIHZhciBzcGlubmVyID0gbmV3IFNwaW5uZXIoKS5zcGluKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdzaWdudXBfbG9hZGluZ19pY29uJykpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgZm9ybURhdGEgPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcImVtYWlsXCI6ICQoXCIjbW9kYWxfc2lnbl91cF9fZm9ybSBpbnB1dFtuYW1lPSdlbWFpbCddXCIpLnZhbCgpLFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJmaXJzdF9uYW1lXCI6ICQoXCIjbW9kYWxfc2lnbl91cF9fZm9ybSBpbnB1dFtuYW1lPSdmaXJzdE5hbWUnXVwiKS52YWwoKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIFwibGFzdF9uYW1lXCI6ICQoXCIjbW9kYWxfc2lnbl91cF9fZm9ybSBpbnB1dFtuYW1lPSdsYXN0TmFtZSddXCIpLnZhbCgpLFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJwYXNzd29yZFwiOiAkKFwiI21vZGFsX3NpZ25fdXBfX2Zvcm0gaW5wdXRbbmFtZT0ncGFzc3dvcmQnXVwiKS52YWwoKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiY29uZmlybV90eXBlXCI6IFwiYWN0aXZhdGVfbGlua1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgc2FuZGJveC5lbWl0KCdhamF4L3JlcXVlc3QnLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBhamF4T3B0aW9uczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybDogJChcIiNhcGlfdXJsXCIpLnZhbCgpICsgXCIvYXBpL3YxL2F1dGgvc2lnbnVwL1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdQT1NUJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiBmb3JtRGF0YVxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbihkYXRhKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5zdGF0dXMgPT0gJ3N1Y2Nlc3MnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm0uc3VibWl0KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3Bpbm5lci5zdG9wKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjbW9kYWxfc2lnbl91cF9fZm9ybSAuZmllbGRzIC5lcnJvclwiKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjbW9kYWxfc2lnbl91cF9fZm9ybSAuZmllbGRzIC5lcnJvciAubGlzdFwiKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjbW9kYWxfc2lnbl91cF9fZm9ybSAuZmllbGRzIC5lcnJvciAubGlzdCAuaXRlbVwiKS5oaWRlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLnJlc3BvbnNlLnN0YXR1cyA9PSA0MDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjbW9kYWxfc2lnbl91cF9fZm9ybV9faW5wdXRfc3VibWl0LWVycm9yXCIpLmh0bWwoZGF0YS5yZXNwb25zZS5yZXNwb25zZUpTT04uZXJyb3IubWVzc2FnZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiI21vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X3N1Ym1pdC1lcnJvclwiKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKFwiI21vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X3N1Ym1pdC1lcnJvclwiKS5odG1sKFwiVW5hYmxlIHRvIHByb2Nlc3MgeW91ciByZXF1ZXN0LiBQbGVhc2UgdHJ5IGFnYWluXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNtb2RhbF9zaWduX3VwX19mb3JtX19pbnB1dF9zdWJtaXQtZXJyb3JcIikuc2hvdygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfTtcblxuXG4gICAgX3RoaXMuc2hvd01vZGFsUmVzZXRQYXNzd29yZCA9IGZ1bmN0aW9uKCl7XG4gICAgICAgIHZhciB0aXRsZSA9IHN3aWcucmVuZGVyKF90aGlzLmRhdGEudGl0bGVUZW1wbGF0ZSwge2xvY2Fsczoge1xuICAgICAgICAgICAgICAgIHRpdGxlOiAnUmVzZXQgeW91ciBwYXNzd29yZCdcbiAgICAgICAgICAgIH19KSxcbiAgICAgICAgICAgIG1lc3NhZ2VUZW1wbGF0ZSA9IG11bHRpbGluZShmdW5jdGlvbigpey8qIUBwcmVzZXJ2ZVxuICAgICAgICAgICAgPGZvcm0gY2xhc3M9XCJ1bml2dG9wIGZvcm1cIiBpZD1cIm1vZGFsX3Jlc2V0X3Bhc3N3b3JkX19mb3JtXCIgbWV0aG9kPSdQT1NUJz5cbiAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImhpZGRlblwiIG5hbWU9XCJjc3JmbWlkZGxld2FyZXRva2VuXCIgdmFsdWU9XCJcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRzXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1vZGFsX3Jlc2V0X3Bhc3N3b3JkX19mb3JtX19pbnB1dF9wYXNzd29yZDFcIj5OZXcgUGFzc3dvcmQ8L2xhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJwYXNzd29yZFwiIGlkPVwibW9kYWxfcmVzZXRfcGFzc3dvcmRfX2Zvcm1fX2lucHV0X3Bhc3N3b3JkMVwiIG5hbWU9XCJwYXNzd29yZDFcIi8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJtb2RhbF9yZXNldF9wYXNzd29yZF9fZm9ybV9faW5wdXRfcGFzc3dvcmQyXCI+Q29uZmlybSBQYXNzd29yZDwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInBhc3N3b3JkXCIgaWQ9XCJtb2RhbF9yZXNldF9wYXNzd29yZF9fZm9ybV9faW5wdXRfcGFzc3dvcmQyXCIgbmFtZT1cInBhc3N3b3JkMlwiLz5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkc1wiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZXJyb3IgZmllbGRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsaXN0XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD0nbW9kYWxfcmVzZXRfcGFzc3dvcmRfX2Zvcm1fX2lucHV0X3N1Ym1pdC1lcnJvcicgY2xhc3M9XCJpdGVtXCIgc3R5bGU9J2Rpc3BsYXk6bm9uZSc+PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkc1wiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGQgdGV4dC1jZW50ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJ1bml2dG9wIGJ1dHRvblwiIHR5cGU9XCJzdWJtaXRcIj5SZXNldCBQYXNzd29yZDwvYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZm9ybT5cbiAgICAgICAgICAgIDxkaXYgaWQ9J3Jlc2V0X3Bhc3N3b3JkX2xvYWRpbmdfaWNvbic+PC9kaXY+XG4gICAgICAgICAgICAqL2NvbnNvbGUubG9nfSksXG4gICAgICAgICAgICBtZXNzYWdlID0gc3dpZy5yZW5kZXIobWVzc2FnZVRlbXBsYXRlKTtcbiAgICAgICAgO1xuXG4gICAgICAgIHZhciAkZGlhbG9nID0gYm9vdGJveC5kaWFsb2coe1xuICAgICAgICAgICAgY2xhc3NOYW1lOiAnbW9kYWwtcmVzZXQtcGFzc3dvcmQnLFxuICAgICAgICAgICAgdGl0bGU6IHRpdGxlLFxuICAgICAgICAgICAgbWVzc2FnZTogbWVzc2FnZSxcbiAgICAgICAgICAgIG9uRXNjYXBlOiB0cnVlLFxuICAgICAgICAgICAgYmFja2Ryb3A6ICdzdGF0aWMnXG4gICAgICAgIH0pO1xuXG4gICAgICAgICRkaWFsb2cub24oJ3Nob3duLmJzLm1vZGFsJywgZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIHZhciAkZm9ybSA9ICRkaWFsb2cuZmluZCgnI21vZGFsX3Jlc2V0X3Bhc3N3b3JkX19mb3JtJyk7XG4gICAgICAgICAgICB2YXIgY3NyZnRva2VuID0gQ29va2llcy5nZXQoJ2NzcmZ0b2tlbicpO1xuICAgICAgICAgICAgJChcIiNtb2RhbF9yZXNldF9wYXNzd29yZF9fZm9ybSBpbnB1dFtuYW1lPSdjc3JmbWlkZGxld2FyZXRva2VuJ11cIikudmFsKGNzcmZ0b2tlbik7XG5cbiAgICAgICAgICAgICRmb3JtLm9uKCdzdWJtaXQnLCBmdW5jdGlvbihldmVudCl7XG4gICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBqUXVlcnkudmFsaWRhdG9yLmFkZE1ldGhvZChcIm1hdGNoUGFzc3dvcmRcIiwgZnVuY3Rpb24odmFsdWUsIGVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICB2YXIgcGFzc3dvcmQxID0gJChcIiNtb2RhbF9yZXNldF9wYXNzd29yZF9fZm9ybSBpbnB1dFtuYW1lPSdwYXNzd29yZDEnXVwiKS52YWwoKTtcbiAgICAgICAgICAgICAgICB2YXIgcGFzc3dvcmQyID0gJChcIiNtb2RhbF9yZXNldF9wYXNzd29yZF9fZm9ybSBpbnB1dFtuYW1lPSdwYXNzd29yZDInXVwiKS52YWwoKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gcGFzc3dvcmQxID09IHBhc3N3b3JkMjtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAkZm9ybS52YWxpZGF0ZSh7XG4gICAgICAgICAgICAgICAgb25jbGljazogZmFsc2UsXG4gICAgICAgICAgICAgICAgb25mb2N1c291dDogZmFsc2UsXG4gICAgICAgICAgICAgICAgb25rZXl1cDogZmFsc2UsXG4gICAgICAgICAgICAgICAgcnVsZXM6IHtcbiAgICAgICAgICAgICAgICAgICAgcGFzc3dvcmQxOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbmxlbmd0aDogNlxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBwYXNzd29yZDI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgbWlubGVuZ3RoOiA2LFxuICAgICAgICAgICAgICAgICAgICAgICAgbWF0Y2hQYXNzd29yZDogdHJ1ZVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBtZXNzYWdlczoge1xuICAgICAgICAgICAgICAgICAgICBwYXNzd29yZDE6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBcIlBsZWFzZSBpbnB1dCB5b3VyIGVtYWlsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5sZW5ndGg6IFwiUGFzc3dvcmQgaGFzIHRvIGJlIGF0IGxlYXN0IDYgY2hhcmFjdGVyc1wiXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIHBhc3N3b3JkMjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IFwiUGxlYXNlIGNvbmZpcm0geW91ciBwYXNzd29yZFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgbWF0Y2hQYXNzd29yZDogXCJDb25maXJtIHBhc3N3b3JkIGRvZXMgbm90IG1hdGNoLiBQbGVhc2UgdHJ5IGFnYWluXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZXJyb3JDb250YWluZXI6ICcjbW9kYWxfcmVzZXRfcGFzc3dvcmRfX2Zvcm0gLmZpZWxkcyAuZmllbGQuZXJyb3InLFxuICAgICAgICAgICAgICAgIGVycm9yTGFiZWxDb250YWluZXI6ICcjbW9kYWxfcmVzZXRfcGFzc3dvcmRfX2Zvcm0gLmZpZWxkcyAuZmllbGQuZXJyb3IgLmxpc3QnLFxuICAgICAgICAgICAgICAgIGVycm9yRWxlbWVudDogJ2RpdicsXG4gICAgICAgICAgICAgICAgZXJyb3JDbGFzczogJ2l0ZW0nLFxuXG4gICAgICAgICAgICAgICAgc3VibWl0SGFuZGxlcjogZnVuY3Rpb24oZm9ybSkgeyBcbiAgICAgICAgICAgICAgICAgICAgZm9ybS5zdWJtaXQoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgJGRpYWxvZy5vbignaGlkZS5icy5tb2RhbCcsZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IFwiL1wiXG4gICAgICAgIH0pO1xuICAgIH07XG5cblxuICAgIF90aGlzLnNob3dJbml0TW9kYWwgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIGluaXRfbW9kYWwgPSBnZXRVcmxQYXJhbWV0ZXIoXCJtb2RhbFwiKTtcbiAgICAgICAgaWYgKGluaXRfbW9kYWwgPT0gJ2xvZ2luJykge1xuICAgICAgICAgICAgX3RoaXMuaGlkZUFsbE1vZGFscygpO1xuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBfdGhpcy5zaG93TW9kYWxMb2dpbigpO1xuICAgICAgICAgICAgfSw0MDApXG4gICAgICAgIH0gZWxzZSBpZiAoaW5pdF9tb2RhbCA9PSAnc2lnbnVwJykge1xuICAgICAgICAgICAgX3RoaXMuaGlkZUFsbE1vZGFscygpO1xuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBfdGhpcy5zaG93TW9kYWxTaWduVXAoKTtcbiAgICAgICAgICAgIH0sNDAwKTtcbiAgICAgICAgfSBlbHNlIGlmIChpbml0X21vZGFsID09ICdmb3Jnb3RfcGFzc3dvcmQnKSB7XG4gICAgICAgICAgICBfdGhpcy5oaWRlQWxsTW9kYWxzKCk7XG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIF90aGlzLnNob3dNb2RhbEZvcmdvdFBhc3N3b3JkKCk7XG4gICAgICAgICAgICB9LDQwMCk7XG4gICAgICAgIH0gXG4gICAgfVxuXG4gICAgc2FuZGJveC5vbignbW9kYWwvbG9naW4vc2hvdycsIGZ1bmN0aW9uKCl7XG4gICAgICAgIF90aGlzLmhpZGVBbGxNb2RhbHMoKTtcbiAgICAgICAgX3RoaXMuc2hvd01vZGFsTG9naW4oKTtcbiAgICB9KTtcblxuICAgIHNhbmRib3gub24oJ21vZGFsL2ZvcmdvdFBhc3N3b3JkL3Nob3cnLCBmdW5jdGlvbigpe1xuICAgICAgICBfdGhpcy5oaWRlQWxsTW9kYWxzKCk7XG4gICAgICAgIF90aGlzLnNob3dNb2RhbEZvcmdvdFBhc3N3b3JkKCk7XG4gICAgfSk7XG5cbiAgICBzYW5kYm94Lm9uKCdtb2RhbC9yZXNldFBhc3N3b3JkL3Nob3cnLCBmdW5jdGlvbigpe1xuICAgICAgICBfdGhpcy5oaWRlQWxsTW9kYWxzKCk7XG4gICAgICAgIF90aGlzLnNob3dNb2RhbFJlc2V0UGFzc3dvcmQoKTtcbiAgICB9KTtcblxuICAgIHNhbmRib3gub24oJ21vZGFsL3NpZ25VcC9zaG93JywgZnVuY3Rpb24oKXtcbiAgICAgICAgX3RoaXMuaGlkZUFsbE1vZGFscygpO1xuICAgICAgICBfdGhpcy5zaG93TW9kYWxTaWduVXAoKTtcbiAgICB9KTtcblxuICAgIF90aGlzLmluaXQgPSBmdW5jdGlvbihkYXRhKXtcbiAgICAgICAgX3RoaXMuZGF0YSA9IHt9O1xuICAgICAgICBfdGhpcy5kYXRhLnRpdGxlVGVtcGxhdGUgPSBtdWx0aWxpbmUoZnVuY3Rpb24oKXsvKiFAcHJlc2VydmVcbiAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidGl0bGVcIj57e3RpdGxlfX08L2Rpdj5cbiAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3VidGl0bGVcIj57e3N1YnRpdGxlfX08L2Rpdj5cbiAgICAgICAgICovY29uc29sZS5sb2d9KTtcbiAgICAgICAgX3RoaXMuc2hvd0luaXRNb2RhbCgpO1xuICAgIH0gXG4gICAgX3RoaXMuZGVzdHJveSA9IGZ1bmN0aW9uKCl7fVxuXG4gICAgcmV0dXJuIHtcbiAgICAgICAgaW5pdDogX3RoaXMuaW5pdCxcbiAgICAgICAgZGVzdHJveTogX3RoaXMuZGVzdHJveVxuICAgIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBtb2R1bGVNb2RhbDsiLCIndXNlIHN0cmljdCc7XG5cbnZhciBtb2R1bGVXaW5kb3cgPSBmdW5jdGlvbihzYW5kYm94KXtcbiAgICB2YXIgX3RoaXMgPSB0aGlzO1xuXG4gICAgX3RoaXMuaW5pdCA9IGZ1bmN0aW9uKGRhdGEpe1xuICAgICAgIFxuICAgICAgICBpZiAoJChcIiNwYWdlX3R5cGVcIikudmFsKCkgPT0gXCJhY2NvdW50X3Bhc3N3b3JkX3Jlc2V0X2Zyb21fa2V5XCIpIHtcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgc2FuZGJveC5lbWl0KCdtb2RhbC9yZXNldFBhc3N3b3JkL3Nob3cnKTtcbiAgICAgICAgICAgIH0sNDAwKTtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdmFyICRib2R5ID0gJCgnYm9keScpO1xuXG4gICAgICAgICRib2R5XG4gICAgICAgICAgICAub24oJ2NsaWNrJywgJ1tkYXRhLXRhcmdldD1tb2RhbC1sb2dpbl0nLCBmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgIHNhbmRib3guZW1pdCgnbW9kYWwvbG9naW4vc2hvdycpO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC8vIC5vbignY2xpY2snLCAnW2RhdGEtdGFyZ2V0PW1vZGFsLXJlZ2lzdGVyXScsIGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAvLyAgICAgc2FuZGJveC5lbWl0KCdtb2RhbC9yZWdpc3Rlci9zaG93Jyk7XG4gICAgICAgICAgICAvLyB9KVxuICAgICAgICAgICAgLm9uKCdjbGljaycsICdbZGF0YS10YXJnZXQ9bW9kYWwtZm9yZ290LXBhc3N3b3JkXScsIGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgc2FuZGJveC5lbWl0KCdtb2RhbC9mb3Jnb3RQYXNzd29yZC9zaG93Jyk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLm9uKCdjbGljaycsICdbZGF0YS10YXJnZXQ9bW9kYWwtc2lnbi11cF0nLCBmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgIHNhbmRib3guZW1pdCgnbW9kYWwvc2lnblVwL3Nob3cnKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAub24oJ2NsaWNrJywgJy5xdWVzdGlvbiBbZGF0YS1hY3Rpb249XCJleHBhbmRcIl0nLCBmdW5jdGlvbihldmVudCl7XG4gICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyksXG4gICAgICAgICAgICAgICAgICAgICRjdXJyZW50UXVlc3Rpb24gPSAkdGhpcy5jbG9zZXN0KCcucXVlc3Rpb24nKTtcblxuICAgICAgICAgICAgICAgIGlmICgkY3VycmVudFF1ZXN0aW9uLmF0dHIoXCJkYXRhLWlzLWV4cGFuZGVkXCIpID09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgaWYoJGN1cnJlbnRRdWVzdGlvbi5sZW5ndGgpe1xuICAgICAgICAgICAgICAgICAgICAgICAgJGN1cnJlbnRRdWVzdGlvbi5hdHRyKCdkYXRhLWlzLWV4cGFuZGVkJywgMSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAkY3VycmVudFF1ZXN0aW9uLmF0dHIoJ2RhdGEtaXMtZXhwYW5kZWQnLCAwKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLm9uKFwiY2xpY2tcIiwnLnF1ZXN0aW9uIFtkYXRhLWFjdGlvbj1cImZ1bGwtY29udGVudFwiXScsZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKSxcbiAgICAgICAgICAgICAgICAgICAgJGN1cnJlbnRUZXh0ID0gJHRoaXMuY2xvc2VzdCgnLnRleHQnKTtcblxuICAgICAgICAgICAgICAgICRjdXJyZW50VGV4dC5maW5kKFwiLmZ1bGwtY29udGVudFwiKS5yZW1vdmVDbGFzcyhcImhpZGVcIik7XG4gICAgICAgICAgICAgICAgJGN1cnJlbnRUZXh0LmZpbmQoXCIuYnJpZWYtY29udGVudFwiKS5hZGRDbGFzcyhcImhpZGVcIik7XG5cbiAgICAgICAgICAgICAgICAkdGhpcy5hZGRDbGFzcyhcImhpZGVcIik7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgO1xuXG4gICAgfVxuICAgIF90aGlzLmRlc3Ryb3kgPSBmdW5jdGlvbigpe31cblxuICAgIHJldHVybiB7XG4gICAgICAgIGluaXQ6IF90aGlzLmluaXQsXG4gICAgICAgIGRlc3Ryb3k6IF90aGlzLmRlc3Ryb3lcbiAgICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gbW9kdWxlV2luZG93OyIsIid1c2Ugc3RyaWN0JztcblxudmFyIHBhZ2VDb250YWN0ID0gZnVuY3Rpb24oc2FuZGJveCl7XG4gICAgdmFyIF90aGlzID0gdGhpcztcblxuICAgIF90aGlzLmhhbmRsZUZvcm0gPSBmdW5jdGlvbigpe1xuICAgICAgICB2YXIgJGZvcm0gPSAkKCcjc2VnbWVudF8yX19mb3JtJyksXG4gICAgICAgICAgICAkbGlzdEVycm9ycyA9ICRmb3JtLmZpbmQoJy5ncm91cC5lcnJvcicpXG4gICAgICAgIDtcblxuICAgICAgICAkZm9ybS5vbignc3VibWl0JywgZnVuY3Rpb24oZXZlbnQpe1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgJGZvcm0udmFsaWRhdGUoe1xuICAgICAgICAgICAgZGVidWc6IHRydWUsXG4gICAgICAgICAgICBydWxlczoge1xuICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInJlcXVpcmVkXCIsXG4gICAgICAgICAgICAgICAgXCJlbWFpbFwiOiB7XG4gICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICBlbWFpbDogdHJ1ZVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgXCJzdWJqZWN0XCI6IFwicmVxdWlyZWRcIixcbiAgICAgICAgICAgICAgICBcIm1lc3NhZ2VcIjoge1xuICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBtZXNzYWdlczoge1xuICAgICAgICAgICAgICAgIG5hbWU6IFwiUGxlYXNlIHNwZWNpZnkgeW91ciBuYW1lXCIsXG4gICAgICAgICAgICAgICAgZW1haWw6IHtcbiAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IFwiV2UgbmVlZCB5b3VyIGVtYWlsIGFkZHJlc3MgdG8gY29udGFjdCB5b3VcIixcbiAgICAgICAgICAgICAgICAgICAgZW1haWw6IFwiWW91ciBlbWFpbCBhZGRyZXNzIG11c3QgYmUgaW4gdGhlIGZvcm1hdCBvZiBuYW1lQGRvbWFpbi5jb21cIlxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgXCJzdWJqZWN0XCI6IFwiUGxlYXNlIHNwZWNpZnkgeW91ciBzdWJqZWN0XCIsXG4gICAgICAgICAgICAgICAgXCJtZXNzYWdlXCI6IFwiUGxlYXNlIHNwZWNpZnkgeW91ciBtZXNzYWdlXCJcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBlcnJvckNvbnRhaW5lcjogJyNzZWdtZW50XzJfX2Zvcm0gLmdyb3VwLmVycm9yJyxcbiAgICAgICAgICAgIGVycm9yTGFiZWxDb250YWluZXI6ICcjc2VnbWVudF8yX19mb3JtIC5ncm91cC5lcnJvciAubGlzdCcsXG4gICAgICAgICAgICBlcnJvckVsZW1lbnQ6ICdkaXYnLFxuICAgICAgICAgICAgZXJyb3JDbGFzczogJ2l0ZW0nLFxuICAgICAgICAgICAgLy9lcnJvclBsYWNlbWVudDogZnVuY3Rpb24oZXJyb3IsIGVsZW1lbnQpe1xuICAgICAgICAgICAgLy8gICAgJGxpc3RFcnJvcnMuaHRtbCgnJyk7XG4gICAgICAgICAgICAvLyAgICAkKCc8ZGl2IGNsYXNzPVwiaXRlbVwiPjwvZGl2PicpLmh0bWwoJChlcnJvcikudGV4dCgpKS5hcHBlbmRUbygkbGlzdEVycm9ycyk7XG4gICAgICAgICAgICAvLyAgICBlbGVtZW50LmFkZENsYXNzKCdlcnJvcicpO1xuICAgICAgICAgICAgLy99LFxuICAgICAgICAgICAgc3VibWl0SGFuZGxlcjogZnVuY3Rpb24oZm9ybSkgeyBcbiAgICAgICAgICAgICAgICBmb3JtLnN1Ym1pdCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBfdGhpcy5pbml0ID0gZnVuY3Rpb24oZGF0YSl7XG4gICAgICAgIF90aGlzLmhhbmRsZUZvcm0oKTtcbiAgICB9XG4gICAgX3RoaXMuZGVzdHJveSA9IGZ1bmN0aW9uKCl7fVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHBhZ2VDb250YWN0OyIsIid1c2Ugc3RyaWN0JztcblxudmFyIHBhZ2VEYXNoYm9hcmQgPSBmdW5jdGlvbihzYW5kYm94KXtcbiAgICB2YXIgX3RoaXMgPSB0aGlzO1xuXG4gICAgX3RoaXMuaGFuZGxlVGFicyA9IGZ1bmN0aW9uKCl7XG4gICAgXHR2YXIgJG1lbnUgPSAkKCcjc2VnbWVudF8yX19tZW51JyksXG4gICAgICAgICAgICAkdGFicyA9ICQoJyNzZWdtZW50XzNfX3NlZ21lbnRfcXVlc3Rpb25zX190YWJzJylcbiAgICAgICAgO1xuXG4gICAgICAgICRtZW51Lm9uKCdjbGljaycsICc+Lml0ZW0nLCBmdW5jdGlvbigpe1xuICAgICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKSxcbiAgICAgICAgICAgICAgICAkdGFyZ2V0ID0gJCgkdGhpcy5kYXRhKCd0YXJnZXQnKSlcbiAgICAgICAgICAgIDtcblxuICAgICAgICAgICAgJHRoaXNcbiAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICAgICAgICAgICAgLnNpYmxpbmdzKCcuYWN0aXZlJylcbiAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICAgICAgICA7XG5cbiAgICAgICAgICAgIGlmKCR0YXJnZXQubGVuZ3RoKXtcbiAgICAgICAgICAgICAgICAkdGFyZ2V0XG4gICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnYWN0aXZlJylcbiAgICAgICAgICAgICAgICAgICAgLnNpYmxpbmdzKCcuYWN0aXZlJylcbiAgICAgICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCdhY3RpdmUnKVxuICAgICAgICAgICAgICAgIDtcblxuICAgICAgICAgICAgICAgIHZhciBzcGlubmVyID0gbmV3IFNwaW5uZXIoKS5zcGluKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdsb2FkaW5nX2ljb24nKSk7XG4gICAgICAgICAgICAgICAgdmFyIGRhdGEgPSB7XG4gICAgICAgICAgICAgICAgICAgIFwidXNlcm5hbWVcIjogJChcIiN1c2VyX2xvZ2luX3VzZXJuYW1lXCIpLnZhbCgpLFxuICAgICAgICAgICAgICAgICAgICBcInF1ZXN0aW9uX3R5cGVcIjogJHRhcmdldC5kYXRhKFwicXVlc3Rpb24tdHlwZVwiKSxcbiAgICAgICAgICAgICAgICAgICAgXCJzdGFydF9pbmRleFwiOiAwXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgYWpheEdldChcIi9hamF4L3VzZXIvbG9hZF91c2VyX3F1ZXN0aW9uc1wiLGRhdGEsZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgJHRhcmdldC5maW5kKFwiLmxpc3QucXVlc3Rpb25zXCIpLmh0bWwocmVzcG9uc2VbJ2NvbnRlbnQnXSk7XG4gICAgICAgICAgICAgICAgICAgICR0YXJnZXQuZmluZChcIi5zdGFydC1pbmRleFwiKS52YWwocmVzcG9uc2VbJ3N0YXJ0X2luZGV4J10pO1xuICAgICAgICAgICAgICAgICAgICAkdGFyZ2V0LmZpbmQoXCIuYWxsLWl0ZW0tbG9hZGVkXCIpLnZhbChyZXNwb25zZVsnaXNfYWxsX2l0ZW1zX2xvYWRlZCddKTtcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMuaGFuZGxlQW5zd2VyKCk7XG4gICAgICAgICAgICAgICAgICAgIHNwaW5uZXIuc3RvcCgpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgICRtZW51LmNoaWxkcmVuKCkuZmlyc3QoKS50cmlnZ2VyKCdjbGljaycpO1xuXG4gICAgICAgIHZhciB3aW4gPSAkKHdpbmRvdyk7XG5cbiAgICAgICAgd2luLnNjcm9sbChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmICgkKGRvY3VtZW50KS5oZWlnaHQoKSAtIHdpbi5oZWlnaHQoKSA9PSB3aW4uc2Nyb2xsVG9wKCkpIHtcblxuICAgICAgICAgICAgICAgIHZhciAkdGFyZ2V0ID0gJHRhYnMuZmluZChcIi5hY3RpdmVcIik7XG4gICAgICAgIFxuICAgICAgICAgICAgICAgIGlmICgkdGFyZ2V0LmZpbmQoXCIuYWxsLWl0ZW0tbG9hZGVkXCIpLnZhbCgpICE9IFwidHJ1ZVwiKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBzcGlubmVyID0gbmV3IFNwaW5uZXIoKS5zcGluKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdsb2FkaW5nX2ljb24nKSk7XG5cbiAgICAgICAgICAgICAgICAgICAgdmFyIGRhdGEgPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInVzZXJuYW1lXCI6ICQoXCIjdXNlcl9sb2dpbl91c2VybmFtZVwiKS52YWwoKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIFwicXVlc3Rpb25fdHlwZVwiOiAkdGFyZ2V0LmRhdGEoXCJxdWVzdGlvbi10eXBlXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJzdGFydF9pbmRleFwiOiAkdGFyZ2V0LmZpbmQoXCIuc3RhcnQtaW5kZXhcIikudmFsKClcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGFqYXhHZXQoXCIvYWpheC91c2VyL2xvYWRfdXNlcl9xdWVzdGlvbnNcIixkYXRhLGZ1bmN0aW9uKHJlc3BvbnNlKXtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNwaW5uZXIuc3RvcCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHBhcnNlSW50KHJlc3BvbnNlWydudW1faXRlbSddKSA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkdGFyZ2V0LmZpbmQoXCIubGlzdC5xdWVzdGlvbnNcIikuYXBwZW5kKHJlc3BvbnNlWydjb250ZW50J10pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0YXJnZXQuZmluZChcIi5zdGFydC1pbmRleFwiKS52YWwocmVzcG9uc2VbJ3N0YXJ0X2luZGV4J10pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0YXJnZXQuZmluZChcIi5hbGwtaXRlbS1sb2FkZWRcIikudmFsKHJlc3BvbnNlWydpc19hbGxfaXRlbXNfbG9hZGVkJ10pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF90aGlzLmhhbmRsZUFuc3dlcigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgX3RoaXMuaGFuZGxlQW5zd2VyID0gZnVuY3Rpb24oKXtcblxuICAgICAgICBfdGhpcy50ZW1wbGF0ZXMuYW5zd2VySXRlbSA9IG11bHRpbGluZShmdW5jdGlvbigpey8qXG4gICAgICAgIDxkaXYgY2xhc3M9XCJ1bml2dG9wIGNhcmRcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0aHVtYm5haWxcIj5cbiAgICAgICAgICAgICAgICA8aW1nIHNyYz0ne3thbnN3ZXIudXNlcl9wb3N0LmF2YXRhcn19JyB3aWR0aD0nNTYnIGhlaWdodD0nNTYnLz5cbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiI1wiIGNsYXNzPVwibWV0YSBuYW1lXCI+e3thbnN3ZXIudXNlcl9wb3N0LmZ1bGxuYW1lfX08L2E+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb250ZW50XCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNlZ21lbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImhlYWRlclwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1ldGEgdGltZVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHslIGlmIGFuc3dlci5lZGl0X2RhdGUgJX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgRWRpdGVkIHt7YW5zd2VyLmVkaXRfZGF0ZXxlbGFwc2VUaW1lfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7JSBlbHNlICV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt7YW5zd2VyLmNyZWF0ZV9kYXRlfGVsYXBzZVRpbWV9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHslIGVuZGlmICV9XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJib2R5XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3VtbWFyeVwiPnt7YW5zd2VyLmNvbnRlbnR9fTwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvb3RlclwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgeyUgaWYgYW5zd2VyLmlzX3VzZXJfbG9naW5fdm90ZSAlfVxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJ1bml2dG9wIGxhYmVsIHZvdGVcIiBkYXRhLWFjdGlvbj0ndW52b3RlLWFuc3dlcic+XG4gICAgICAgICAgICAgICAgICAgICAgICB7JSBlbHNlICV9XG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cInVuaXZ0b3AgbGFiZWwgdm90ZVwiIGRhdGEtYWN0aW9uPSd2b3RlLWFuc3dlcic+XG4gICAgICAgICAgICAgICAgICAgICAgICB7JSBlbmRpZiAlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwidW5pdnRvcCBpY29uIGhlYXJ0XCIgPjwvaT48c3BhbiBjbGFzcz0ndmFsdWUnPnt7YW5zd2VyLnRvdGFsX3ZvdGV9fTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgICovY29uc29sZS5sb2d9KTtcblxuICAgICAgICB2YXIgJHRhYnMgPSAkKCcjc2VnbWVudF8zX19zZWdtZW50X3F1ZXN0aW9uc19fdGFicycpO1xuXG4gICAgICAgICR0YWJzLmZpbmQoXCIudW5pdnRvcC5mb3JtLmFuc3dlclwiKS5vbihcInN1Ym1pdFwiLGZ1bmN0aW9uKGV2ZW50KXtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyksXG4gICAgICAgICAgICAgICAgJGNvbnRlbnQgPSAkdGhpcy5maW5kKFwidGV4dGFyZWFbbmFtZT0nY29udGVudCddXCIpLnZhbCgpO1xuXG4gICAgICAgICAgICB2YXIgdXNlcl9hbnN3ZXJfaWQgPSAkdGhpcy5kYXRhKFwiYW5zd2VyLWlkXCIpO1xuXG4gICAgICAgICAgICBpZiAoJGNvbnRlbnQubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgdmFyIHNwaW5uZXIgPSBuZXcgU3Bpbm5lcigpLnNwaW4oZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2xvYWRpbmdfaWNvbicpKTtcbiAgICAgICAgICAgICAgICB2YXIgcXVlc3Rpb25faWQgPSAkdGhpcy5hdHRyKFwiZGF0YS1xdWVzdGlvbi1pZFwiKTtcbiAgICAgICAgICAgICAgICB2YXIgZGF0YSA9IHtcbiAgICAgICAgICAgICAgICAgICAgXCJjb250ZW50XCI6ICRjb250ZW50XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKGlzVW5kZWZpbmVkKHVzZXJfYW5zd2VyX2lkKSkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgYWpheF9kYXRhID0geyBcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcInBvc3RcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidXJsXCI6ICQoXCIjYXBpX3VybFwiKS52YWwoKSArICcvYXBpL3YxL3F1ZXN0aW9uLycgKyBxdWVzdGlvbl9pZCArIFwiL2FkZF9hbnN3ZXIvXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcImRhdGFcIjogSlNPTi5zdHJpbmdpZnkoZGF0YSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBhamF4UG9zdChcIi9hamF4L2FwaS9jYWxsXCIsYWpheF9kYXRhLGZ1bmN0aW9uKGRhdGEpe1xuICAgICAgICAgICAgICAgICAgICAgICAgc3Bpbm5lci5zdG9wKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YVsnc3RhdHVzJ10gPT0gJ3N1Y2Nlc3MnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGl0ZW1fY29udGVudCA9IHN3aWcucmVuZGVyKF90aGlzLnRlbXBsYXRlcy5hbnN3ZXJJdGVtLCB7bG9jYWxzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbnN3ZXI6IGRhdGFbJ3Jlc3BvbnNlJ11bJ2Fuc3dlciddXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgaHRtbCA9IFwiPGRpdiBjbGFzcz0naXRlbSBhbnN3ZXInIGRhdGEtYW5zd2VyLWlkPSdcIiArIGRhdGFbJ3Jlc3BvbnNlJ11bJ2Fuc3dlcl9pZCddICsgXCInPlwiICsgaXRlbV9jb250ZW50ICsgXCI8L2Rpdj5cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciAkYW5zd2Vyc0xpc3QgPSAkdGhpcy5wYXJlbnQoKS5maW5kKFwiLmxpc3QuYW5zd2Vyc1wiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkYW5zd2Vyc0xpc3QuYXBwZW5kKGh0bWwpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0aGlzLmF0dHIoXCJkYXRhLWFuc3dlci1pZFwiLGRhdGFbJ3Jlc3BvbnNlJ11bJ2Fuc3dlcl9pZCddKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB2YXIgYWpheF9kYXRhID0geyBcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcInBhdGNoXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcInVybFwiOiAkKFwiI2FwaV91cmxcIikudmFsKCkgKyAnL2FwaS92MS9hbnN3ZXIvJyArIHVzZXJfYW5zd2VyX2lkICsgXCIvXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcImRhdGFcIjogSlNPTi5zdHJpbmdpZnkoZGF0YSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBhamF4UG9zdChcIi9hamF4L2FwaS9jYWxsXCIsYWpheF9kYXRhLGZ1bmN0aW9uKGRhdGEpe1xuICAgICAgICAgICAgICAgICAgICAgICAgc3Bpbm5lci5zdG9wKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YVsnc3RhdHVzJ10gPT0gJ3N1Y2Nlc3MnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGh0bWwgPSBzd2lnLnJlbmRlcihfdGhpcy50ZW1wbGF0ZXMuYW5zd2VySXRlbSwge2xvY2Fsczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYW5zd2VyOiBkYXRhWydyZXNwb25zZSddXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgJGFuc3dlckl0ZW0gPSAkKFwiLmFuc3dlci5pdGVtW2RhdGEtYW5zd2VyLWlkPSdcIiArIHVzZXJfYW5zd2VyX2lkICsgXCInXVwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkYW5zd2VySXRlbS5odG1sKGh0bWwpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghJGFuc3dlckl0ZW0uaXMoXCI6aW4tdmlld3BvcnRcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcImh0bWwsIGJvZHlcIikuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY3JvbGxUb3A6ICRhbnN3ZXJJdGVtLm9mZnNldCgpLnRvcFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCAzMDApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgX3RoaXMuaGFuZGxlVm90ZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICBcbiAgICAgICAgdmFyICR0YWJzID0gJCgnI3NlZ21lbnRfM19fc2VnbWVudF9xdWVzdGlvbnNfX3RhYnMnKTtcblxuICAgICAgICAkdGFicy5vbihcImNsaWNrXCIsXCIuaXRlbS5xdWVzdGlvbiBbZGF0YS1hY3Rpb249J3ZvdGUtcXVlc3Rpb24nXVwiLGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKTtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdmFyICRxdWVzdGlvbkl0ZW0gPSAkdGhpcy5jbG9zZXN0KFwiLml0ZW0ucXVlc3Rpb25cIik7XG4gICAgICAgICAgICB2YXIgcXVlc3Rpb25faWQgPSAkcXVlc3Rpb25JdGVtLmRhdGEoXCJpZFwiKTtcblxuICAgICAgICAgICAgdmFyIGFqYXhfZGF0YSA9IHsgXG4gICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwicG9zdFwiLFxuICAgICAgICAgICAgICAgIFwidXJsXCI6ICQoXCIjYXBpX3VybFwiKS52YWwoKSArIFwiL2FwaS92MS9xdWVzdGlvbi9cIiArIHF1ZXN0aW9uX2lkICsgXCIvdm90ZS9cIixcbiAgICAgICAgICAgICAgICBcImRhdGFcIjogSlNPTi5zdHJpbmdpZnkoe30pXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHZhciBzcGlubmVyID0gbmV3IFNwaW5uZXIoKS5zcGluKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdsb2FkaW5nX2ljb24nKSk7XG5cbiAgICAgICAgICAgIGFqYXhQb3N0KFwiL2FqYXgvYXBpL2NhbGxcIixhamF4X2RhdGEsZnVuY3Rpb24oZGF0YSl7XG4gICAgICAgICAgICAgICAgc3Bpbm5lci5zdG9wKCk7XG4gICAgICAgICAgICAgICAgaWYgKGRhdGFbJ3N0YXR1cyddID09ICdzdWNjZXNzJykge1xuICAgICAgICAgICAgICAgICAgICAkdGhpcy5hdHRyKFwiZGF0YS1hY3Rpb25cIixcInVudm90ZS1xdWVzdGlvblwiKTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGN1cnJlbnRfdm90ZSA9IHBhcnNlSW50KCR0aGlzLmZpbmQoXCJzcGFuLnZhbHVlXCIpLnRleHQoKSk7XG4gICAgICAgICAgICAgICAgICAgICR0aGlzLmZpbmQoXCJzcGFuLnZhbHVlXCIpLnRleHQoY3VycmVudF92b3RlKzEpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICBcbiAgICAgICAgfSlcblxuICAgICAgICAkdGFicy5vbihcImNsaWNrXCIsXCIuaXRlbS5hbnN3ZXIgW2RhdGEtYWN0aW9uPSd2b3RlLWFuc3dlciddXCIsZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpO1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB2YXIgJGFuc3dlckl0ZW0gPSAkdGhpcy5jbG9zZXN0KFwiLml0ZW0uYW5zd2VyXCIpO1xuICAgICAgICAgICAgdmFyIGFuc3dlcl9pZCA9ICRhbnN3ZXJJdGVtLmRhdGEoXCJpZFwiKTtcblxuICAgICAgICAgICAgdmFyIGFqYXhfZGF0YSA9IHsgXG4gICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwicG9zdFwiLFxuICAgICAgICAgICAgICAgIFwidXJsXCI6ICQoXCIjYXBpX3VybFwiKS52YWwoKSArIFwiL2FwaS92MS9hbnN3ZXIvXCIgKyBhbnN3ZXJfaWQgKyBcIi92b3RlL1wiLFxuICAgICAgICAgICAgICAgIFwiZGF0YVwiOiBKU09OLnN0cmluZ2lmeSh7fSlcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdmFyIHNwaW5uZXIgPSBuZXcgU3Bpbm5lcigpLnNwaW4oZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2xvYWRpbmdfaWNvbicpKTtcblxuICAgICAgICAgICAgYWpheFBvc3QoXCIvYWpheC9hcGkvY2FsbFwiLGFqYXhfZGF0YSxmdW5jdGlvbihkYXRhKXtcbiAgICAgICAgICAgICAgICBzcGlubmVyLnN0b3AoKTtcbiAgICAgICAgICAgICAgICBpZiAoZGF0YVsnc3RhdHVzJ10gPT0gJ3N1Y2Nlc3MnKSB7XG4gICAgICAgICAgICAgICAgICAgICR0aGlzLmF0dHIoXCJkYXRhLWFjdGlvblwiLFwidW52b3RlLWFuc3dlclwiKTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGN1cnJlbnRfdm90ZSA9IHBhcnNlSW50KCR0aGlzLmZpbmQoXCJzcGFuLnZhbHVlXCIpLnRleHQoKSk7XG4gICAgICAgICAgICAgICAgICAgICR0aGlzLmZpbmQoXCJzcGFuLnZhbHVlXCIpLnRleHQoY3VycmVudF92b3RlKzEpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICBcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBfdGhpcy5oYW5kbGVVbnZvdGUgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgJHRhYnMgPSAkKCcjc2VnbWVudF8zX19zZWdtZW50X3F1ZXN0aW9uc19fdGFicycpO1xuXG4gICAgICAgICR0YWJzLm9uKFwiY2xpY2tcIixcIi5pdGVtLnF1ZXN0aW9uIFtkYXRhLWFjdGlvbj0ndW52b3RlLXF1ZXN0aW9uJ11cIixmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidW52b3RlXCIpO1xuXG4gICAgICAgICAgICB2YXIgJHF1ZXN0aW9uSXRlbSA9ICR0aGlzLmNsb3Nlc3QoXCIuaXRlbS5xdWVzdGlvblwiKTtcbiAgICAgICAgICAgIHZhciBxdWVzdGlvbl9pZCA9ICRxdWVzdGlvbkl0ZW0uZGF0YShcImlkXCIpO1xuXG4gICAgICAgICAgICB2YXIgYWpheF9kYXRhID0geyBcbiAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJwb3N0XCIsXG4gICAgICAgICAgICAgICAgXCJ1cmxcIjogJChcIiNhcGlfdXJsXCIpLnZhbCgpICsgXCIvYXBpL3YxL3F1ZXN0aW9uL1wiICsgcXVlc3Rpb25faWQgKyBcIi91bnZvdGUvXCIsXG4gICAgICAgICAgICAgICAgXCJkYXRhXCI6IEpTT04uc3RyaW5naWZ5KHt9KVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgc3Bpbm5lciA9IG5ldyBTcGlubmVyKCkuc3Bpbihkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbG9hZGluZ19pY29uJykpO1xuXG4gICAgICAgICAgICBhamF4UG9zdChcIi9hamF4L2FwaS9jYWxsXCIsYWpheF9kYXRhLGZ1bmN0aW9uKGRhdGEpe1xuICAgICAgICAgICAgICAgIHNwaW5uZXIuc3RvcCgpO1xuICAgICAgICAgICAgICAgIGlmIChkYXRhWydzdGF0dXMnXSA9PSAnc3VjY2VzcycpIHtcbiAgICAgICAgICAgICAgICAgICAgJHRoaXMuYXR0cihcImRhdGEtYWN0aW9uXCIsXCJ2b3RlLXF1ZXN0aW9uXCIpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgY3VycmVudF92b3RlID0gcGFyc2VJbnQoJHRoaXMuZmluZChcInNwYW4udmFsdWVcIikudGV4dCgpKTtcbiAgICAgICAgICAgICAgICAgICAgJHRoaXMuZmluZChcInNwYW4udmFsdWVcIikudGV4dChjdXJyZW50X3ZvdGUtMSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSk7XG5cblxuICAgICAgICAkdGFicy5vbihcImNsaWNrXCIsXCIuaXRlbS5hbnN3ZXIgW2RhdGEtYWN0aW9uPSd1bnZvdGUtYW5zd2VyJ11cIixmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZhciAkYW5zd2VySXRlbSA9ICR0aGlzLmNsb3Nlc3QoXCIuaXRlbS5hbnN3ZXJcIik7XG4gICAgICAgICAgICB2YXIgYW5zd2VyX2lkID0gJGFuc3dlckl0ZW0uZGF0YShcImlkXCIpO1xuXG4gICAgICAgICAgICB2YXIgYWpheF9kYXRhID0geyBcbiAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJwb3N0XCIsXG4gICAgICAgICAgICAgICAgXCJ1cmxcIjogJChcIiNhcGlfdXJsXCIpLnZhbCgpICsgXCIvYXBpL3YxL2Fuc3dlci9cIiArIGFuc3dlcl9pZCArIFwiL3Vudm90ZS9cIixcbiAgICAgICAgICAgICAgICBcImRhdGFcIjogSlNPTi5zdHJpbmdpZnkoe30pXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHZhciBzcGlubmVyID0gbmV3IFNwaW5uZXIoKS5zcGluKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdsb2FkaW5nX2ljb24nKSk7XG5cbiAgICAgICAgICAgIGFqYXhQb3N0KFwiL2FqYXgvYXBpL2NhbGxcIixhamF4X2RhdGEsZnVuY3Rpb24oZGF0YSl7XG4gICAgICAgICAgICAgICAgc3Bpbm5lci5zdG9wKCk7XG4gICAgICAgICAgICAgICAgaWYgKGRhdGFbJ3N0YXR1cyddID09ICdzdWNjZXNzJykge1xuICAgICAgICAgICAgICAgICAgICAkdGhpcy5hdHRyKFwiZGF0YS1hY3Rpb25cIixcInZvdGUtYW5zd2VyXCIpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgY3VycmVudF92b3RlID0gcGFyc2VJbnQoJHRoaXMuZmluZChcInNwYW4udmFsdWVcIikudGV4dCgpKTtcbiAgICAgICAgICAgICAgICAgICAgJHRoaXMuZmluZChcInNwYW4udmFsdWVcIikudGV4dChjdXJyZW50X3ZvdGUtMSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIFxuICAgICAgICB9KVxuICAgIH1cblxuICAgIF90aGlzLmhhbmRsZUVkaXRQcm9maWxlID0gZnVuY3Rpb24oKXtcblxuICAgIH1cblxuICAgIF90aGlzLmhhbmRsZUZvbGxvdyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgJHRhYnMgPSAkKCcjc2VnbWVudF8zX19zZWdtZW50X3F1ZXN0aW9uc19fdGFicycpO1xuXG4gICAgICAgICR0YWJzLm9uKFwiY2xpY2tcIixcIi5pdGVtLnF1ZXN0aW9uIFtkYXRhLWFjdGlvbj0nZm9sbG93J11cIixmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZhciAkcXVlc3Rpb25JdGVtID0gJHRoaXMuY2xvc2VzdChcIi5pdGVtLnF1ZXN0aW9uXCIpO1xuICAgICAgICAgICAgdmFyIHF1ZXN0aW9uX2lkID0gJHF1ZXN0aW9uSXRlbS5kYXRhKFwiaWRcIik7XG5cbiAgICAgICAgICAgIHZhciBhamF4X2RhdGEgPSB7IFxuICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcInBvc3RcIixcbiAgICAgICAgICAgICAgICBcInVybFwiOiAkKFwiI2FwaV91cmxcIikudmFsKCkgKyBcIi9hcGkvdjEvdXNlci9mb2xsb3cvcXVlc3Rpb24vXCIgKyBxdWVzdGlvbl9pZCArIFwiL1wiLFxuICAgICAgICAgICAgICAgIFwiZGF0YVwiOiBKU09OLnN0cmluZ2lmeSh7fSlcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdmFyIHNwaW5uZXIgPSBuZXcgU3Bpbm5lcigpLnNwaW4oZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2xvYWRpbmdfaWNvbicpKTtcblxuICAgICAgICAgICAgYWpheFBvc3QoXCIvYWpheC9hcGkvY2FsbFwiLGFqYXhfZGF0YSxmdW5jdGlvbihkYXRhKXtcbiAgICAgICAgICAgICAgICBzcGlubmVyLnN0b3AoKTtcbiAgICAgICAgICAgICAgICBpZiAoZGF0YVsnc3RhdHVzJ10gPT0gJ3N1Y2Nlc3MnKSB7XG4gICAgICAgICAgICAgICAgICAgICR0aGlzLmF0dHIoXCJkYXRhLWFjdGlvblwiLFwidW5mb2xsb3dcIik7XG4gICAgICAgICAgICAgICAgICAgICR0aGlzLnRleHQoXCJGT0xMT1dFRFwiKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgXG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgX3RoaXMuaGFuZGxlVW5mb2xsb3cgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyICR0YWJzID0gJCgnI3NlZ21lbnRfM19fc2VnbWVudF9xdWVzdGlvbnNfX3RhYnMnKTtcblxuICAgICAgICAkdGFicy5vbihcImNsaWNrXCIsXCIuaXRlbS5xdWVzdGlvbiBbZGF0YS1hY3Rpb249J3VuZm9sbG93J11cIixmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZhciAkcXVlc3Rpb25JdGVtID0gJHRoaXMuY2xvc2VzdChcIi5pdGVtLnF1ZXN0aW9uXCIpO1xuICAgICAgICAgICAgdmFyIHF1ZXN0aW9uX2lkID0gJHF1ZXN0aW9uSXRlbS5kYXRhKFwiaWRcIik7XG5cbiAgICAgICAgICAgIHZhciBhamF4X2RhdGEgPSB7IFxuICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcInBvc3RcIixcbiAgICAgICAgICAgICAgICBcInVybFwiOiAkKFwiI2FwaV91cmxcIikudmFsKCkgKyBcIi9hcGkvdjEvdXNlci91bmZvbGxvdy9xdWVzdGlvbi9cIiArIHF1ZXN0aW9uX2lkICsgXCIvXCIsXG4gICAgICAgICAgICAgICAgXCJkYXRhXCI6IEpTT04uc3RyaW5naWZ5KHt9KVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgc3Bpbm5lciA9IG5ldyBTcGlubmVyKCkuc3Bpbihkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbG9hZGluZ19pY29uJykpO1xuXG4gICAgICAgICAgICBhamF4UG9zdChcIi9hamF4L2FwaS9jYWxsXCIsYWpheF9kYXRhLGZ1bmN0aW9uKGRhdGEpe1xuICAgICAgICAgICAgICAgIHNwaW5uZXIuc3RvcCgpO1xuICAgICAgICAgICAgICAgIGlmIChkYXRhWydzdGF0dXMnXSA9PSAnc3VjY2VzcycpIHtcbiAgICAgICAgICAgICAgICAgICAgJHRoaXMuYXR0cihcImRhdGEtYWN0aW9uXCIsXCJmb2xsb3dcIik7XG4gICAgICAgICAgICAgICAgICAgICR0aGlzLnRleHQoXCJGT0xMT1dcIik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIFxuICAgICAgICB9KVxuICAgIH1cblxuICAgIF90aGlzLmluaXQgPSBmdW5jdGlvbihkYXRhKXtcbiAgICBcdF90aGlzLmRhdGEgPSB7fTtcbiAgICAgICAgX3RoaXMudGVtcGxhdGVzID0ge307XG4gICAgICAgIF90aGlzLmhhbmRsZVRhYnMoKTtcbiAgICAgICAgX3RoaXMuaGFuZGxlRWRpdFByb2ZpbGUoKTtcbiAgICAgICAgX3RoaXMuaGFuZGxlQW5zd2VyKCk7XG4gICAgICAgIF90aGlzLmhhbmRsZVZvdGUoKTtcbiAgICAgICAgX3RoaXMuaGFuZGxlVW52b3RlKCk7XG4gICAgICAgIF90aGlzLmhhbmRsZUZvbGxvdygpO1xuICAgICAgICBfdGhpcy5oYW5kbGVVbmZvbGxvdygpO1xuICAgIH1cblxuICAgIF90aGlzLmRlc3Ryb3kgPSBmdW5jdGlvbigpe31cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBwYWdlRGFzaGJvYXJkOyIsIi8qKlxuICogQG1vZHVsZSBwYWdlSG9tZVxuICovXG52YXIgcGFnZUhvbWUgPSBmdW5jdGlvbihzYW5kYm94KXtcbiAgICB2YXIgX3RoaXMgPSB0aGlzO1xuXG4gICAgLyoqXG4gICAgICogQG1vZHVsZSBwYWdlSG9tZVxuICAgICAqIEBmdW5jdGlvbiBpbml0XG4gICAgICogQHBhcmFtIG9wdGlvbnNcbiAgICAgKi9cbiAgICBfdGhpcy5pbml0ID0gZnVuY3Rpb24ob3B0aW9ucyl7XG4gICAgICAgIF90aGlzLm9iamVjdHMgPSB7fTtcbiAgICAgICAgX3RoaXMub2JqZWN0cy4kc2VnbWVudDEgPSAkKCcjc2VnbWVudF8xJyk7XG4gICAgICAgIF90aGlzLm9iamVjdHMuJHNlZ21lbnQyID0gJCgnI3NlZ21lbnRfMicpO1xuICAgICAgICBfdGhpcy5vYmplY3RzLiRzZWdtZW50MVRvZ2dsZVNjcm9sbERvd24gPSAkKCcjc2VnbWVudF8xX190b2dnbGVfc2Nyb2xsX2Rvd24nKTtcblxuICAgICAgICBfdGhpcy5oYW5kbGVBc2tGb3JtKCk7XG4gICAgICAgIF90aGlzLmhhbmRsZVNlZ21lbnQxKCk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEBtb2R1bGUgcGFnZUhvbWVcbiAgICAgKiBAZnVuY3Rpb24gZGVzdHJveVxuICAgICAqL1xuICAgIF90aGlzLmRlc3Ryb3kgPSBmdW5jdGlvbigpe31cblxuICAgIC8qKlxuICAgICAqIEBtb2R1bGUgcGFnZUhvbWVcbiAgICAgKiBAZnVuY3Rpb24gaGFuZGxlQXNrRm9ybVxuICAgICAqL1xuICAgIF90aGlzLmhhbmRsZUFza0Zvcm0gPSBmdW5jdGlvbigpe1xuICAgICAgICBcbiAgICB9XG5cbiAgICBfdGhpcy5oYW5kbGVTZWdtZW50MSA9IGZ1bmN0aW9uKCl7XG4gICAgICAgIF90aGlzLm9iamVjdHMuJHNlZ21lbnQxLmZpbmQoJ1tkYXRhLXRvZ2dsZT1zY3JvbGxEb3duXScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAkKFwiaHRtbCwgYm9keVwiKS5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICBzY3JvbGxUb3A6IF90aGlzLm9iamVjdHMuJHNlZ21lbnQyLm9mZnNldCgpLnRvcFxuICAgICAgICAgICAgfSwgMzAwKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcmV0dXJuICh7XG4gICAgICAgIGluaXQ6IF90aGlzLmluaXQsXG4gICAgICAgIGRlc3Ryb3k6IF90aGlzLmRlc3Ryb3lcbiAgICB9KVxufTtcblxubW9kdWxlLmV4cG9ydHMgPSBwYWdlSG9tZTsiLCIndXNlIHN0cmljdCc7XG5cbnZhciBwYWdlUHJvZmlsZSA9IGZ1bmN0aW9uKHNhbmRib3gpe1xuICAgIHZhciBfdGhpcyA9IHRoaXM7XG5cbiAgICBfdGhpcy5oYW5kbGVUYWJzID0gZnVuY3Rpb24oKXtcbiAgICBcdHZhciAkbWVudSA9ICQoJyNzZWdtZW50XzJfX21lbnUnKSxcbiAgICAgICAgICAgICR0YWJzID0gJCgnI3NlZ21lbnRfM19fc2VnbWVudF9xdWVzdGlvbnNfX3RhYnMnKVxuICAgICAgICA7XG5cbiAgICAgICAgJG1lbnUub24oJ2NsaWNrJywgJz4uaXRlbScsIGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpLFxuICAgICAgICAgICAgICAgICR0YXJnZXQgPSAkKCR0aGlzLmRhdGEoJ3RhcmdldCcpKVxuICAgICAgICAgICAgO1xuXG4gICAgICAgICAgICAkdGhpc1xuICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnYWN0aXZlJylcbiAgICAgICAgICAgICAgICAuc2libGluZ3MoJy5hY3RpdmUnKVxuICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcygnYWN0aXZlJylcbiAgICAgICAgICAgIDtcblxuICAgICAgICAgICAgaWYoJHRhcmdldC5sZW5ndGgpe1xuICAgICAgICAgICAgICAgICR0YXJnZXRcbiAgICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKCdhY3RpdmUnKVxuICAgICAgICAgICAgICAgICAgICAuc2libGluZ3MoJy5hY3RpdmUnKVxuICAgICAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBfdGhpcy5pbml0ID0gZnVuY3Rpb24oZGF0YSl7XG4gICAgXHRfdGhpcy5kYXRhID0ge307XG4gICAgICAgIF90aGlzLnRlbXBsYXRlcyA9IHt9O1xuICAgICAgICBfdGhpcy5oYW5kbGVUYWJzKCk7XG4gICAgfVxuICAgIF90aGlzLmRlc3Ryb3kgPSBmdW5jdGlvbigpe31cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBwYWdlUHJvZmlsZTsiLCIndXNlIHN0cmljdCc7XG5cbnZhciBwYWdlUXVlc3Rpb25zID0gZnVuY3Rpb24oc2FuZGJveCl7XG4gICAgdmFyIF90aGlzID0gdGhpcztcblxuICAgIF90aGlzLmhhbmRsZUZvcm1TZWFyY2ggPSBmdW5jdGlvbigpe1xuICAgICAgICB2YXIgJGlucHV0ID0gJCgnI2Zvcm1fYXNrX3F1ZXN0aW9uX19pbnB1dCcpO1xuXG4gICAgICAgIHZhciBpbnB1dE1hZ2ljU3VnZ2VzdCA9ICRpbnB1dC5tYWdpY1N1Z2dlc3Qoe1xuICAgICAgICAgICAgZGF0YTogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogJ1F1ZXN0aW9uIDEnLFxuICAgICAgICAgICAgICAgICAgICBocmVmOiAnL3BhZ2VzL2NvbnRhY3QtdXMuaHRtbCdcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogJ1F1ZXN0aW9uIDInLFxuICAgICAgICAgICAgICAgICAgICBocmVmOiAnL3BhZ2VzL3F1ZXN0aW9uLmh0bWwnXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGhpZGVUcmlnZ2VyOiB0cnVlLFxuICAgICAgICAgICAgc2VsZWN0Rmlyc3Q6IHRydWUsXG4gICAgICAgICAgICBwbGFjZWhvbGRlcjogJycsXG4gICAgICAgICAgICByZW5kZXJlcjogZnVuY3Rpb24oZGF0YSl7XG4gICAgICAgICAgICAgICAgcmV0dXJuICc8YSBocmVmPVwiJytkYXRhLmhyZWYrJ1wiPicrZGF0YS5uYW1lKyc8L2E+JztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgJChpbnB1dE1hZ2ljU3VnZ2VzdCkub24oJ3NlbGVjdGlvbmNoYW5nZScsIGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICB2YXIgc2VsZWN0ZWRJdGVtcyA9IHRoaXMuZ2V0U2VsZWN0aW9uKCk7XG4gICAgICAgICAgICBpZihzZWxlY3RlZEl0ZW1zLmxlbmd0aCl7XG4gICAgICAgICAgICAgICAgdmFyIHNlbGVjdGVkSXRlbSA9IHNlbGVjdGVkSXRlbXNbMF07XG4gICAgICAgICAgICAgICAgaWYoc2VsZWN0ZWRJdGVtLmhyZWYpe1xuICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uYXNzaWduKHNlbGVjdGVkSXRlbS5ocmVmKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIF90aGlzLmhhbmRsZVRhYnMgPSBmdW5jdGlvbigpe1xuICAgICAgICBfdGhpcy50ZW1wbGF0ZXMudGFiID0gbXVsdGlsaW5lKGZ1bmN0aW9uKCl7LypcbiAgICAgICAgIDxkaXYgY2xhc3M9XCJsaXN0IHF1ZXN0aW9uc1wiPlxuICAgICAgICAgeyUgZm9yIHF1ZXN0aW9uIGluIHF1ZXN0aW9ucyAlfVxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIml0ZW0gcXVlc3Rpb25cIiBkYXRhLWlkPVwie3txdWVzdGlvbi5pZH19XCIgZGF0YS1pcy1leHBhbmRlZD1cIjBcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidW5pdnRvcCBjYXJkXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0aHVtYm5haWxcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVwie3txdWVzdGlvbi51c2VyX3Bvc3QuYXZhdGFyfX1cIiBhbHQ9XCJcIiB3aWR0aD1cIjcyXCIgaGVpZ2h0PVwiNzJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIvdXNlci97e3F1ZXN0aW9uLnVzZXJfcG9zdC51c2VybmFtZX19XCIgY2xhc3M9XCJtZXRhIG5hbWVcIj57e3F1ZXN0aW9uLnVzZXJfcG9zdC5maXJzdF9uYW1lfX0ge3txdWVzdGlvbi51c2VyX3Bvc3QubGFzdF9uYW1lfX08L2E+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29udGVudFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNlZ21lbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaGVhZGVyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhY3Rpb25zXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiI1wiIGRhdGEtYWN0aW9uPVwiZm9sbG93XCIgZGF0YS1xdWVzdGlvbi1pZD1cInt7cXVlc3Rpb24uaWR9fVwiIGRhdGEtaXMtZm9sbG93ZWQ9XCIwXCI+Rm9sbG93PC9hPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1ldGEgdGltZVwiPnt7cXVlc3Rpb24uY3JlYXRlX2RhdGV9fTwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidGFnc1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7JSBmb3IgdGFnIGluIHF1ZXN0aW9uLnRhZ3MgJX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwidGFnXCIgZGF0YS1pZD1cInt7dGFnLmlkfX1cIj57e3RhZy52YWx1ZX19PC9hPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7JSBlbmRmb3IgJX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYm9keVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3VtbWFyeVwiPnt7cXVlc3Rpb24udGl0bGV9fTwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZXh0cmEgdGV4dFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3txdWVzdGlvbi5jb250ZW50fX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHslIGlmIHF1ZXN0aW9uLmxhdGVzdF9hbnN3ZXIgJX08YSBocmVmPVwiI1wiIGRhdGEtYWN0aW9uPVwiZXhwYW5kXCI+bW9yZTwvYT57JSBlbmRpZiAlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj48IS0tIGVuZCAuYm9keSAtLT5cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb290ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJ1bml2dG9wIGxhYmVsXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cInVuaXZ0b3AgaWNvbiBoZWFydFwiPjwvaT43MFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJ1bml2dG9wIGxhYmVsXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cInVuaXZ0b3AgaWNvbiBjaGF0LWJ1YmJsZVwiPjwvaT43MFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+PCEtLSBlbmQgLmZvb3RlciAtLT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgeyUgaWYgcXVlc3Rpb24ubGF0ZXN0X2Fuc3dlciAlfVxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYW5zd2Vyc1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIml0ZW1cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidW5pdnRvcCBjYXJkXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0aHVtYm5haWxcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVwie3txdWVzdGlvbi51c2VyX3Bvc3QuYXZhdGFyfX1cIiBhbHQ9XCJcIiB3aWR0aD1cIjU2XCIgaGVpZ2h0PVwiNTZcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjXCIgY2xhc3M9XCJtZXRhIG5hbWVcIj57e3F1ZXN0aW9uLmxhdGVzdF9hbnN3ZXIudXNlcl9wb3N0X2Z1bGxuYW1lfX08L2E+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29udGVudFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNlZ21lbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYm9keVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3VtbWFyeVwiPnt7cXVlc3Rpb24ubGF0ZXN0X2Fuc3dlci5jb250ZW50fX08L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgeyUgZW5kaWYgJX1cblxuICAgICAgICAgICAgICAgICAgICA8Zm9ybSBjbGFzcz1cInVuaXZ0b3AgZm9ybVwiIGRhdGEtZm9ybT1cInJlcGx5XCIgZGF0YS1xdWVzdGlvbi1pZD1cInt7cXVlc3Rpb24uaWR9fVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlubGluZSBmaWVsZHNcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGQgYXZhdGFyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVwiLi4vYXNzZXRzL2ltYWdlcy9kZW1vL3BhZ2VzL3F1ZXN0aW9uL3NlZ21lbnQtMi5hdmF0YXItMS5wbmdcIiBhbHQ9XCJcIiB3aWR0aD1cIjcyXCIgaGVpZ2h0PVwiNzJcIi8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtZXRhIG5hbWVcIj5ZT1U8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGQgaW5wdXRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkc1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRleHRhcmVhIG5hbWU9XCJjb250ZW50XCIgcGxhY2Vob2xkZXI9XCJBZGQgeW91ciBhbnN3ZXJcIj48L3RleHRhcmVhPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGQgdGV4dC1yaWdodFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cInN1Ym1pdFwiIGNsYXNzPVwidW5pdnRvcCBidXR0b25cIj5TdWJtaXQ8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Zvcm0+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgeyUgZW5kZm9yICV9XG4gICAgICAgIDwvZGl2PlxuICAgICAgICAqL2NvbnNvbGUubG9nfSk7XG5cbiAgICAgICAgdmFyICRtZW51ID0gJCgnI3NlZ21lbnRfMl9fbWVudScpLFxuICAgICAgICAgICAgJHRhYnMgPSAkKCcjc2VnbWVudF8yX190YWJzJylcbiAgICAgICAgO1xuXG4gICAgICAgICRtZW51Lm9uKCdjbGljaycsICc+Lml0ZW0nLCBmdW5jdGlvbigpe1xuICAgICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKSxcbiAgICAgICAgICAgICAgICAkdGFyZ2V0ID0gJCgkdGhpcy5kYXRhKCd0YXJnZXQnKSlcbiAgICAgICAgICAgIDtcblxuICAgICAgICAgICAgJHRoaXNcbiAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICAgICAgICAgICAgLnNpYmxpbmdzKCcuYWN0aXZlJylcbiAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICAgICAgICA7XG5cbiAgICAgICAgICAgIGlmKCR0YXJnZXQubGVuZ3RoKXtcbiAgICAgICAgICAgICAgICAkdGFyZ2V0XG4gICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnYWN0aXZlJylcbiAgICAgICAgICAgICAgICAgICAgLnNpYmxpbmdzKCcuYWN0aXZlJylcbiAgICAgICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCdhY3RpdmUnKVxuICAgICAgICAgICAgICAgIDtcblxuICAgICAgICAgICAgICAgIHZhciBzcGlubmVyID0gbmV3IFNwaW5uZXIoKS5zcGluKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdsb2FkaW5nX2ljb24nKSk7XG5cbiAgICAgICAgICAgICAgICAvL2xvYWQgdGFiIGNvbnRlbnRcbiAgICAgICAgICAgICAgICAvL2dldCBkYXRhXG4gICAgICAgICAgICAgICAgc3dpdGNoKCR0YXJnZXQuZGF0YSgnbG9hZGluZy1zdGF0dXMnKSl7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ3N1Y2Nlc3MnOlxuICAgICAgICAgICAgICAgICAgICAgICAgLy9kbyBub3RoaW5nXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnbm90LXlldCc6XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2Vycm9yJzpcbiAgICAgICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgICAgIHNhbmRib3guZW1pdCgnYWpheC9yZXF1ZXN0Jywge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFqYXhPcHRpb25zOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybDogJChcIiNhcGlfdXJsXCIpLnZhbCgpICsgJy9hcGkvdjEvcXVlc3Rpb24/Zm9ybWF0PWpzb24nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiB7bGltaXQ6IDEwfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uKGRhdGEpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzcGlubmVyLnN0b3AoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYoJ3N1Y2Nlc3MnID09PSBkYXRhLnN0YXR1cyl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvL3JlbmRlclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGh0bWwgPSBzd2lnLnJlbmRlcihfdGhpcy50ZW1wbGF0ZXMudGFiLCB7bG9jYWxzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHF1ZXN0aW9uczogZGF0YS5yZXNwb25zZS5vYmplY3RzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkdGFyZ2V0Lmh0bWwoaHRtbCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0YXJnZXQuZGF0YSgnbG9hZGluZy1zdGF0dXMnLCAnc3VjY2VzcycpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHRhcmdldC5odG1sKFwiVGhlcmUncyBhbiBlcnJvciB3aGlsZSBmZXRjaGluZyB0aGUgY29udGVudFwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgICRtZW51LmNoaWxkcmVuKCkuZmlyc3QoKS50cmlnZ2VyKCdjbGljaycpO1xuICAgIH1cblxuICAgIF90aGlzLmhhbmRsZUFuc3dlciA9IGZ1bmN0aW9uKCl7XG5cbiAgICAgICAgX3RoaXMudGVtcGxhdGVzLmFuc3dlckl0ZW0gPSBtdWx0aWxpbmUoZnVuY3Rpb24oKXsvKlxuICAgICAgICA8ZGl2IGNsYXNzPVwiaXRlbSBhbnN3ZXJcIiBkYXRhLWFuc3dlci1pZD1cInt7YW5zd2VyX2lkfX1cIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ1bml2dG9wIGNhcmRcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidGh1bWJuYWlsXCI+XG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPSd7e2Fuc3dlci51c2VyX3Bvc3QuYXZhdGFyfX0nIHdpZHRoPSc1NicgaGVpZ2h0PSc1NicvPlxuICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVwiI1wiIGNsYXNzPVwibWV0YSBuYW1lXCI+e3thbnN3ZXIudXNlcl9wb3N0LmZ1bGxuYW1lfX08L2E+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbnRlbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNlZ21lbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJib2R5XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInN1bW1hcnlcIj57e2Fuc3dlci5jb250ZW50fX08L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgKi9jb25zb2xlLmxvZ30pO1xuXG4gICAgICAgIHZhciAkdGFicyA9ICQoJyNzZWdtZW50XzNfX3NlZ21lbnRfcXVlc3Rpb25zX190YWJzJyk7XG5cbiAgICAgICAgJHRhYnMuZmluZChcIi51bml2dG9wLmZvcm0uYW5zd2VyXCIpLm9uKFwic3VibWl0XCIsZnVuY3Rpb24oZXZlbnQpe1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKSxcbiAgICAgICAgICAgICAgICAkY29udGVudCA9ICR0aGlzLmZpbmQoXCJ0ZXh0YXJlYVtuYW1lPSdjb250ZW50J11cIikudmFsKCk7XG5cbiAgICAgICAgICAgIHZhciB1c2VyX2Fuc3dlcl9pZCA9ICR0aGlzLmRhdGEoXCJhbnN3ZXItaWRcIik7XG5cbiAgICAgICAgICAgIGlmICgkY29udGVudC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICB2YXIgc3Bpbm5lciA9IG5ldyBTcGlubmVyKCkuc3Bpbihkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbG9hZGluZ19pY29uJykpO1xuICAgICAgICAgICAgICAgIHZhciBxdWVzdGlvbl9pZCA9ICR0aGlzLmF0dHIoXCJkYXRhLXF1ZXN0aW9uLWlkXCIpO1xuICAgICAgICAgICAgICAgIHZhciBkYXRhID0ge1xuICAgICAgICAgICAgICAgICAgICBcImNvbnRlbnRcIjogJGNvbnRlbnRcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoaXNVbmRlZmluZWQodXNlcl9hbnN3ZXJfaWQpKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBhamF4X2RhdGEgPSB7IFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwicG9zdFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJ1cmxcIjogJChcIiNhcGlfdXJsXCIpLnZhbCgpICsgJy9hcGkvdjEvcXVlc3Rpb24vJyArIHF1ZXN0aW9uX2lkICsgXCIvYWRkX2Fuc3dlci9cIixcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZGF0YVwiOiBKU09OLnN0cmluZ2lmeShkYXRhKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGFqYXhQb3N0KFwiL2FqYXgvYXBpL2NhbGxcIixhamF4X2RhdGEsZnVuY3Rpb24oZGF0YSl7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgaHRtbCA9IHN3aWcucmVuZGVyKF90aGlzLnRlbXBsYXRlcy5hbnN3ZXJJdGVtLCB7bG9jYWxzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFuc3dlcjogZGF0YVsncmVzcG9uc2UnXVsnYW5zd2VyJ10sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFuc3dlcl9pZDogZGF0YVsncmVzcG9uc2UnXVsnYW5zd2VyX2lkJ11cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciAkYW5zd2Vyc0xpc3QgPSAkdGhpcy5wYXJlbnQoKS5maW5kKFwiLmxpc3QuYW5zd2Vyc1wiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICRhbnN3ZXJzTGlzdC5hcHBlbmQoaHRtbCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAkdGhpcy5hdHRyKFwiZGF0YS1hbnN3ZXItaWRcIixkYXRhWydyZXNwb25zZSddWydhbnN3ZXJfaWQnXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBzcGlubmVyLnN0b3AoKTtcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB2YXIgYWpheF9kYXRhID0geyBcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcInBhdGNoXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcInVybFwiOiAkKFwiI2FwaV91cmxcIikudmFsKCkgKyAnL2FwaS92MS9hbnN3ZXIvJyArIHVzZXJfYW5zd2VyX2lkICsgXCIvXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcImRhdGFcIjogSlNPTi5zdHJpbmdpZnkoZGF0YSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBhamF4UG9zdChcIi9hamF4L2FwaS9jYWxsXCIsYWpheF9kYXRhLGZ1bmN0aW9uKGRhdGEpe1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyICRhbnN3ZXJJdGVtID0gJChcIi5hbnN3ZXIuaXRlbVtkYXRhLWFuc3dlci1pZD0nXCIgKyB1c2VyX2Fuc3dlcl9pZCArIFwiJ11cIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAkYW5zd2VySXRlbS5maW5kKFwiLnN1bW1hcnlcIikuaHRtbChkYXRhWydyZXNwb25zZSddWydjb250ZW50J10pXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoISRhbnN3ZXJJdGVtLmlzKFwiOmluLXZpZXdwb3J0XCIpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcImh0bWwsIGJvZHlcIikuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjcm9sbFRvcDogJGFuc3dlckl0ZW0ub2Zmc2V0KCkudG9wXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgMzAwKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gIFxuICAgICAgICAgICAgICAgICAgICAgICAgc3Bpbm5lci5zdG9wKCk7XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBfdGhpcy5oYW5kbGVWb3RlID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciAkdGFicyA9ICQoJyNzZWdtZW50XzNfX3NlZ21lbnRfcXVlc3Rpb25zX190YWJzJyk7XG5cbiAgICAgICAgJHRhYnMub24oXCJjbGlja1wiLFwiLnVuaXZ0b3AuaWNvbi5oZWFydFwiLGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgX3RoaXMuaW5pdCA9IGZ1bmN0aW9uKGRhdGEpe1xuICAgICAgICBfdGhpcy5kYXRhID0ge307XG4gICAgICAgIF90aGlzLnRlbXBsYXRlcyA9IHt9O1xuXG4gICAgICAgIF90aGlzLmhhbmRsZUZvcm1TZWFyY2goKTtcbiAgICAgICAgX3RoaXMuaGFuZGxlVGFicygpO1xuICAgICAgICBfdGhpcy5oYW5kbGVBbnN3ZXIoKTtcbiAgICAgICAgX3RoaXMuaGFuZGxlVm90ZSgpO1xuICAgIH1cbiAgICBfdGhpcy5kZXN0cm95ID0gZnVuY3Rpb24oKXt9XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBpbml0OiBfdGhpcy5pbml0LFxuICAgICAgICBkZXN0cm95OiBfdGhpcy5kZXN0cm95XG4gICAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHBhZ2VRdWVzdGlvbnM7Il19
