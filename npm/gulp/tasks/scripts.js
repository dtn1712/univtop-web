var gulp = require('../gulp.js'),
    gutil = require('gulp-util'),
    watch = require('gulp-watch'),
    gulpif = require('gulp-if'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    mainBowerFiles = require('main-bower-files'),
//addsrc = require('gulp-add-src'),
    size = require('gulp-size'),
//filelog = require('gulp-filelog'),
//buffer = require('gulp-buffer'),
    fs = require('fs'),
    source = require('vinyl-source-stream'),
    browserify = require('browserify'),

    glob = require('glob-all'),
    es = require('event-stream'),
    notifier = require('node-notifier'),
    shell = require('gulp-shell')
    ;

gulp.task('scripts:vendors:compile', function () {
    return gulp
        .src(mainBowerFiles({
            filter: ["**/*.js"]
        }))
        //.pipe(size())
        .pipe(uglify())
        .pipe(concat('vendors.js'))
        .pipe(gulp.dest(config.paths.relative.buildScripts))
        //.pipe(size())
        ;
});

gulp.task('scripts:watch', function () {
    return gulp
        .watch(config.paths.relative.sourceScripts + '/**/*', ['scripts:compile'])
        ;
});

gulp.task('scripts:compile', function () {
    return gulp
        .src(config.paths.relative.sourceScripts + '/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest(config.paths.relative.buildScripts))
        ;
});

gulp.task('scripts:jsdoc', shell.task('jsdoc ' + config.paths.absolute.sourceStyles + '/style.less -c jsdoc.configuration.json --verbose'));

gulp.task('scripts:compile', function functionScriptsCompile(done) {
    glob(config.paths.relative.sourceScripts + '/*.js', function (error, files) {
        if (error) done(error);
        console.log(files);
        var tasks = files.map(function (entry) {
            return browserify({entries: [entry], debug: true})
                .bundle()
                .on('error', function (error) {
                    notifier.notify({message: 'Error: ' + error.message});
                    gutil.log(gutil.colors.red('Error'), error.message);
                    this.emit('end');
                })
                .pipe(source(entry))
                .pipe(rename({
                    dirname: '',
                    extname: '.js'
                }))
                .pipe(gulp.dest(config.paths.relative.buildScripts))
                ;
        });
        es.merge(tasks).on('end', done);
    });
});