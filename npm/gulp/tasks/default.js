var gulp = require('../gulp.js')
    ;

gulp.task('default', function () {
    gulp.start('build');
    gulp.start('server:start');
    gulp.start('watch');
});

gulp.task('build', function () {
    gulp.start('templates:compile');
    gulp.start('styles:compile');
    gulp.start('scripts:compile');
});

gulp.task('watch', function () {
    gulp.start('templates:watch');
    gulp.start('styles:watch');
    gulp.start('scripts:watch');
});