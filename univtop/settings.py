"""Common settings and globals."""
from __future__ import absolute_import

import os

from datetime import timedelta
from os.path import abspath, basename, dirname, join, normpath
from os import environ
from sys import path

from django.utils.translation import ugettext_lazy as _

import djcelery
djcelery.setup_loader()

ROOT_PATH = os.path.dirname(__file__)
path.append(ROOT_PATH)

SITE_NAME = os.path.basename(ROOT_PATH)
SITE_DOMAIN = SITE_NAME + ".com"

DEBUG = False

########## MANAGER CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    ('Dang Nguyen', 'teamunivtop@gmail.com'),
)

ADMINS_USERNAME = (
    "dtn29",
    "dtn1712",
)
# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS
########## END MANAGER CONFIGURATION

SENDGRID_API_KEY = environ.get("SENDGRID_API_KEY")
DEFAULT_FROM_EMAIL = environ.get("DEFAULT_FROM_EMAIL")

EMAIL_BACKEND = "sgbackend.SendGridBackend"
EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_HOST_USER = environ.get("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = environ.get('EMAIL_HOST_PASSWORD')

EMAIL_PORT = 587
EMAIL_USE_TLS = True

########## GENERAL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#time-zone
TIME_ZONE = 'America/Los_Angeles'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'en-us'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True
########## END GENERAL CONFIGURATION


########## MEDIA CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = os.path.join(ROOT_PATH, 'assets/media')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'
########## END MEDIA CONFIGURATION


########## STATIC FILE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = 'staticfiles'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    os.path.join(ROOT_PATH, 'assets/static'),
)

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
########## END STATIC FILE CONFIGURATION


########## SECRET CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = "chbrc3p7q%g9e80a(&bm$ci6ygdc_ak99q6ep90!#evq7yc@@@"

# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
    os.path.join(ROOT_PATH, 'fixtures'),
)
########## END FIXTURE CONFIGURATION

DEFAULT_TEMPLATE_DIRS = [
    os.path.join(ROOT_PATH, 'assets/templates'),
    os.path.join(ROOT_PATH, 'assets/templates/sites'),
    os.path.join(ROOT_PATH, 'assets/templates/common'),
    os.path.join(ROOT_PATH, 'assets/templates/sites/non_responsive'),
    os.path.join(ROOT_PATH, 'assets/templates/sites/non_responsive/apps'),
    os.path.join(ROOT_PATH, 'assets/templates/sites/non_responsive/apps/auth'),
    os.path.join(ROOT_PATH, 'assets/templates/sites/responsive'),
    os.path.join(ROOT_PATH, 'assets/templates/sites/responsive/apps'),
    os.path.join(ROOT_PATH, 'assets/templates/sites/responsive/apps/auth'),
]

DEFAULT_TEMPLATE_CONTEXT_PROCESSOR = [
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.request',
    'django_mobile.context_processors.flavour',
    "univtop.apps.main.context_processors.site_data",
    "univtop.apps.main.context_processors.global_data",
]

DEFAULT_TEMPLATE_EXTENSIONS = [
    "jinja2.ext.do",
    "jinja2.ext.loopcontrols",
    "jinja2.ext.with_",
    "jinja2.ext.i18n",
    "jinja2.ext.autoescape",
    "django_jinja.builtins.extensions.CsrfExtension",
    "django_jinja.builtins.extensions.CacheExtension",
    "django_jinja.builtins.extensions.TimezoneExtension",
    "django_jinja.builtins.extensions.UrlsExtension",
    "django_jinja.builtins.extensions.StaticFilesExtension",
    "django_jinja.builtins.extensions.DjangoFiltersExtension",
]

DEFAULT_TEMPLATE_FILTERS = {
    "shorten_content": "univtop.apps.main.templatetags.filters.shorten_content",
    "get_dictionary_item_value": "univtop.apps.main.templatetags.filters.get_dictionary_item_value",
    "display_elapse_time": "univtop.apps.main.templatetags.filters.display_elapse_time",
    "words_first_char_upper": "univtop.apps.main.templatetags.filters.words_first_char_upper",      
    "first_word_only": "univtop.apps.main.templatetags.filters.first_word_only",
    "first_char_only": "univtop.apps.main.templatetags.filters.first_char_only",
    'render_photo': "univtop.apps.main.templatetags.filters.render_photo",
}

DEFAULT_TEMPLATE_GLOBALS = {
    "load_css": "univtop.apps.main.templatetags.static_loader.load_css",
    "load_js": "univtop.apps.main.templatetags.static_loader.load_js",
    "get_image_url": "univtop.apps.main.templatetags.global_functions.get_image_url",
}

DEFAULT_TEMPLATE_FILE_EXTENSION = ".jinja.html"

TEMPLATES = [
    {
        "NAME": "jinja",
        "BACKEND": "django_jinja.backend.Jinja2",
        'DIRS': DEFAULT_TEMPLATE_DIRS,
        "OPTIONS": {
            "match_extension": DEFAULT_TEMPLATE_FILE_EXTENSION,
            "match_regex": r"^(?!admin/|!debug_toolbar/).*",
            "newstyle_gettext": True,
            'context_processors': DEFAULT_TEMPLATE_CONTEXT_PROCESSOR,
            "filters": DEFAULT_TEMPLATE_FILTERS,
            "globals": DEFAULT_TEMPLATE_GLOBALS,
            "extensions": DEFAULT_TEMPLATE_EXTENSIONS,
            "bytecode_cache": {
                "name": "default",
                "backend": "django_jinja.cache.BytecodeCache",
                "enabled": False,
            },
            "autoescape": False,
            "translation_engine": "django.utils.translation",
        }
    },
    {
        "NAME": "django",
        "BACKEND": 'django.template.backends.django.DjangoTemplates',
        'DIRS': DEFAULT_TEMPLATE_DIRS,
        'OPTIONS': {
            'context_processors': DEFAULT_TEMPLATE_CONTEXT_PROCESSOR,
            "loaders": [
                ('django_mobile.loader.CachedLoader', [
                      'django_mobile.loader.Loader',
                      'django.template.loaders.filesystem.Loader',
                      'django.template.loaders.app_directories.Loader',
                ]),
            ]
        },
    },
]


ROOT_URLCONF = '%s.urls' % SITE_NAME


DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    #"django.contrib.gis",

    # Useful template tags:
    'django.contrib.humanize',

    # Admin panel and documentation:
    'django.contrib.admin',
    #'django.contrib.admindocs',
)

THIRD_PARTY_APPS = (

    # Allauth
    # 'allauth',
    # 'allauth.account',
    # 'allauth.socialaccount',
    # 'allauth.socialaccount.providers.facebook',

    # Search
    # "haystack",

    # Asynchronous task queue:
    'djcelery',

    # Metrics
    "app_metrics",

    # Others'
    'django_ajax',
    "django_jinja",
    "django_redis",
    'django_mobile',
    'crispy_forms',
    'tastypie',
    "kombu",
    'kombu.transport.django',
    "timelog",

    "imagekit",
    "cloudinary",
)

LOCAL_APPS = (
    "univtop.apps.about",
    "univtop.apps.account",
    "univtop.apps.ajax",
    "univtop.apps.answer",
    "univtop.apps.main",
    "univtop.apps.member",
    "univtop.apps.notification",
    "univtop.apps.question",
    "univtop.apps.search",
)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

TIMELOG_LOG = os.path.join(ROOT_PATH,"logs","timelog.log")

SITE_LOG_HANDLER = "logfile_" + SITE_NAME

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
        'plain': {
            'format': '%(asctime)s %(message)s'
        },
    },
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
    },
    'handlers': {
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'filters': ['require_debug_true'],
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
            'formatter': 'verbose'
        },
        SITE_LOG_HANDLER: {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename':  os.path.join(ROOT_PATH,"logs",SITE_NAME + ".log"),
            'maxBytes': 1024 * 1024 * 5,  # 5 MB
            'backupCount': 5,
            'formatter': 'verbose'
        },
        'logfile_request': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename':  os.path.join(ROOT_PATH, "logs/request.log"),
            'maxBytes': 1024 * 1024 * 5,  # 5 MB
            'backupCount': 5,
            'formatter': 'verbose'
        },
        'timelog': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': TIMELOG_LOG,
            'maxBytes': 1024 * 1024 * 5,  # 5 MB
            'backupCount': 5,
            'formatter': 'plain',
        },

    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
        },
        'django.request': {
            'handlers': ['logfile_request'],
            'level': 'ERROR',
            'propagate': False,
        },
        SITE_NAME: {
            'handlers': [SITE_LOG_HANDLER],
            'level': 'DEBUG',
            'propogate': True,
        },
        'timelog.middleware': {
            'handlers': ['timelog'],
            'level': 'DEBUG',
            'propogate': False,
        }
    },
}


########## CELERY CONFIGURATION
# See: http://celery.readthedocs.org/en/latest/configuration.html#celery-task-result-expires
CELERY_TASK_RESULT_EXPIRES = timedelta(minutes=30)

# See: http://docs.celeryproject.org/en/master/configuration.html#std:setting-CELERY_CHORD_PROPAGATES
CELERY_CHORD_PROPAGATES = True

CELERY_TIMEZONE = 'UTC'

CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'

CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'msgpack', 'yaml']
########## END CELERY CONFIGURATION


########## WSGI CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'wsgi.application'
########## END WSGI CONFIGURATION


AUTH_PROFILE_MODULE = 'main.UserProfile'

SERIALIZATION_MODULES = {
    'json': "django.core.serializers.json",
}

AUTHENTICATION_BACKENDS = (
    'univtop.apps.account.backends.AuthenticationBackend',
    'django.contrib.auth.backends.ModelBackend',
)

ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_PASSWORD_MIN_LENGTH = 4
ACCOUNT_EMAIL_VERIFICATION = "none"
ACCOUNT_LOGOUT_ON_GET = True

#HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
#HAYSTACK_DEFAULT_OPERATOR = 'OR'

LOGIN_URL = '/account/login'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'

ACCOUNT_USERNAME_BLACKLIST = [
    'admin','signup','login','password',"accounts"
    'logout','confirm_email','search','settings',
    'buzz','messages',"about",'api','asset','photo',
    'feeds','friends'
]

GEOIP_PATH = os.path.join(ROOT_PATH, "db/geolocation")  ### path for geoip dat

GEOIP_DATABASE = os.path.join(ROOT_PATH, "db/geolocation/GeoLiteCity.dat")

OW_LY_API_KEY = "6sv891CJpDcuiz8eyRHfy"

CRISPY_TEMPLATE_PACK = 'bootstrap3'

APPEND_SLASH = False
TASTYPIE_ALLOW_MISSING_SLASH = True

API_LIMIT_PER_PAGE = 100

KEY_PREFIX = SITE_NAME

TIMELOG_IGNORE_URIS = (
    '^/admin/',         # Ignores all URIs beginning with '/admin/'
    '.jpg$',            # Ignores all URIs ending in .jpg
    '.js$',            # Ignores all URIs ending in .js
    '.css$',            # Ignores all URIs ending in .css
)

DEFAULT_APP_NAME = "main"

CACHE_KEY_SPECIAL_SEPARATOR = ":::"

import cloudinary
cloudinary.config( 
    cloud_name = environ.get("CLOUDINARY_CLOUD_NAME"), 
    api_key = environ.get("CLOUDINARY_API_KEY"), 
    api_secret = environ.get("CLOUDINARY_API_SECRET"),
    cdn_subdomain = True,
    secure = True
)

MAX_IMAGE_WIDTH = 900
MAX_IMAGE_HEIGHT = 600
DEFAULT_IMAGE_WIDTH = 600
DEFAULT_IMAGE_HEIGHT = 400

from django.contrib.gis.geoip import GeoIP
geoip = GeoIP()

FACEBOOK_PAGE_ID = "928956663860935"
IOS_APP_ID = "1054877132"




STAGE = "dev"

DEBUG = True

WEBSITE_URL = "http://localhost:8001"

API_URL = 'http://localhost:8000'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(ROOT_PATH, 'db/dev/univtop.db'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    } 
}


########## CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        'TIMEOUT': 60,
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "SOCKET_TIMEOUT": 5,
        }
    }
}
########## END CACHE CONFIGURATION


########## CELERY CONFIGURATION
# See: http://docs.celeryq.org/en/latest/configuration.html#celery-always-eager
#CELERY_ALWAYS_EAGER = True

# See: http://docs.celeryproject.org/en/latest/configuration.html#celery-eager-propagates-exceptions
#CELERY_EAGER_PROPAGATES_EXCEPTIONS = True

BROKER_URL = 'redis://127.0.0.1:6379/0'
CELERY_RESULT_BACKEND = 'redis://127.0.0.1:6379/0'

########## END CELERY CONFIGURATION


########## TOOLBAR CONFIGURATION
# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
INSTALLED_APPS += (
    'debug_toolbar',
)

# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
INTERNAL_IPS = ('127.0.0.1',)

# HAYSTACK_CONNECTIONS = {
#     'default': {
#         'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
#         'URL': 'http://127.0.0.1:9200',
#         'INDEX_NAME': 'documents',
#         'TIMEOUT': 60 * 5,
#     },
# }

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}
########## END TOOLBAR CONFIGURATION


DEBUG_TOOLBAR_CONFIG = {
    'JQUERY_URL':'',
}

DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
]
########## END TOOLBAR CONFIGURATION

STATIC_URL = '/static/'


MIDDLEWARE_CLASSES = (
  'timelog.middleware.TimeLogMiddleware',
  'debug_toolbar.middleware.DebugToolbarMiddleware',

  'django.middleware.security.SecurityMiddleware',
  'django.middleware.cache.UpdateCacheMiddleware',
  'django.contrib.sessions.middleware.SessionMiddleware',
  'django.middleware.common.CommonMiddleware',

  'django_mobile.middleware.MobileDetectionMiddleware',
  'django_mobile.middleware.SetFlavourMiddleware',
  "django_mobile.cache.middleware.CacheFlavourMiddleware",

  'django.middleware.csrf.CsrfViewMiddleware',
  'django.contrib.auth.middleware.AuthenticationMiddleware',
  'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
  'django.contrib.messages.middleware.MessageMiddleware',

  'django.middleware.cache.FetchFromCacheMiddleware',
)

DEFAULT_FROM_EMAIL = "teamunivtop@gmail.com"
SENDGRID_API_KEY = "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"

import cloudinary
cloudinary.config( 
    cloud_name = "dlnrgtqqv", 
    api_key = "552849736355724", 
    api_secret = "j3lS-0qhDKK-u-ozeJg2D3X9kNI",
    cdn_subdomain = True,
    secure = True
)

CACHE_MIDDLEWARE_SECONDS = 120

CSS_STATIC_URL_PATH_REPLACEMENT = {
  
}

BUILD_VERSION_ID = 'yaEeNYU2FrbzCjXUEe83eb'
