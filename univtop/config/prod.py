"""Production settings and globals."""

from os import environ

STAGE = "prod"

WEBSITE_URL = "http://univtop.com"

API_URL = 'http://development.univtop.com'

MIDDLEWARE_CLASSES = (

    'django.middleware.security.SecurityMiddleware',
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.gzip.GZipMiddleware',  # Gzip has to be above html minify
    # 'htmlmin.middleware.HtmlMinifyMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',

    'django_mobile.middleware.MobileDetectionMiddleware',
    'django_mobile.middleware.SetFlavourMiddleware',
    "django_mobile.cache.middleware.CacheFlavourMiddleware",

    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',

    'django.middleware.cache.FetchFromCacheMiddleware',
    
)

# HTML_MINIFY = False

DEBUG = False

DATABASES = {
   'default': {
       'ENGINE': 'django.db.backends.postgresql_psycopg2',
       'NAME':  environ.get("DATABASE_NAME"),
       'USER':  environ.get("DATABASE_USER"),
       'PASSWORD': environ.get("DATABASE_PASSWORD"),
       'HOST': environ.get("DATABASE_HOST"),
       'PORT': environ.get("DATABASE_PORT"),
   }
}


CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": environ.get("CACHE_LOCATION",'redis://127.0.0.1:6379/1'),
        'TIMEOUT': 60,
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "SOCKET_TIMEOUT": 5,
        }
    }
}


BROKER_URL = environ.get("BROKER_URL",'redis://127.0.0.1:6379/0')
CELERY_RESULT_BACKEND = environ.get("CELERY_RESULT_BACKEND",'redis://127.0.0.1:6379/0')


########## STORAGE CONFIGURATION
# See: http://django-storages.readthedocs.org/en/latest/index.html
INSTALLED_APPS += (
    'storages',
)

# See: http://django-storages.readthedocs.org/en/latest/backends/amazon-S3.html#settings
STATICFILES_STORAGE = DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

# See: http://django-storages.readthedocs.org/en/latest/backends/amazon-S3.html#settings
AWS_IS_GZIPPED = True

# See: http://django-storages.readthedocs.org/en/latest/backends/amazon-S3.html#settings
AWS_ACCESS_KEY_ID = environ.get("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = environ.get("AWS_SECRET_ACCESS_KEY")
AWS_STORAGE_BUCKET_NAME = environ.get("AWS_STORAGE_BUCKET_NAME")
AWS_AUTO_CREATE_BUCKET = True
AWS_QUERYSTRING_AUTH = False

# AWS cache settings, don't change unless you know what you're doing:
AWS_EXPIRY = 60 * 60 * 24 * 7
AWS_HEADERS = {
    'Cache-Control': 'max-age=%d, s-maxage=%d, must-revalidate' % (AWS_EXPIRY,
        AWS_EXPIRY)
}

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = environ.get("STATIC_URL")
########## END STORAGE CONFIGURATION


# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

########## ALLOWED HOSTS CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']
########## END ALLOWED HOST CONFIGURATION


CACHE_MIDDLEWARE_SECONDS = 120

import cloudinary
cloudinary.config( 
    cloud_name = environ.get("CLOUDINARY_NAME"), 
    api_key = environ.get("CLOUDINARY_API_KEY"), 
    api_secret = environ.get("CLOUDINARY_API_SECRET"),
    cdn_subdomain = True,
    secure = True
)

CSS_STATIC_URL_PATH_REPLACEMENT = {
    "https://d1aiy1qufjcdnj.cloudfront.net/": STATIC_URL,
    "../../resources/": STATIC_URL + "css/prod/"
}
