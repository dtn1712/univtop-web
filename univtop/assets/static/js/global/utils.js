function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)  {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
    return null;
}


function isNumber (o) {
  return !isNaN(parseFloat(o));
}

function isEmpty(el) {
    if(($(el).val().length==0) || ($(el).val()==null)) {
      return true;
    }
    return false
}

function isExist(el) {
    if ($(el).length > 0){
      return true
    }
    return false
}

function isUndefined(el) {
  if (typeof el === "undefined") return true;
  return false;
}

function isVisible(el) {
  if ($(el).hasClass("hide")) return false;
  return true;
}