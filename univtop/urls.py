from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.views.generic import RedirectView

from univtop import settings 


urlpatterns = patterns('',
    # Admin URL
    url(r'^admin/', include(admin.site.urls)),

    # Auth
    url(r'^account/', include("univtop.apps.account.urls")),
    # url(r'^oauth2/', include('provider.oauth2.urls', namespace = 'oauth2')),

    # Haystack app URL
    url(r'^search/', include("univtop.apps.search.urls")),

    url(r'^ajax/', include("univtop.apps.ajax.urls")),

    # univtop URL
    url(r'^about/', include('univtop.apps.about.urls')),
    url(r'^question/', include('univtop.apps.question.urls')),
    url(r'^notification/',include('univtop.apps.notification.urls')),

    url(r'^user/',include('univtop.apps.member.urls')),

    # Media URL
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.MEDIA_ROOT}),

    url(r'^i18n/', include('django.conf.urls.i18n')),

    url(r'^$', include('univtop.apps.main.urls')),

)


handler404 = "univtop.apps.main.views.handler404"
handler500 = "univtop.apps.main.views.handler500"