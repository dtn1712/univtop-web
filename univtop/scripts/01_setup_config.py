import sys, shutil, os, shortuuid

PROJECT_PATH = "univtop"

FILE_NAME = "env_variable"

def setupConfig(stage,build_version_id):
    common = PROJECT_PATH + "/config/common.py"
    shutil.copy(common,PROJECT_PATH + "/settings.py")
    stage_file = open(PROJECT_PATH + "/config/" + stage + ".py","r")
    dest_file = open(PROJECT_PATH + "/settings.py","a+")
    dest_file.write("\n\n\n")
    for line in stage_file:
        dest_file.write(line)
    dest_file.write("\nBUILD_VERSION_ID = '" + build_version_id  + "'\n")

def injectEnvironmentVariable(stage):
    file_name = FILE_NAME + "_" + stage + ".txt"
    bucket = PROJECT_PATH + "-" + stage

    if os.path.exists(file_name):
        os.system("rm " + file_name)

    access_key=raw_input("Please enter the AWS Access Key:")
    secret_key=raw_input("Please enter the AWS Secret Key:")

    sign = "--access_key=" + access_key + " " + "--secret_key=" + secret_key

    cmd = "s3cmd " + sign + " get s3://" + bucket + "/doc/" + file_name + " " + file_name
    os.system(cmd)

    environment = {}
    f1 = open(file_name,"r")
    for line in f1:
        data = line.replace("\n","").split("=")
        environment[data[0]] = data[1]

    environment["AWS_ACCESS_KEY_ID"] = access_key
    environment["AWS_SECRET_ACCESS_KEY"] = secret_key
    environment["AWS_STORAGE_BUCKET_NAME"] = bucket

    f1.close()

    os.system("rm " + file_name)

    f2 = open(PROJECT_PATH + "/settings.py","r")
    filedata = f2.read()
    f2.close()

    for key in environment:
        filedata = filedata.replace('environ.get("' + key + '")','"' + environment[key] + '"')

    f3 = open(PROJECT_PATH + "/settings.py",'w')
    f3.write(filedata)
    f3.close()


def main():
    print "...RUNNING SETUP CONFIGURATION..."
    try:
        stage = sys.argv[1]
        setupConfig(stage,sys.argv[2])

        if stage != "dev":
            confirm = ""
            while confirm.lower() != "y" and confirm.lower() != "n":
                confirm = raw_input("Do you want to inject environment variable to settings file? Please enter y to proceed or n to cancel:")

            if confirm.lower() == "y":
                injectEnvironmentVariable(stage)

        print "Setup configuration successfully"
    except Exception:
        print "Setup configuration error"
        raise

if __name__ == "__main__":
    main()
