from django.conf.urls import *

from django.views.generic import RedirectView

from univtop.apps.ajax import views

urlpatterns = [
   url(r"^api/call$",views.call_api,name='call_api'),

   url(r"^user/load_user_questions$",views.load_user_questions,name='load_question'),
]
