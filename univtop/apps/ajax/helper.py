from django.core.exceptions import PermissionDenied

from univtop.apps.app_helper import get_user_login_object

from tastypie.models import ApiKey

def get_request_params(request,required_params,optional_params=[]):
	results = {}
	missing_params = []
	if request.method == "GET":
		for param in required_params:
			if param not in request.GET or request.GET[param] == None or len(request.GET[param]) == 0:
				missing_params.append(param)
			else: 
				results[param] = request.GET[param]
		for param in optional_params:
			results[param] = request.GET.get(param,"")
	else:
		for param in required_params:
			if param not in request.POST or request.POST[param] == None or len(request.POST[param]) == 0:
				missing_params.append(param)
			else:
				results[param] = request.POST[param]
		for param in optional_params:
			results[param] = request.POST.get(param,"")

	if len(missing_params) != 0:
		raise ValueError('Missing argument: ' + str(missing_params))
	return results

def validate_request(request,method_allowed=['GET','POST']):
	if request.method not in method_allowed:
		raise PermissionDenied


def generate_headers(request):
	user_login = get_user_login_object(request)
	api_key = ApiKey.objects.get(user=user_login)
	headers = {
		"Authorization": "ApiKey " + user_login.username + ":" + api_key.key,
		"content-type": "application/json"
	}
	return headers