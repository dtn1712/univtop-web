from django.db.models import Count, Q
from django.contrib.auth.models import User
from django.core.cache import cache
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required

from django_ajax.decorators import ajax

from univtop.apps.ajax.helper import validate_request, get_request_params, generate_headers
from univtop.apps.app_helper import get_user_login_object, generate_html_snippet
from univtop.apps.main.constants import SUCCESS_STATUS_CODE, DEFAULT_PAGE_SIZE

from tastypie.models import ApiKey

from validate_email import validate_email

import logging, requests, json, ast

logger = logging.getLogger(__name__)

@ajax
@login_required
def call_api(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['url','data',"type"])

		payload = ast.literal_eval(data['data'])

		user_login = get_user_login_object(request)
		headers = generate_headers(request)

		r = None
		if data['type'].lower() == "put":
			r = requests.put(data['url'], data=json.dumps(payload), headers=headers)
		elif data['type'].lower() == 'patch':
			r = requests.patch(data['url'], data=json.dumps(payload), headers=headers)
		else:
			r = requests.post(data['url'], data=json.dumps(payload), headers=headers)

		if r.status_code in SUCCESS_STATUS_CODE:
			result['status'] = "success"
		else:
			result['status'] = "error"
		
		result['response'] = r.json()
	except Exception as e:
		logger.exception(e)
		result['status'] = "error"
		result['response'] = {
			"error_message": str(e)
		}
	return result


@ajax
def load_user_questions(request):
	result = {}
	try:
		validate_request(request,['GET'])
		data = get_request_params(request,["username",'question_type','start_index'])

		question_type = data['question_type']
		user = User.objects.get(username=data['username'])
		start_index = int(data['start_index'])

		count = 0
		context_data = { 
			"user": user,
		}

		if question_type == "user_ask_questions":
			questions = user.userprofile.get_user_questions(start_index)
			count = len(questions)
			context_data['questions'] = questions
		elif question_type == 'user_answer_questions':
			answers = user.userprofile.get_user_answers(start_index)
			count = len(answers)
			context_data['answers'] = answers
		elif question_type == 'user_following_questions':
			questions = user.userprofile.get_user_following_questions(start_index)
			count = len(questions)
			context_data['questions'] = questions

		result['content'] = generate_html_snippet(request,question_type,context_data)
		result['num_item'] = count
		result['start_index'] = start_index + count
		result['is_all_items_loaded'] = False
		if count < DEFAULT_PAGE_SIZE:
			result['is_all_items_loaded'] = True
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


