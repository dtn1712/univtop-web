from django.conf import settings

from univtop.settings import SITE_NAME, DEFAULT_TEMPLATE_FILE_EXTENSION, API_URL, FACEBOOK_PAGE_ID, IOS_APP_ID

import datetime

LOGO_ICON_URL = "https://univtop.s3.amazonaws.com/img/ico/favicon.ico"
USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

DEFAULT_LATITUDE = 47.616000
DEFAULT_LONGITUDE = -122.322464
DEFAULT_CITY = "Seattle"
DEFAULT_COUNTRY = "United States"
DEFAULT_COUNTRY_CODE = "US"
DEFAULT_TIMEZONE = "America/Los_Angeles"

SITE_DATA = {
  "SITE_NAME" : SITE_NAME,
  "SITE_KEYWORDS": "Univtop, UnivTop, univtop, study abroad, international student, career, college, college life, campus life, scholarship, success",
  "SITE_DESCRIPTION": SITE_NAME[0].upper() + SITE_NAME[1:].lower() + " is a platform for international student to connect and share their experience",
  'API_URL': API_URL,
  "TEMPLATE_FILE_EXTENSION": DEFAULT_TEMPLATE_FILE_EXTENSION,
  "DEFAULT_TIMEZONE": DEFAULT_TIMEZONE,
  'PREVIEW_TEXT_LENGTH': 200,
  "CONTACT_EMAIL": "contact@univtop.com",
  "COMPANY_ADDRESS": "",
  "FACEBOOK_PAGE_ID": FACEBOOK_PAGE_ID,
  "IOS_APP_ID": IOS_APP_ID
}

KEYWORDS_URL = [
	'admin','signup','login','password',"accounts"
	'logout','confirm_email','searXch','settings',
	'buzz','messages',"about",'api','asset','photo',
	'feeds','friends'
]

MODEL_KEY_LIST = [
  "EM","OR", "PJ", "PA", "IV", "PT", "CM", "CN", "RP", "NF", "MG", "FD", "DI", "UN"
]

MODEL_KEY_MAP = {
  "email": "EM",
  "question": "QU",
  "answer": "AN",
  "tag": "TG",
  "school": "SC",
  "photo": "PT",
  "comment": "CM",
  "report": "RP",
  "notification": "NF",
  "vote": "VT",
  "default_image": "DI",
}

MODEL_KEY_REVERSE_MAP = {
  "QU": "question",
  "AN": "answer",
  "TG": "tag",
  "SC": "school",
  "PT": "photo",
  "CM": "comment",
  "RP": "report",
  "NF": "notification",
  "DI": "default_image",
}

UNSPECIFIED_MODEL_KEY = "UN"


DEFAULT_IMAGE_PATH_MAPPING = {
  # User Image
  "default_male_icon": "default/img/user/male_icon.jpg",
  "default_female_icon": "default/img/user/female_icon.jpg",
  "default_cover_picture": 'default/img/user/cover_picture.jpg',
  "default_project_picture": 'default/img/user/project_picture.jpg',
  "default_project_activity_picture": 'default/img/user/project_activity_picture.jpg',
}


DEFAULT_IMAGE_UNIQUE_ID = {
  "default_male_icon": "DIBHgn2pXkaWLAYgpRsQGTo3088",
  "default_female_icon": "DIJE4S63KnuKozEq4BsC2PH4019",
  "default_cover_picture": 'DInXsK7dXR9BGXpmKR9Mhd6D124',
  "default_project_picture": 'DInXsK7dXR9BGXpmKR9Mhd7B89',
  "default_project_activity_picture": 'DInXsK7dXR9BGXpmKR9Mhd2M33',
}

MESSAGE_SNIPPET_TEMPLATE =  { 
  # Auth app
  "signup_success": "messages/apps/account/signup_success.html",
  "confirm_email_success": "messages/apps/account/confirm_email_success.html",
  "confirm_email_asking": "messages/apps/account/confirm_email_asking.html",
  "send_confirm_email_success": "messages/apps/account/send_confirm_email_success.html",
  "send_confirm_email_error": "messages/apps/account/send_confirm_email_error.html",
  "change_password_success": "messages/apps/account/change_password_success.html",
  "social_login_error": "messages/apps/account/social_login_error.html",
  "reset_password_success": "messages/apps/account/reset_password_success.html",

  # "send_feedback_success": "messages/apps/about/send_feedback_success.html",
}

HTML_SNIPPET_TEMPLATE_NON_RESPONSIVE = {
  "user_ask_questions": "sites/non_responsive/apps/member/snippet/user_ask_questions.jinja.html",
  "user_answer_questions": "sites/non_responsive/apps/member/snippet/user_answer_questions.jinja.html",
  "user_following_questions": "sites/non_responsive/apps/member/snippet/user_following_questions.jinja.html"
}

HTML_SNIPPET_TEMPLATE_RESPONSIVE = {
  "user_ask_questions": "sites/responsive/apps/member/snippet/user_ask_questions.jinja.html",
  "user_answer_questions": "sites/responsive/apps/member/snippet/user_answer_questions.jinja.html",
  "user_following_questions": "sites/responsive/apps/member/snippet/user_following_questions.jinja.html"
}

NOTIFICATION_SNIPPET_TEMPLATE = {
  "one_answer" : "notifications/apps/question/one_answer_for_question.txt",
  "many_answers" : "notifications/apps/question/many_answers_for_questions.txt",
  "notification_tag" : "notifications/apps/tag/notification_tag.txt",
}


