from django.conf.urls import *
from django.views.generic import RedirectView

from univtop.apps.account import views
from univtop.apps.account import forms

urlpatterns = [

    url(r"^login/$",views.login, name="account_login"),

    url(r"^logout$", views.logout, name="account_logout"),
    url(r"^logout/$",RedirectView.as_view(url='/account/logout',permanent=False)),

    url(r"^password/change$", views.password_change,name="account_change_password"),
    url(r"^password/set$", views.password_set, name="account_set_password"),

    # E-mail
    url(r"^confirm-email/$", views.send_confirm_email, name="account_email"),
    url(r"^confirm-email/(?P<key>\w+)/$", views.confirm_email,name="account_confirm_email"),
    
    # Handle old redirects
    url(r"^confirm_email/(?P<key>\w+)/$",RedirectView.as_view(url='/accounts/confirm-email/%(key)s/')),

    url(r"^password/reset/key/(?P<uidb36>[0-9A-Za-z]+)-(?P<key>.+)$",views.password_reset_from_key,name="reset_password_from_key"),
]
