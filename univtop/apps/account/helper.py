from univtop.apps.main.models import EmailAddress
from univtop.apps.app_helper import get_user_login_object, generate_message, capitalize_first_letter
from univtop.settings import SITE_NAME

def activate_email_reminder_message(request,user_login):
	if ("action" not in request.GET or "result" not in request.GET) and (user_login != None):
		is_not_activate = bool(EmailAddress.objects.filter(email=user_login.email,verified=False).count())
		if is_not_activate:
			return generate_message("confirm_email","asking",{
				'user_login':user_login,
				'site_name': capitalize_first_letter(SITE_NAME)
			})
	return None
