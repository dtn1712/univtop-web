from django import forms
from django.contrib.auth.models import User
from django.shortcuts import render_to_response, get_object_or_404, redirect, render
from django.utils.translation import pgettext, ugettext_lazy as _, ugettext
from django.core.urlresolvers import reverse
from django.utils.http import int_to_base36
from django.contrib.auth import authenticate
from django.db.models import Q
from django.template.loader import render_to_string

from univtop.apps.member.helper import get_user_common_info
from univtop.apps.account.utils import perform_login, random_token
from univtop.apps.main.models import Email, UserProfile, Notification
from univtop.apps.app_helper import check_key_not_blank
from univtop.settings import WEBSITE_URL
from univtop.apps.app_settings import NOTIFICATION_SNIPPET_TEMPLATE
from univtop.apps.account import settings as app_settings
from univtop.apps.account.utils import email_address_exists, generate_unique_username

import logging

logger = logging.getLogger(__name__)

class UserForm(forms.Form):

	def __init__(self, user=None, *args, **kwargs):
		self.user = user
		super(UserForm, self).__init__(*args, **kwargs)


class SetPasswordField(forms.CharField):

	def clean(self, value):
		value = super(SetPasswordField, self).clean(value)
		min_length = app_settings.PASSWORD_MIN_LENGTH
		if len(value) < min_length:
			raise forms.ValidationError(_("Password must be a minimum of {0} "
										  "characters.").format(min_length))
		return value


class LoginForm(forms.Form):

	email = forms.EmailField(label=_("Email"),max_length=60,widget=forms.TextInput(attrs={'placeholder': 'Email','class': "form-control"}))
	password = forms.CharField(label=_("Password"),widget=forms.PasswordInput(attrs={'placeholder':"Password", "class": "form-control"}))

	def user_credentials(self):
		"""
		Provides the credentials required to authenticate the user for
		login.
		"""
		credentials = {
		  "email": self.cleaned_data["email"],
		  "password": self.cleaned_data["password"]
		}
		return credentials

	def clean(self):
		if self._errors:
			return
		user = authenticate(**self.user_credentials())
		if user:
			if user.is_active:
				self.user = user
			else:
				raise forms.ValidationError(_("This account is currently"
											  " inactive."))
		else:
			error = _("The e-mail address and/or password you specified"
						  " are not correct.")
			raise forms.ValidationError(error)
		return self.cleaned_data

	def login(self, request, redirect_url=None):
		ret = perform_login(request, self.user,
							email_verification=app_settings.EMAIL_VERIFICATION,
							redirect_url=redirect_url)
		request.session.set_expiry(60 * 60 * 24 * 7 * 3)
		return ret

class ResetPasswordKeyForm(forms.Form):

	password1 = SetPasswordField(label=_("New Password"),widget=forms.PasswordInput(attrs={'placeholder':"Password", "class": "form-control"}))
	password2 = forms.CharField(label=_("Confirm Password"),widget=forms.PasswordInput(attrs={'placeholder':"Confirm Password", "class": "form-control"}))

	def __init__(self, *args, **kwargs):
		self.user = kwargs.pop("user", None)
		self.temp_key = kwargs.pop("temp_key", None)
		super(ResetPasswordKeyForm, self).__init__(*args, **kwargs)

	def save(self):
		# set the new user password
		user = self.user
		user.set_password(self.cleaned_data["password1"])
		user.save()


class ChangePasswordForm(UserForm):

	oldpassword = forms.CharField(label=_("Current Password"),widget=forms.PasswordInput(attrs={'placeholder':"Confirm Password", "class": "form-control"}))
	password1 = SetPasswordField(label=_("New Password"),widget=forms.PasswordInput(attrs={'placeholder':"Password", "class": "form-control"}))
	password2 = forms.CharField(label=_("Confirm Password"),widget=forms.PasswordInput(attrs={'placeholder':"Confirm Password", "class": "form-control"}))

	def clean_oldpassword(self):
		if not self.user.check_password(self.cleaned_data.get("oldpassword")):
			raise forms.ValidationError(_("Please type your current"
										  " password."))
		return self.cleaned_data["oldpassword"]

	def clean_password2(self):
		if ("password1" in self.cleaned_data
				and "password2" in self.cleaned_data):
			if (self.cleaned_data["password1"]
					!= self.cleaned_data["password2"]):
				raise forms.ValidationError(_("You must type the same password"
											  " each time."))
		return self.cleaned_data["password2"]

	def save(self):
		self.user.set_password(self.cleaned_data["password1"])
		self.user.save()


class SetPasswordForm(UserForm):

	password1 = SetPasswordField(label=_("Password"),widget=forms.PasswordInput(attrs={'placeholder':"Password", "class": "form-control"}))
	password2 = forms.CharField(label=_("Confirm Password"),widget=forms.PasswordInput(attrs={'placeholder':"Confirm Password", "class": "form-control"}))

	def clean_password2(self):
		if ("password1" in self.cleaned_data
				and "password2" in self.cleaned_data):
			if (self.cleaned_data["password1"]
					!= self.cleaned_data["password2"]):
				raise forms.ValidationError(_("You must type the same password"
											  " each time."))
		return self.cleaned_data["password2"]

	def save(self):
		self.user.set_password(self.cleaned_data["password1"])
		self.user.save()

