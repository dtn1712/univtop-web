from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError, Http404
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, redirect, render
from django.core.urlresolvers import reverse, reverse_lazy
from django.utils.http import base36_to_int
from django.views.generic import RedirectView
from django.views.generic.base import ContextMixin
from django.views.generic.base import TemplateResponseMixin, View, TemplateView
from django.views.generic.edit import FormView
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.tokens import default_token_generator
from django.utils.cache import patch_response_headers
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect, Http404, HttpResponsePermanentRedirect
from django.views.decorators.debug import sensitive_post_parameters
from django.utils.decorators import method_decorator
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt

from univtop.apps.account.utils import get_user_model, complete_signup
from univtop.apps.account.utils import get_login_redirect_url
from univtop.apps.account import signals
from univtop.apps.account import settings as app_settings

from univtop.apps.app_helper import get_user_login_object, get_template_path
from univtop.apps.account.forms import LoginForm, ChangePasswordForm
from univtop.apps.account.forms import SetPasswordForm, ResetPasswordKeyForm
from univtop.apps.app_views import AppBaseView, PostRequestOnlyView
from univtop.apps.main.constants import ACTIVATE_LINK
from univtop.apps.main.models import EmailAddress, EmailConfirmation
from univtop.settings import API_URL

import logging, requests, json, requests

logger = logging.getLogger(__name__)

APP_NAME = "account"

sensitive_post_parameters_m = method_decorator(sensitive_post_parameters('password', 'password1', 'password2'))

class SendConfirmEmailView(RedirectView):

	def get(self, request, *args, **kwargs):
		user_login = get_user_login_object(request)
		email = user_login.email
		try:
			email_address = EmailAddress.objects.get(
					user=request.user,
					email=email,
				)
			email_address.send_confirmation(request,ACTIVATE_LINK)
			return HttpResponseRedirect("/?action=send_confirm_email&result=success&session_id=" + request.session.session_key)
		except EmailAddress.DoesNotExist:
			return HttpResponseRedirect("/?action=send_confirm_email&result=error&session_id=" + request.session.session_key)

send_confirm_email = SendConfirmEmailView.as_view(permanent=False)



class ConfirmEmailView(View):

	def dispatch(self, request, *args, **kwargs):
		self.request = request
		return super(ConfirmEmailView, self).dispatch(request, *args, **kwargs)

	def get(self, *args, **kwargs):
		try:
			self.object = self.get_object()
			return self.post(*args,**kwargs)
		except Http404:
			return HttpResponseRedirect("/")

	def post(self, *args, **kwargs):
		self.object = confirmation = self.get_object()
		confirmation.confirm(self.request)
		# get_adapter().add_message(self.request,
		#                           messages.SUCCESS,
		#                           'account/messages/email_confirmed.txt',
		#                           {'email': confirmation.email_address.email})
		resp = self.login_on_confirm(confirmation)
		if resp:
			return resp
		# Don't -- allauth doesn't touch is_active so that sys admin can
		# use it to block users et al
		#
		# user = confirmation.email_address.user
		# user.is_active = True
		# user.save()
		redirect_url = self.get_redirect_url()
		if not redirect_url:
			ctx = self.get_context_data()
			return self.render_to_response(ctx)
		return redirect(redirect_url)

	def login_on_confirm(self, confirmation):
		"""
		Simply logging in the user may become a security issue. If you
		do not take proper care (e.g. don't purge used email
		confirmations), a malicious person that got hold of the link
		will be able to login over and over again and the user is
		unable to do anything about it. Even restoring his own mailbox
		security will not help, as the links will still work. For
		password reset this is different, this mechanism works only as
		long as the attacker has access to the mailbox. If he no
		longer has access he cannot issue a password request and
		intercept it. Furthermore, all places where the links are
		listed (log files, but even Google Analytics) all of a sudden
		need to be secured. Purging the email confirmation once
		confirmed changes the behavior -- users will not be able to
		repeatedly confirm (in case they forgot that they already
		clicked the mail).

		All in all, opted for storing the user that is in the process
		of signing up in the session to avoid all of the above.  This
		may not 100% work in case the user closes the browser (and the
		session gets lost), but at least we're secure.
		"""
		user_pk = self.request.session.pop('account_user', None)
		user = confirmation.email_address.user
		if user_pk == user.pk and self.request.user.is_anonymous():
			return perform_login(self.request,
								 user,
								 app_settings.EmailVerificationMethod.NONE)

	def get_object(self, queryset=None):
		if queryset is None:
			queryset = self.get_queryset()
		try:
			return queryset.get(key=self.kwargs["key"].lower())
		except EmailConfirmation.DoesNotExist:
			raise Http404()

	def get_queryset(self):
		qs = EmailConfirmation.objects.all_valid()
		qs = qs.select_related("email_address__user")
		return qs

	def get_context_data(self, **kwargs):
		ctx = kwargs
		ctx["confirmation"] = self.object
		return ctx

	def get_redirect_url(self):
		return "/question/?action=confirm_email&result=success&session_id=" + self.request.session.session_key

confirm_email = ConfirmEmailView.as_view()



class LoginView(PostRequestOnlyView,FormView):
	form_class = LoginForm
	redirect_field_name = "next"

	def get_success_url(self,user):
		return reverse("member_profile",kwargs=dict(username=user.username))

	def post(self, request, *args, **kwargs):
		"""
		Handles POST requests, instantiating a form instance with the passed
		POST variables and then checked for validity.
		"""
		form = self.get_form()
		if form.is_valid():
			success_url = self.get_success_url(form.user)
			return form.login(self.request,success_url)
		else:
			logger.exception(form.errors)
			raise Exception


login = LoginView.as_view()


class PasswordChangeView(FormView,AppBaseView):
	app_name = APP_NAME
	template_name = "password_change"
	form_class = ChangePasswordForm
	success_url = reverse_lazy("login")

	def dispatch(self, request, *args, **kwargs):
		if not request.user.has_usable_password():
			return HttpResponseRedirect(reverse('account_set_password'))
		return super(PasswordChangeView, self).dispatch(request, *args,
														**kwargs)

	def get_form_kwargs(self):
		kwargs = super(PasswordChangeView, self).get_form_kwargs()
		kwargs["user"] = self.request.user
		return kwargs

	def form_valid(self, form):
		form.save()
		signals.password_changed.send(sender=self.request.user.__class__,
									  request=self.request,
									  user=self.request.user)
		return super(PasswordChangeView, self).form_valid(form)

password_change = login_required(PasswordChangeView.as_view())


class PasswordSetView(FormView,AppBaseView):
	app_name = APP_NAME
	template_name = "password_set"
	form_class = SetPasswordForm
	success_url = reverse_lazy("login")

	def dispatch(self, request, *args, **kwargs):
		if request.user.has_usable_password():
			return HttpResponseRedirect(reverse('account_change_password'))
		return super(PasswordSetView, self).dispatch(request, *args, **kwargs)

	def get_form_kwargs(self):
		kwargs = super(PasswordSetView, self).get_form_kwargs()
		kwargs["user"] = self.request.user
		return kwargs

	def form_valid(self, form):
		form.save()
		signals.password_set.send(sender=self.request.user.__class__,
								  request=self.request, user=self.request.user)
		return super(PasswordSetView, self).form_valid(form)

password_set = login_required(PasswordSetView.as_view())


class PasswordResetFromKeyView(FormView,AppBaseView):
	app_name = APP_NAME
	template_name = "password_reset_from_key"
	form_class = ResetPasswordKeyForm

	@method_decorator(csrf_exempt)
	def dispatch(self, request, uidb36, key, **kwargs):
		self.request = request
		path = request.path
		token = path[path.rfind("/")+1:]
		r = requests.post(API_URL + "/api/v1/auth/password_reset/" + token)
		data = r.json()
		if data['is_valid_key']:
			self.reset_user = User.objects.get(username=data['user']['username'])
			return super(PasswordResetFromKeyView, self).dispatch(request,uidb36,key,**kwargs)
		else:
			return HttpResponseRedirect("/")

	def post(self, *args, **kwargs):
		return super(PasswordResetFromKeyView, self).post(*args,**kwargs)

	def get_form_kwargs(self):
		kwargs = super(PasswordResetFromKeyView, self).get_form_kwargs()
		kwargs["user"] = self.reset_user
		return kwargs

	def form_valid(self, form):
		form.save()
		signals.password_reset.send(sender=self.reset_user.__class__,request=self.request,user=self.reset_user)
		return super(PasswordResetFromKeyView, self).form_valid(form)

	def get_success_url(self):
		return "/?action=reset_password&result=success&show=1" 

password_reset_from_key = PasswordResetFromKeyView.as_view()


class LogoutView(TemplateResponseMixin, View):

	redirect_url = "/"
	redirect_field_name = "next"

	def get(self, *args, **kwargs):
		if app_settings.LOGOUT_ON_GET:
			return self.post(*args, **kwargs)
		if not self.request.user.is_authenticated():
			return redirect(self.get_redirect_url())
		ctx = self.get_context_data()
		return self.render_to_response(ctx)

	def post(self, *args, **kwargs):
		url = self.get_redirect_url()
		if self.request.user.is_authenticated():
			self.logout()
		return redirect(url)

	def logout(self):
		auth_logout(self.request)

	def get_context_data(self, **kwargs):
		ctx = kwargs
		redirect_field_value = self.request.REQUEST.get(self.redirect_field_name)
		ctx.update({
			"redirect_field_name": self.redirect_field_name,
			"redirect_field_value": redirect_field_value})
		return ctx

	def get_redirect_url(self):
		return self.redirect_url

logout = LogoutView.as_view()



