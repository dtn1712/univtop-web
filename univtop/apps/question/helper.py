import json, logging

logger = logging.getLogger(__name__)

def get_question_common_info(question):
	try:
		data = {
			"unique_id": question.unique_id,
			"title": question.title,
			"type": question.question_type,
			"create_date" : question.create_date,
			"user_post_username": question.user_post.username,
			"user_post_fullname": question.user_post.get_profile().get_user_fullname(),
			"tags": question.tags.all().values_list('value', flat=True),
		}
		return data
	except Exception as e:
		logger.exception(e)
	return None

def get_question_common_info_json(question):
	data = get_question_common_info(question)
	if data == None:
		return json.dumps({})
	else:
		return json.dumps(data)


