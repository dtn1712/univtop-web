from django.views.generic import ListView

from univtop.apps.main.models import Question
from univtop.apps.app_views import AppBaseView

import logging

logger = logging.getLogger(__name__)

APP_NAME = "question"


class ListQuestionView(ListView,AppBaseView):
	app_name = APP_NAME
	template_name = "list"
	model = Question
	paginate_by = 10






