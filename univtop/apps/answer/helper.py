import json, logging

logger = logging.getLogger(__name__)

def get_answer_common_info(answer):
	try:
		data = {
			"unique_id": answer.unique_id,
			"content": answer.content,
			"user_post_username": answer.user_post.username,
			"user_post_fullname": answer.user_post.get_profile().get_user_fullname(),
			"question": answer.question.unique_id,
			"create_date" : answer.create_date,
			"edit_date": answer.edit_date,
			"votes": answer.votes.all().count()
		}
		return data
	except Exception as e:
		logger.exception(e)
	return None

def get_answer_common_info_json(answer):
	data = get_answer_common_info(answer)
	if data == None:
		return json.dumps({})
	else:
		return json.dumps(data)

