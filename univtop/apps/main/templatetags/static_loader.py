from django.middleware.csrf import get_token
from django.conf import settings
from django.core.files.storage import get_storage_class

from django_jinja import library

from univtop.settings import ROOT_PATH, STATIC_URL, SITE_NAME
from univtop.settings import BUILD_VERSION_ID, DEFAULT_APP_NAME
from univtop.apps.app_helper import read_catalogue

import logging, os, jinja2

logger = logging.getLogger(__name__)

NON_RESPONSIVE_TYPE = "N"
RESPONSIVE_TYPE = "R"


@library.global_function
@jinja2.contextfunction
def load_js(context):
	stage = context['stage']
	app_name = DEFAULT_APP_NAME if "app_name" not in context else context['app_name']
	#responsive_type = "non_responsive" if context['flavour'] == 'full' else "responsive"
	responsive_type = "non_responsive"
	result = ''
	if stage == "dev":
		plugin_files, global_files = [], []
		read_catalogue(plugin_files,ROOT_PATH + "/assets/static/js/plugins/",None)
		read_catalogue(global_files,ROOT_PATH + "/assets/static/js/global/",None)
		for filename in plugin_files:
			result = result + '<script type="text/javascript" src="' + STATIC_URL + 'js/plugins/' + filename +'"></script>\n'
		
		for filename in global_files:
			result = result + '<script type="text/javascript" src="' + STATIC_URL + 'js/global/' + filename +'"></script>\n'
		
		# result = result + '<script type="text/javascript" src="' + STATIC_URL + 'js/apps/' + app_name + '.js"></script>\n'
		return result
	else:
		result = '<script type="text/javascript" src="' + STATIC_URL + 'js/prod/' + SITE_NAME + '.script.' + BUILD_VERSION_ID + '.min.js"></script>'
		#result = result + '<script type="text/javascript" src="' + STATIC_URL + 'js/apps/' + responsive_type + "/" + app_name + '.js"></script>'
		return result

@library.global_function
@jinja2.contextfunction
def load_css(context):
	stage = context['stage']
	app_name = DEFAULT_APP_NAME if "app_name" not in context else context['app_name']
	#responsive_type = "non_responsive" if context['flavour'] == 'full' else "responsive"
	responsive_type = "non_responsive"
	result = ""
	if stage == "dev":
		catalogue_type = NON_RESPONSIVE_TYPE if context['flavour'] == 'full' else  RESPONSIVE_TYPE
		css_path = ROOT_PATH + "/assets/static/css/"
		list_file = []
		read_catalogue(list_file,css_path,catalogue_type)
		for filename in list_file:
			result = result + '<link rel="stylesheet" href="' + STATIC_URL + 'css/' + filename + '" type="text/css" />\n'
		result = result + '<link rel="stylesheet" href="' + STATIC_URL + 'css/apps/' + responsive_type + "/" + app_name + '.css" type="text/css" />'
		return result
	else:
		return '<link rel="stylesheet" href="' + STATIC_URL + 'css/prod/stylesheets/' + app_name + '.' + responsive_type + '.' + BUILD_VERSION_ID + '.min.css" type="text/css" />'





