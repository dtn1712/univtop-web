from django_jinja import library

import logging, cloudinary

logger = logging.getLogger(__name__)

@library.global_function
def get_image_url(photo, width, height, crop):
	if width == None:
		url = cloudinary.CloudinaryImage(photo.unique_id).build_url(height=height,crop=crop)
	elif height == None:
		url =cloudinary.CloudinaryImage(photo.unique_id).build_url(width=width,crop=crop)
	else:
		url = cloudinary.CloudinaryImage(photo.unique_id).build_url(width=width,height=height,crop=crop)
	
	print url
	if url == None:
		return photo.image.url
	else:
		return url

@library.global_function
def get_image_element(photo, width, height, crop):
	return "<img src='" + get_image_url(photo,width,height,crop) + "' width='" + width + "' height='" + height + "' />"