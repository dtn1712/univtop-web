from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from tastypie.admin import ApiKeyInline

from univtop.apps.main.models import *

class NotificationAdmin(admin.ModelAdmin):
    list_display = ['unique_id','content','status','notify_to','user_post','date','notification_type']

admin.site.register(Notification, NotificationAdmin)


