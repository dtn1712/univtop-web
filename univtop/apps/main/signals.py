from django.core.cache import cache
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete, pre_delete

@receiver(post_delete)
@receiver(post_save)
def clear_template_cache(sender, instance, **kwargs):
	list_of_models = ('User', 'UserProfile', 'Question', 'Notification', 'Answer')
	if sender.__name__ in list_of_models:
		cache.delete_pattern("views.decorators.cache.*")