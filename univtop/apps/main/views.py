from django.views.generic import ListView, TemplateView
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.template import RequestContext, engines
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.utils import timezone
from django.core import serializers

from univtop.apps.app_helper import get_template_path, get_user_login_object
from univtop.apps.app_views import AppBaseView

import logging

logger = logging.getLogger(__name__)

APP_NAME = "main"


class MainView(TemplateView,AppBaseView):
	app_name = APP_NAME
	template_name = "index"


def handler404(request):
	data = {
		"user_login": get_user_login_object(request),
		"template_type": "non_responsive"
	}
	jinja_engine = engines['jinja']
	template = jinja_engine.get_template('sites/non_responsive/404.jinja.html')
	return HttpResponse(template.render(context=data,request=request))

def handler500(request):
	data = {
		"user_login": get_user_login_object(request),
		"template_type": "non_responsive"
	}
	jinja_engine = engines['jinja']
	template = jinja_engine.get_template('sites/non_responsive/500.jinja.html')
	return HttpResponse(template.render(context=data,request=request))