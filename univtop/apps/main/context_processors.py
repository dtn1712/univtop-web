from django.contrib.gis.geoip import GeoIP
from django.template import RequestContext

from univtop.apps.app_settings import SITE_DATA, DEFAULT_LATITUDE
from univtop.apps.app_settings import DEFAULT_LONGITUDE, DEFAULT_CITY
from univtop.apps.app_settings import DEFAULT_COUNTRY, DEFAULT_COUNTRY_CODE

from univtop.settings import SITE_NAME, STAGE, geoip

from univtop.apps.app_helper import handle_request_get_message, get_user_login_object
from univtop.apps.app_helper import capitalize_first_letter, get_client_ip, get_base_template_path

import datetime, django_mobile

def site_data(request):
	return SITE_DATA 

def global_data(request):
	results = {}
	client_ip = get_client_ip(request)
	results['client_ip'] = client_ip
	results['current_lat'] = DEFAULT_LATITUDE
	results['current_lng'] = DEFAULT_LONGITUDE
	results['current_city'] = DEFAULT_CITY
	results['current_country'] = DEFAULT_COUNTRY
	results['current_country_code'] = DEFAULT_COUNTRY_CODE
	results['geo_data'] = None
	if geoip.city(client_ip) != None:
		geo_data = geoip.city(client_ip)
		results['current_lat'] = geo_data['latitude']
		results['current_lng'] = geo_data['longitude']
		results['current_city'] = geo_data['city']
		results['current_country'] = geo_data['country_name']
		results['current_country_code'] = geo_data['country_code']
		results['geo_data'] = geo_data

	results['stage'] = STAGE
	results['base_template'] = get_base_template_path(None)
	#results['base_template'] = get_base_template_path(django_mobile.get_flavour(request))
	
	return results


