from django.db import models, transaction
from django.db.models import Count
from django.contrib.auth.models import User
from django.contrib import admin
from django.core.cache import cache
from django.conf import settings
from django.utils import timezone
from django.template.loader import render_to_string
from django.db.models import Q
from django.core.mail import get_connection, EmailMultiAlternatives
from django.core.mail.backends.smtp import EmailBackend as DjangoSmtpEmailBackend
from django.utils.encoding import python_2_unicode_compatible
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils.crypto import get_random_string

from univtop.settings import MEDIA_URL, DEFAULT_FROM_EMAIL, SITE_NAME, WEBSITE_URL
from univtop.settings import EMAIL_PORT, EMAIL_HOST_USER, EMAIL_HOST_PASSWORD, EMAIL_USE_TLS

from univtop.apps.main.constants import PASSCODE, ACTIVATE_LINK 
from univtop.apps.main.constants import GENDER, NEW, OLD
from univtop.apps.main.constants import PHOTO_TYPE, NOTIFICATION_STATUS, NOTIFICATION_TYPE
from univtop.apps.main.constants import REPORT_TYPE, VOTE_TYPE
from univtop.apps.main.constants import PRIVACY_STATUS, PUBLIC
from univtop.apps.main.constants import QUESTION_TYPE, NORMAL_QUESTION
from univtop.apps.main.constants import UP_VOTE, DOWN_VOTE, TAG_TYPE
from univtop.apps.main.constants import DEFAULT_PAGE_SIZE

from univtop.apps.app_helper import generate_unique_id, get_elapse_time_text, is_empty
from univtop.apps.member.helper import get_user_common_info
from univtop.apps.question.helper import get_question_common_info

from univtop.apps.account.managers import EmailAddressManager, EmailConfirmationManager
from univtop.apps.account import settings as app_settings

from imagekit.models import ProcessedImageField

import datetime, random, datetime, logging, ast, hashlib

try:
	storage = settings.MULTI_IMAGES_FOLDER + '/'
except AttributeError:
	storage = 'upload_storage/'


logger = logging.getLogger(__name__)

def user_field(user, field, *args):
	"""
	Gets or sets (optional) user model fields. No-op if fields do not exist.
	"""
	if field and hasattr(user, field):
		if args:
			# Setter
			v = args[0]
			if v:
				v = v[0:User._meta.get_field(field).max_length]
			setattr(user, field, v)
		else:
			# Getter
			return getattr(user, field)


def user_username(user, *args):
	return user_field(user, app_settings.USER_MODEL_USERNAME_FIELD, *args)


def user_email(user, *args):
	return user_field(user, app_settings.USER_MODEL_EMAIL_FIELD, *args)


def random_token(extra=None, hash_func=hashlib.sha256):
	if extra is None:
		extra = []
	bits = extra + [str(random.SystemRandom().getrandbits(512))]
	return hash_func("".join(bits).encode('utf-8')).hexdigest()


class Email(models.Model):
	unique_id = models.CharField(max_length=100,blank=True,null=True,unique=True,default=lambda:generate_unique_id("email"))
	to_emails = models.TextField()
	email_template = models.CharField(max_length=200)
	subject = models.CharField(max_length=350,blank=True)
	text_content = models.TextField(blank=True)
	context_data = models.TextField()
	status = models.CharField(max_length=50,default="pending")
	send_time = models.DateTimeField(blank=True,null=True)
	request_time = models.DateTimeField(auto_now_add=True)


class Photo(models.Model):
	unique_id = models.CharField(max_length=100,blank=True,null=True,unique=True,default=lambda:generate_unique_id("photo"))
	name = models.CharField(max_length=150,blank=True)
	image = models.ImageField(upload_to=storage+"/photo/%Y/%m/%d")
	# image = ProcessedImageField(upload_to=storage+"/photo/%Y/%m/%d",format='JPEG',options={'quality': 70},blank=True,null=True)
	image_url = models.CharField(max_length=300,blank=True)
	image_secure_url = models.CharField(max_length=300,blank=True)
	rotation_angle = models.IntegerField(default=0)
	width = models.IntegerField(blank=True,null=True)
	height = models.IntegerField(blank=True,null=True)
	upload_time = models.DateTimeField(auto_now_add=True)
	upload_success = models.BooleanField(default=True)
	user_post = models.ForeignKey(User,blank=True,null=True)

	def __unicode__(self):
		return self.image.name


class Notification(models.Model):
	unique_id = models.CharField(max_length=100,blank=True,null=True,unique=True,default=lambda:generate_unique_id("notification"))
	content = models.TextField()
	html_content = models.TextField(blank=True)
	template = models.CharField(max_length=100)
	context_data = models.TextField(blank=True)
	status = models.CharField(max_length=1,choices=NOTIFICATION_STATUS,default=NEW)
	notification_type = models.CharField(max_length=20,choices=NOTIFICATION_TYPE)
	notify_object_id = models.CharField(max_length=100,null=True,blank=True)
	notify_to = models.ForeignKey(User, related_name='notify_to')
	user_post = models.ForeignKey(User, related_name='notify_from',blank=True,null=True)
	image = models.ForeignKey("Photo",blank=True,null=True)
	related_link = models.TextField(blank=True)
	date = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.unique_id


class Report(models.Model):
	unique_id = models.CharField(max_length=100,blank=True,null=True,unique=True,default=lambda:generate_unique_id("report"))
	report_type = models.CharField(max_length=20,choices=REPORT_TYPE)
	object_id = models.CharField(max_length=100)
	user_post = models.ForeignKey(User,related_name='report_user_post')
	date = models.DateTimeField(auto_now_add=True)
	report_content = models.TextField()


class Vote(models.Model):
	unique_id = models.CharField(max_length=100,blank=True,null=True,unique=True,default=lambda:generate_unique_id("vote"))
	user_vote = models.ForeignKey(User,related_name="user_vote")
	vote_type = models.CharField(max_length=30,choices=VOTE_TYPE)
	object_id = models.CharField(max_length=100)

	def __unicode__(self):
		return unicode(self.unique_id)


class Tag(models.Model):
	unique_id = models.CharField(max_length=100,blank=True,null=True,unique=True,default=lambda:generate_unique_id("tag"))
	value = models.CharField(max_length=100)
	tag_type = models.CharField(max_length=100,choices=TAG_TYPE)

	def __unicode__(self):
		return self.value


class Answer(models.Model):
	unique_id = models.CharField(max_length=100,blank=True,null=True,unique=True,default=lambda:generate_unique_id("answer"))
	content = models.TextField()
	question = models.ForeignKey("Question",related_name="answer_question")
	user_post = models.ForeignKey(User,related_name="answer_user_post")
	reports = models.ManyToManyField("Report",related_name="answer_reports",blank=True,null=True)
	create_date = models.DateTimeField(auto_now_add=True)
	edit_date =  models.DateTimeField(blank=True,null=True)
	votes = models.ManyToManyField("Vote",related_name="answer_vote",blank=True,null=True)
	photos = models.ManyToManyField("Photo",related_name="answer_photos",blank=True,null=True)

	def __unicode__(self):
		return unicode(self.unique_id)


	def is_user_vote(self,user,return_type="boolean"):
		is_user_vote = Vote.objects.filter(user_vote=user,vote_type='answer',object_id=self.unique_id).exists()
		if return_type == 'int':
			return 0 if is_user_vote == False else 1

		return is_user_vote


class Question(models.Model):
	unique_id = models.CharField(max_length=100,blank=True,null=True,unique=True,default=lambda:generate_unique_id("question"))
	title = models. CharField(max_length=1000)
	content = models.TextField(blank=True)
	tags = models.ManyToManyField("Tag",related_name="question_tags",blank=True,null=True)
	want_answers = models.ManyToManyField(User,related_name="question_answer_wanted",blank=True,null=True)
	question_type = models.CharField(max_length=30,choices=QUESTION_TYPE,default=NORMAL_QUESTION)
	user_post = models.ForeignKey(User,related_name="question_user_post")
	reports = models.ManyToManyField("Report",related_name="question_reports",blank=True,null=True)
	create_date = models.DateTimeField(auto_now_add=True)
	edit_date =  models.DateTimeField(blank=True,null=True)
	votes = models.ManyToManyField("Vote",related_name="question_vote",blank=True,null=True)
	photos = models.ManyToManyField("Photo",related_name="question_photos",blank=True,null=True)

	def __unicode__(self):
		return unicode(self.unique_id)

	def get_answers(self):
		return Answer.objects.filter(question=self).select_related('user_post','question')

	def get_answer_count(self):
		return Answer.objects.filter(question=self).count()

	def is_user_follow(self,user,return_type="boolean"):
		is_user_follow = False
		if self in user.userprofile.following_questions.all():
			is_user_follow = True

		if return_type == 'int':
			return 0 if is_user_follow == False else 1

		return is_user_follow

	def is_user_answer(self,user,return_type='boolean'):
		is_user_answer = Answer.objects.filter(question=self,user_post=user).exists()
		if return_type == 'int':
			return 0 if is_user_answer == False else 1

		return is_user_answer

	def is_user_vote(self,user,return_type="boolean"):
		is_user_vote = Vote.objects.filter(user_vote=user,vote_type='question',object_id=self.unique_id).exists()
		if return_type == 'int':
			return 0 if is_user_vote == False else 1

		return is_user_vote

	def get_user_answer(self,user):
		answers = Answer.objects.filter(question=self,user_post=user).select_related("user_post","question")
		if len(answers) != 0:
			return answers[0]
		else:
			return None

class UserProfile(models.Model):
	user = models.OneToOneField(User)   
	avatar = models.ForeignKey("Photo",blank=True,null=True,related_name="avatar")
	gender = models.CharField(choices=GENDER,max_length=1,default="m")
	bio = models.CharField(max_length=124,blank=True)
	occupation = models.CharField(max_length=124,blank=True)
	privacy_status = models.CharField(max_length=1,choices=PRIVACY_STATUS,default=PUBLIC)
	facebook_id = models.CharField(max_length=40,blank=True,null=True)
	reports = models.ManyToManyField(Report,related_name='user_reports',blank=True,null=True)
	following_users = models.ManyToManyField(User,related_name="following_users",blank=True,null=True)
	following_tags = models.ManyToManyField(Tag,related_name="following_tags",blank=True,null=True)
	following_questions = models.ManyToManyField(Question,related_name="following_questions",blank=True,null=True)


	def __unicode__(self):
		return unicode(self.user)

	def is_facebook_account(self):
		if self.facebook_id == None: return False
		if len(self.facebook_id) != 0:
			return True
		return False

	def get_user_fullname(self):
		first_name = self.user.first_name
		last_name = self.user.last_name
		if first_name.strip() == "" and last_name.strip() == "":
			username = self.user.username
			return username[0].upper() + username[1:].lower()
		return first_name + " " + last_name

	def get_user_questions(self,start_index=None):
		if start_index == None:
			return Question.objects.filter(user_post=self.user).order_by("-create_date").select_related('user_post').prefetch_related('tags')[:DEFAULT_PAGE_SIZE]
		else:
			return Question.objects.filter(user_post=self.user).order_by("-create_date").select_related('user_post').prefetch_related('tags')[start_index:start_index + DEFAULT_PAGE_SIZE]

	def get_user_answers(self,start_index=None,is_include_own_question=False):
		print is_include_own_question
		if start_index == None:
			if is_include_own_question:
				return Answer.objects.filter(user_post=self.user).select_related('user_post','question')[:DEFAULT_PAGE_SIZE]
			else:
				return Answer.objects.filter(user_post=self.user).exclude(question__user_post=self.user).select_related('user_post','question')[:DEFAULT_PAGE_SIZE]
		else:
			if is_include_own_question:
				return Answer.objects.filter(user_post=self.user).select_related('user_post','question')[start_index:start_index + DEFAULT_PAGE_SIZE]
			else:
				return Answer.objects.filter(user_post=self.user).exclude(question__user_post=self.user).select_related('user_post','question')[start_index:start_index + DEFAULT_PAGE_SIZE]

	def get_user_following_questions(self,start_index=None):
		if start_index == None:
			return self.following_questions.all().select_related('user_post').prefetch_related('tags')[:DEFAULT_PAGE_SIZE]
		else:
			return self.following_questions.all().select_related('user_post').prefetch_related('tags')[start_index:start_index + DEFAULT_PAGE_SIZE]

	def get_user_question_count(self):
		return Question.objects.filter(user_post=self.user).count()

	def get_user_answer_count(self):
		return Answer.objects.filter(user_post=self.user).count()

	def get_user_following_questions_count(self):
		return self.following_questions.count()

	def get_user_follower_count(self):
		return UserProfile.objects.filter(following_users=self.user).count()

	def get_user_following_count(self):
		return self.following_users.count()	

	def get_user_vote_count(self):
		return Vote.objects.filter(user_vote=self.user).count()


@python_2_unicode_compatible
class EmailAddress(models.Model):

    user = models.ForeignKey(User,verbose_name=_('main_user'),related_name="user_email_address")
    email = models.EmailField(unique=True,verbose_name=_('e-mail address'))
    verified = models.BooleanField(verbose_name=_('verified'), default=False)
    primary = models.BooleanField(verbose_name=_('primary'), default=False)

    objects = EmailAddressManager()

    class Meta:
        verbose_name = _("email address")
        verbose_name_plural = _("email addresses")
        if not app_settings.UNIQUE_EMAIL:
            unique_together = [("user", "email")]

    def __str__(self):
        return u"%s (%s)" % (self.email, self.user)

    def set_as_primary(self, conditional=False):
        old_primary = EmailAddress.objects.get_primary(self.user)
        if old_primary:
            if conditional:
                return False
            old_primary.primary = False
            old_primary.save()
        self.primary = True
        self.save()
        user_email(self.user, self.email)
        self.user.save()
        return True

    def send_confirmation(self, request, confirm_type, signup=False):
    	confirmation = None
    	try:
    		confirmation = EmailConfirmation.objects.get(email_address=self)
        except EmailConfirmation.DoesNotExist: 
        	confirmation = EmailConfirmation.create(self)

        confirmation.send(request,confirm_type,signup=signup)
        return confirmation

    def change(self, request, new_email, confirm=True):
        """
        Given a new email address, change self and re-confirm.
        """
        with transaction.commit_on_success():
            user_email(self.user, new_email)
            self.user.save()
            self.email = new_email
            self.verified = False
            self.save()
            if confirm:
                self.send_confirmation(request)


@python_2_unicode_compatible
class EmailConfirmation(models.Model):

    email_address = models.ForeignKey(EmailAddress,verbose_name=_('e-mail address'))
    created = models.DateTimeField(verbose_name=_('created'),default=timezone.now)
    sent = models.DateTimeField(verbose_name=_('sent'), null=True)
    email_sent = models.ForeignKey(Email,related_name="email_sent_confirmation",blank=True,null=True)
    key = models.CharField(verbose_name=_('key'), max_length=64, unique=True)
    passcode = models.CharField(verbose_name=_('passcode'), max_length=5)

    objects = EmailConfirmationManager()

    class Meta:
        verbose_name = _("email confirmation")
        verbose_name_plural = _("email confirmations")

    def __str__(self):
        return u"confirmation for %s" % self.email_address

    @classmethod
    def create(cls, email_address):
        key = random_token([email_address.email])
        passcode = str(random.randrange(10000, 99999, 3))
        return cls._default_manager.create(email_address=email_address,
                                           key=key,passcode=passcode)

    def key_expired(self):
        expiration_date = self.sent \
            + datetime.timedelta(days=app_settings
                                 .EMAIL_CONFIRMATION_EXPIRE_DAYS)
        return expiration_date <= timezone.now()
    key_expired.boolean = True

    def confirm(self, request):
        if not self.key_expired() and not self.email_address.verified:
            email_address = self.email_address
            email_address.verified = True
            email_address.set_as_primary(conditional=True)
            email_address.save()
            signals.email_confirmed.send(sender=self.__class__,
                                         request=request,
                                         email_address=email_address)
            return email_address

    def send(self, request, confirm_type, signup=False, **kwargs):
        context = {}
        email_template = None
        if confirm_type == PASSCODE:
            context_data = { "passcode": self.passcode }
            email_template = "confirmation_passcode_signup" if signup else "confirmation_passcode"
        elif confirm_type == ACTIVATE_LINK:
            activate_url = WEBSITE_URL + "/account/confirm-email/" + self.key
            context_data = {"activate_url": activate_url }
            email_template = "confirmation_link_signup" if signup else "confirmation_link"
        else:
            raise Exception(message=("Incorrect confirm type. Please choose either {passcode} or {activate_link}".format(passcode=PASSCODE,activate_link=ACTIVATE_LINK)))
        
        email = Email.objects.create(to_emails=self.email_address.email,email_template=email_template,context_data=str(context_data))
        
        self.email_sent = email
        self.sent = timezone.now()
        self.save()


