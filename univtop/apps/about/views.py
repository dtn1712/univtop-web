from django.views.generic import ListView, TemplateView
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.views.generic.edit import FormView
from django.utils import timezone
from django.core import serializers
from django.contrib.auth.models import User

from tastypie.models import ApiKey

# from univtop.apps.about.forms import ContactForm
# from univtop.apps.app_helper import get_template_path
# from univtop.settings import API_URL

from univtop.apps.app_views import AppBaseView

import logging, requests

logger = logging.getLogger(__name__)

APP_NAME = "about"


class AboutView(TemplateView,AppBaseView):
	app_name = APP_NAME
	template_name = "about"
	

# class ContactView(FormView,AppBaseView):
# 	app_name = APP_NAME
# 	template_name = "contact"
# 	form_class = ContactForm

# 	def get_success_url(self):
# 		return "/?action=send_feedback&result=success&show=1" 

# 	def form_valid(self, form):
# 		form.save()
# 		admins = User.objects.filter(is_superuser=True)
# 		if len(admins) != 0:
# 			try:
# 				data = form.cleaned_data
# 				api_key = ApiKey.objects.get(user=admins[0]).key
# 				headers = {'Authorization': 'ApiKey ' + admins[0].username + ":" + api_key }
# 				context_data = {
# 					"name": data['name'],
# 					'email': data['email'], 
# 					'subject': data['subject'], 
# 					'message': data['message']
# 				}
				
# 				to_emails = ""
# 				for admin in admins:
# 					to_emails = to_emails + admin.email + ";"
				
# 				payload = {
# 					'to_emails': to_emails[:len(to_emails)-1],
# 					"email_template": "customer_feedback",
# 					"context_data": str(context_data)
# 				}
# 				requests.post(API_URL + "/api/v1/email/send/",headers=headers,data=payload)
# 			except Exception as e:
# 				logger.exception(e)

# 		return super(ContactView, self).form_valid(form)