from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.views.generic import ListView, TemplateView, DetailView
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.utils import timezone
from django.core import serializers

from univtop.apps.app_views import AppBaseView
from univtop.apps.app_helper import get_template_path, handle_request_get_message
from univtop.apps.app_helper import capitalize_first_letter, get_user_login_object
from univtop.apps.app_helper import get_base_template_path
from univtop.apps.main.constants import DEFAULT_PAGE_SIZE
from univtop.settings import SITE_NAME, DEFAULT_TEMPLATE_FILE_EXTENSION

import django_mobile

import logging, json, datetime

logger = logging.getLogger(__name__)

APP_NAME = "member"


class ProfileView(AppBaseView,DetailView):
	app_name = APP_NAME
	model = User
	slug_field = 'username'
	slug_url_kwarg = 'username'
	queryset = User.objects.filter(is_staff=False)
	
	def dispatch(self, *args, **kwargs):
		obj = self.get_object()
		user_login = get_user_login_object(self.request)
		if user_login != None and user_login.username == obj.username:
			self.template_name = "dashboard"
		else:
			self.template_name = 'profile'
		return super(ProfileView, self).dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(ProfileView, self).get_context_data(**kwargs)
		context['start_index'] = DEFAULT_PAGE_SIZE
		return context