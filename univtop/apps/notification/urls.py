from django.conf.urls import *

from univtop.apps.notification.views import ListNotificationView

urlpatterns = patterns('univtop.apps.notification.views',
    url(r"^$", ListNotificationView.as_view(), name='list_notification'),
)
