from django.views.generic import ListView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from univtop.apps.main.models import Notification
from univtop.apps.app_views import AppBaseView
from univtop.apps.app_helper import get_user_login_object
from univtop.apps.main.constants import DEFAULT_PAGE_SIZE

import logging

logger = logging.getLogger(__name__)

APP_NAME = "notification"


class ListNotificationView(ListView,AppBaseView):
	app_name = APP_NAME
	template_name = "list"

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		self.request = request
		return super(ListNotificationView, self).dispatch(request, *args, **kwargs)

	def get_queryset(self):
		user_login = get_user_login_object(self.request)
		return Notification.objects.filter(notify_to=user_login).order_by("-date").select_related("notify_to")[:DEFAULT_PAGE_SIZE]