from django.views.generic.base import TemplateResponseMixin, ContextMixin
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView, View

from univtop.apps.app_helper import get_template_path, handle_request_get_message
from univtop.apps.app_helper import capitalize_first_letter, get_user_login_object
from univtop.apps.app_helper import get_base_template_path
from univtop.apps.main.models import Notification
from univtop.apps.main.constants import NEW
from univtop.apps.account.helper import activate_email_reminder_message
from univtop.settings import SITE_NAME, DEFAULT_TEMPLATE_FILE_EXTENSION

from tastypie.models import ApiKey

import django_mobile

class AppBaseView(TemplateResponseMixin,ContextMixin):
	sub_path = "/page/"

	def dispatch(self, request, *args, **kwargs):
		if "session_id" in request.GET and request.GET['session_id'] != request.session.session_key:
			return HttpResponseRedirect("/")
			
		user_login = get_user_login_object(request)
		self.user_login = user_login
		return super(AppBaseView, self).dispatch(request, *args,**kwargs)

	def get_context_data(self, **kwargs):
		context = super(AppBaseView, self).get_context_data(**kwargs)
		context['user_login'] = self.user_login
		context['app_name'] = self.app_name
		context['page_type'] = self.app_name + "_" + self.template_name
		activate_message = activate_email_reminder_message(self.request,self.user_login) 
		if activate_message == None:
			context["request_message"] = handle_request_get_message(self.request,{ 
				'user_login': self.user_login, 
				'site_name': capitalize_first_letter(SITE_NAME) 
			})
			context['is_activate_message'] = False
		else:
			context["request_message"] = activate_message
			context['is_activate_message'] = True

		''' 
			Currently, using non responsive template for all request. Will do the responsive template later
		'''
		context['template_type'] = "non_responsive"
		flavour = "full"
		# flavour = django_mobile.get_flavour(self.request)
		# if flavour != None:
		# 	context['template_type'] = "non_responsive" if flavour == "full" else "responsive"

		context['new_notifications'] = bool(Notification.objects.filter(notify_to=self.user_login,status=NEW).count())

		context['app_base_template'] = get_base_template_path(flavour,self.app_name)

		return context

	def get_template_names(self):
		#return [get_template_path(self.app_name,self.template_name,django_mobile.get_flavour(self.request),self.sub_path)]
		return [get_template_path(self.app_name,self.template_name,None,self.sub_path)]


class PostRequestOnlyView(View):

	def get(self, request, *args, **kwargs):
		return HttpResponseRedirect("/")

